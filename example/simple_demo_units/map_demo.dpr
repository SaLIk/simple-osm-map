program map_demo;

uses
  FastMM4,
  Forms,
  demo in 'demo.pas' {frmDemo},
  uSimpleOsmMap in '..\..\source\uSimpleOsmMap.pas',
  uSimpleOsmMapConstFunc in '..\..\source\uSimpleOsmMapConstFunc.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmDemo, frmDemo);
  Application.Run;
end.
