unit demo;

interface

uses
  uSimpleOsmMapConstFunc, uSimpleOsmMap,

  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, GR32_Image, ExtCtrls, StdCtrls, Gauges, ComCtrls, XPMan, Buttons,
  ImgList;

type
  TfrmDemo = class(TForm)
    pnlRight: TPanel;
    btnTest: TBitBtn;
    edtLat: TEdit;
    edtLon: TEdit;
    edtZoom: TEdit;
    pnlMapa: TPanel;
    btnCreateCache: TBitBtn;
    pnl1: TPanel;
    lbl1: TLabel;
    edtLeftDec: TEdit;
    edtTopDec: TEdit;
    Label1: TLabel;
    edtRightDec: TEdit;
    edtBottomDec: TEdit;
    lbl2: TLabel;
    edtZoomStart: TEdit;
    edtZoomStop: TEdit;
    Label2: TLabel;
    pnlCachingProgress: TPanel;
    gProgress: TGauge;
    lblCntTilt: TLabel;
    lblLastTilt: TLabel;
    lblRemainTime: TLabel;
    imgProgress: TImage32;
    splMain: TSplitter;
    btnAddRandomMapPoints: TBitBtn;
    btnSearchPoint5: TBitBtn;
    pgcMain: TPageControl;
    tsMapTest: TTabSheet;
    tsChaching: TTabSheet;
    lbl3: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    pnlMapObjects: TPanel;
    Label5: TLabel;
    edtMapObjectCount: TEdit;
    lbl4: TLabel;
    edtSearchLabel: TEdit;
    pnl2: TPanel;
    btnDelete: TBitBtn;
    btnVisible: TBitBtn;
    lbl5: TLabel;
    btnGpsVisibility: TBitBtn;
    btnObjectsShow: TBitBtn;
    btnGpsPos: TBitBtn;
    cbbMapServer: TComboBox;
    lbl6: TLabel;
    cbbMapTileUrl: TComboBox;
    chkUseCache: TCheckBox;
    chkOffline: TCheckBox;
    scrlbrZoom: TScrollBar;
    btnMerge: TBitBtn;
    tsPointCaching: TTabSheet;
    Panel1: TPanel;
    gProgressObj: TGauge;
    lblCntTiltObj: TLabel;
    lblLastTiltObj: TLabel;
    lblRemainTimeObj: TLabel;
    imgProgressObj: TImage32;
    btnCreateCacheObj: TBitBtn;
    cbbMapTileUrlObj: TComboBox;
    Label9: TLabel;
    edtZoomStopObj: TEdit;
    edtZoomStartObj: TEdit;
    Label10: TLabel;
    Label6: TLabel;
    edtPointRecArea: TEdit;
    XPManifest1: TXPManifest;
    tsMapObjects: TTabSheet;
    tsGPS: TTabSheet;
    pnlGps: TPanel;
    Label7: TLabel;
    edtLonGps: TEdit;
    Label8: TLabel;
    edtLatGps: TEdit;
    btnSetGps: TBitBtn;
    Panel2: TPanel;
    btnShowHideLabels: TBitBtn;
    ilTabs: TImageList;
    Label11: TLabel;
    Label12: TLabel;
    edtObjectLon: TEdit;
    Label13: TLabel;
    edtObjectCaption: TEdit;
    Panel3: TPanel;
    btnAddMapObject: TBitBtn;
    edtObjectLat: TEdit;
    Label14: TLabel;
    chkShowMapZoomControl: TCheckBox;
    chkShowMapMoveControl: TCheckBox;
    btnZoomAtObject: TBitBtn;
    cbbObjectKind: TComboBox;
    Label15: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure btnTestClick(Sender: TObject);
    procedure btnCreateCacheClick(Sender: TObject);
    procedure btnAddRandomMapPointsClick(Sender: TObject);
    procedure btnSearchPoint5Click(Sender: TObject);
    procedure btnGpsVisibilityClick(Sender: TObject);
    procedure btnGpsPosClick(Sender: TObject);
    procedure btnObjectsShowClick(Sender: TObject);
    procedure btnDeleteClick(Sender: TObject);
    procedure btnVisibleClick(Sender: TObject);
    procedure cbbMapServerChange(Sender: TObject);
    procedure chkOfflineClick(Sender: TObject);
    procedure chkUseCacheClick(Sender: TObject);
    procedure scrlbrZoomChange(Sender: TObject);
    procedure edtZoomChange(Sender: TObject);
    procedure btnCreateCacheObjClick(Sender: TObject);
    procedure btnSetGpsClick(Sender: TObject);
    procedure btnMergeClick(Sender: TObject);
    procedure btnShowHideLabelsClick(Sender: TObject);
    procedure btnAddMapObjectClick(Sender: TObject);
    procedure chkShowMapZoomControlClick(Sender: TObject);
    procedure chkShowMapMoveControlClick(Sender: TObject);
    procedure btnZoomAtObjectClick(Sender: TObject);
  private
    { Private declarations }
    procedure ClearCachingInfo;
    procedure ClearCachingInfoObj;
    procedure CachingInfo;
    procedure CachingInfoObj;
    procedure MapMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer;
      MousePos: TPoint; var Handled: Boolean);
  public
    { Public declarations }
    Test: TSimpleOsmMap;
    //CachingThread: TCachingThread;
  end;

var
  frmDemo: TfrmDemo;

implementation

{$R *.dfm}

procedure TfrmDemo.ClearCachingInfo;
begin
  gProgress.Progress := 0;
  lblCntTilt.Caption := '';
  lblLastTilt.Caption := '';
  lblRemainTime.Caption := '';
  imgProgress.Bitmap.Clear;
end;

procedure TfrmDemo.ClearCachingInfoObj;
begin
  gProgressObj.Progress := 0;
  lblCntTiltObj.Caption := '';
  lblLastTiltObj.Caption := '';
  lblRemainTimeObj.Caption := '';
  imgProgressObj.Bitmap.Clear;
end;

procedure TfrmDemo.CachingInfo;
var
  TmpProgress: TProgressInfo;
begin
  TmpProgress := Test.CachingThread.ProgressInfo;

  if TmpProgress.ProgressProc = cClrCacheIntInfo then begin
    ClearCachingInfo;
  end else begin
    gProgress.Progress := TmpProgress.ProgressProc;

    lblCntTilt.Caption := Format('Tiles count: %d / %d', [
      TmpProgress.CntDwnTiles, TmpProgress.CntAllTiles]);

    Delete(TmpProgress.LastTileUrl, 1 , Pos('[', cbbMapTileUrl.Text) - 1);
    lblLastTilt.Caption := Format('Tile: %s', [TmpProgress.LastTileUrl]);

    lblRemainTime.Caption := Format('Elapsed time: %s', [TmpProgress.RemainTime]);
  end;

  Finalize(TmpProgress);
  if (TmpProgress.ImgStream <> nil) then begin
    TmpProgress.ImgStream.Position := 0;
    imgProgress.Bitmap.Assign(nil);
    imgProgress.Bitmap.SetSize(cTileSize, cTileSize);
    imgProgress.AutoSize := True;
    StreamPicToBitmap(TmpProgress.ImgStream, TBitmap(imgProgress.Bitmap));
    FreeAndNil(TmpProgress.ImgStream);
  end;
end;

procedure TfrmDemo.CachingInfoObj;
var
  TmpProgress: TProgressInfo;
begin
  TmpProgress := Test.CachingThread.ProgressInfo;

  if TmpProgress.ProgressProc = cClrCacheIntInfo then begin
    ClearCachingInfoObj;
  end else begin
    gProgressObj.Progress := TmpProgress.ProgressProc;

    lblCntTiltObj.Caption := Format('Tiles count: %d / %d', [
      TmpProgress.CntDwnTiles, TmpProgress.CntAllTiles]);

    Delete(TmpProgress.LastTileUrl, 1 , Pos('[', cbbMapTileUrlObj.Text) - 1);
    lblLastTiltObj.Caption := Format('Tile: %s', [TmpProgress.LastTileUrl]);

    lblRemainTimeObj.Caption := Format('Elapsed time: %s', [TmpProgress.RemainTime]);
  end;

  Finalize(TmpProgress);
  if (TmpProgress.ImgStream <> nil) then begin
    TmpProgress.ImgStream.Position := 0;
    imgProgressObj.Bitmap.Assign(nil);
    imgProgressObj.Bitmap.SetSize(cTileSize, cTileSize);
    imgProgressObj.AutoSize := True;
    StreamPicToBitmap(TmpProgress.ImgStream, TBitmap(imgProgressObj.Bitmap));
    FreeAndNil(TmpProgress.ImgStream);
  end;
end;

procedure TfrmDemo.MapMouseWheel(Sender: TObject; Shift: TShiftState; WheelDelta: Integer;
  MousePos: TPoint; var Handled: Boolean);
begin
  scrlbrZoom.Position := Test.Zoom;
end;


procedure TfrmDemo.cbbMapServerChange(Sender: TObject);
begin
  Test.OsmMapUrl := cbbMapServer.Items.Strings[cbbMapServer.ItemIndex];
  Self.Caption := Test.OsmMapUrl;
end;

procedure TfrmDemo.chkOfflineClick(Sender: TObject);
begin
  Test.WorkOffline := chkOffline.Checked;
end;

procedure TfrmDemo.chkShowMapMoveControlClick(Sender: TObject);
begin
  Test.ShowMapMoveControl := chkShowMapMoveControl.Checked;
end;

procedure TfrmDemo.chkShowMapZoomControlClick(Sender: TObject);
begin
  Test.ShowMapZoomControl := chkShowMapZoomControl.Checked;
end;

procedure TfrmDemo.chkUseCacheClick(Sender: TObject);
begin
  Test.UseCache := chkUseCache.Checked;
end;

procedure TfrmDemo.edtZoomChange(Sender: TObject);
var
  TmpInt: Integer;
begin
  if (Length(edtZoom.Text) > 0) then begin
    if TryStrToInt(edtZoom.Text, TmpInt) then begin
      if MapZoomIsOk(TmpInt) then begin
        scrlbrZoom.Position := TmpInt;
      end;
    end;
  end;
end;

procedure TfrmDemo.btnAddMapObjectClick(Sender: TObject);
var
  idx: Integer;
begin
  idx := cbbObjectKind.ItemIndex;

  case idx of
    0: begin
      //default
      Test.AddMapObject(
        'test map object',
        StrToFloat(edtObjectLat.Text),
        StrToFloat(edtObjectLon.Text),
        edtObjectCaption.Text,
        clYellow,
        True,
        5
      );

    end;
    1: begin
      //circle
      Test.AddMapObjectDrawCircle(
        'test drawing - circle',
        StrToFloat(edtObjectLat.Text),
        StrToFloat(edtObjectLon.Text),
        10,
        edtObjectCaption.Text,
        clYellow
      );

    end;
    2: begin
      //rectangle
      Test.AddMapObjectDrawRectangle(
        'test drawing - rectangle',
        StrToFloat(edtObjectLat.Text),
        StrToFloat(edtObjectLon.Text),
        50,
        150,
        edtObjectCaption.Text,
        clYellow
      );

    end;
    3: begin
      //line
      Test.AddMapObjectDrawLine(
        'test drawing - line',
        StrToFloat(edtObjectLat.Text),
        StrToFloat(edtObjectLon.Text),
        60,
        edtObjectCaption.Text,
        2, //line widht
        clRed,
        600000 //600 km
      );

    end
    else begin
      Test.AddMapObject(
        'test map object',
        StrToFloat(edtObjectLat.Text),
        StrToFloat(edtObjectLon.Text),
        edtObjectCaption.Text,
        clYellow,
        True,
        5
      );

    end;
  end;
end;

procedure TfrmDemo.btnAddRandomMapPointsClick(Sender: TObject);
var
  I, TmpInt, TmpInt2, TmpInt3: Integer;
  Data: TStringArray;
  Cesta: string;
begin
  if (Test <> nil) then begin
    Screen.Cursor := crHourGlass;
    Test.DisableMapObjectsRepaint;
    try

      if (Test.FirstMapObject <> nil) then begin
        //exist one or more objects ?
        Test.ClearMapObjectList;
      end;

      //prepare data array
      SetLength(Data, 5);
      Cesta := ExtractFilePath(Application.ExeName) + 'markers\';
      Data[0] := Cesta + 'lightblue1.png';
      Data[1] := Cesta + 'ol-marker-gold.png';
      Data[2] := Cesta + 'ol-marker-green.png';
      Data[3] := Cesta + 'lightblue3.png';
      Data[4] := Cesta + 'ol-marker-blue.png';


      for I := 1 to StrToInt(edtMapObjectCount.Text) do begin
        TmpInt := Random(100);

        TmpInt2 := Random(50);
        if (TmpInt2 >= 25) then begin
          TmpInt2 := -1;
        end else begin
          TmpInt2 := 1;
        end;

        TmpInt3 := Random(50);
        if (TmpInt3 >= 25) then begin
          TmpInt3 := -1;
        end else begin
          TmpInt3 := 1;
        end;

        if (TmpInt <= 50) then begin
          //clasic point
          Test.AddMapObject(
            'Test ' + IntToStr(I),  //jmeno
            cMaxAbsLat * (Random(100) * 0.01) * TmpInt2,
            cMaxAbsLon * (Random(100) * 0.01) * TmpInt3,
            'Test ' + IntToStr(I),  //caption
            clBlue,
            True,
            Random(10)
          );
        end else begin
          //picture point
          Test.AddMapObject(
            'Test ' + IntToStr(I),  //jmeno
            cMaxAbsLat * (Random(100) * 0.01) * TmpInt2,
            cMaxAbsLon * (Random(100) * 0.01) * TmpInt3,
            Data[Random(Length(Data)-1)],
            'Test ' + IntToStr(I),  //caption
            True
          );
        end;
      
      end; //end - for

    finally
      SetLength(Data, 0);
      Data := nil;

      Test.EnableMapObjectsRepaint;
      Screen.Cursor := crDefault;
    end;
  end;
end;

procedure TfrmDemo.btnCreateCacheClick(Sender: TObject);
begin
  if (not Test.CachingThread.Downloading) then begin
    Test.CachingThread.DbFilePath := ExtractFilePath(Application.ExeName) + 'data\db\data.fdb';
    Test.CachingThread.OnProgress := CachingInfo;
    if (not Test.CachingThread.DbConnected) then begin
      Test.CachingThread.DbConnected := True;
    end;

    if Test.CachingThread.DefineRectangleForDownload(
      StrToFloat(edtLeftDec.Text), StrToFloat(edtRightDec.Text),
      StrToFloat(edtTopDec.Text), StrToFloat(edtBottomDec.Text),
      StrToInt(edtZoomStart.Text), StrToInt(edtZoomStop.Text),
      cbbMapTileUrl.Text)
    then begin
      Test.CachingThread.StartDownload;
      btnCreateCache.Caption := 'Stop caching';
    end else begin
      MessageDlg('Wrong caching parameters!', mtError, [mbOk], 0);
    end;

  end else begin
    Test.CachingThread.StopDownload;
    Test.CachingThread.OnProgress := nil;
    ClearCachingInfo;
    btnCreateCache.Caption := 'Start caching';
  end;
end;

procedure TfrmDemo.btnCreateCacheObjClick(Sender: TObject);
var
  DataArray: TDecLonLatPositionArray;
  MapObject: TMapObject;
begin
  if (not Test.CachingThread.Downloading) then begin
    Test.CachingThread.DbFilePath := ExtractFilePath(Application.ExeName) + 'data\db\data.fdb';
    Test.CachingThread.OnProgress := CachingInfoObj;
    if (not Test.CachingThread.DbConnected) then begin
      Test.CachingThread.DbConnected := True;
    end;

    //prepare point array
    try
      MapObject := Test.FirstMapObject;
      while (MapObject <> nil) do begin
        SetLength(DataArray, Length(DataArray) + 1);
        DataArray[Length(DataArray) - 1].LonDec := MapObject.LonDec;
        DataArray[Length(DataArray) - 1].LatDec := MapObject.LatDec;
        //
        MapObject := Test.NextMapObject(MapObject);
      end;

      if Test.CachingThread.DefineLonLatDecPointsForDownload(DataArray,
        StrToInt(edtZoomStartObj.Text), StrToInt(edtZoomStopObj.Text),
        StrToInt(edtPointRecArea.Text), cbbMapTileUrlObj.Text)
      then begin
        Test.CachingThread.StartDownload;
        btnCreateCacheObj.Caption := 'Stop caching';
      end else begin
        MessageDlg('Wrong caching parameters!', mtError, [mbOk], 0);
      end;

    finally
      SetLength(DataArray, 0);
      DataArray := nil;
    end;
  end else begin
    Test.CachingThread.StopDownload;
    Test.CachingThread.OnProgress := nil;
    ClearCachingInfoObj;
    btnCreateCacheObj.Caption := 'Start caching';
  end;
end;

procedure TfrmDemo.btnDeleteClick(Sender: TObject);
var
  MO: TMapObject;
begin
  MO := Test.FindMapObject(edtSearchLabel.Text); //hledam bod podle nazvu
  if (MO <> nil) then begin
    if Test.DeleteMapObject(MO) then begin
      MessageDlg('Objekt vymazan', mtInformation, [mbOk], 0);
    end else begin
      MessageDlg('Objekt se nepodarilo vymazat', mtError, [mbOk], 0);
    end;
  end else begin
    MessageDlg('Objekt nenalezen', mtError, [mbOk], 0);
  end;
end;

procedure TfrmDemo.btnGpsPosClick(Sender: TObject);
var
  TmpInt2: Integer;
begin
  TmpInt2 := Random(2);
  if (TmpInt2 = 0) then begin
    TmpInt2 := -1;
  end;

  Test.SetGpsPosition(
    cMaxAbsLon * (Random(100) * 0.01) * TmpInt2,
    cMaxAbsLat * (Random(100) * 0.01) * TmpInt2
  );
end;

procedure TfrmDemo.btnGpsVisibilityClick(Sender: TObject);
begin
  Test.SetGpsVisibility(not Test.GpsPosition.Visible);
end;

procedure TfrmDemo.btnMergeClick(Sender: TObject);
begin
  Test.MergeMapObject := not Test.MergeMapObject;
end;

procedure TfrmDemo.btnObjectsShowClick(Sender: TObject);
begin
  Test.ShowMapObjects := not Test.ShowMapObjects;
end;

procedure TfrmDemo.btnSearchPoint5Click(Sender: TObject);
var
  MO: TMapObject;
begin
  MO := Test.FindMapObject('', edtSearchLabel.Text); //hledam nahodny bod
  if (MO <> nil) then begin
    MessageDlg('Objekt "' + MO.Caption + '" nalezen', mtInformation, [mbOk], 0);
  end else begin
    MessageDlg('Objekt nenalezen', mtError, [mbOk], 0);
  end;
end;

procedure TfrmDemo.btnSetGpsClick(Sender: TObject);
begin
  Test.SetGpsPosition(
    StrToFloat(edtLonGps.Text),
    StrToFloat(edtLatGps.Text)
  );
end;

procedure TfrmDemo.btnShowHideLabelsClick(Sender: TObject);
begin
  Test.ShowObjectsLabel := not Test.ShowObjectsLabel;
end;

procedure TfrmDemo.btnTestClick(Sender: TObject);
begin
  Test.OsmMapUrl := cbbMapServer.Items.Strings[cbbMapServer.ItemIndex];
  Self.Caption := Test.OsmMapUrl;
  Test.ShowMap(
    StrToFloat(edtLon.Text),
    StrToFloat(edtLat.Text),
    StrToInt(edtZoom.Text)
  );
end;

procedure TfrmDemo.btnVisibleClick(Sender: TObject);
var
  MO: TMapObject;
begin
  MO := Test.FindMapObject('', edtSearchLabel.Text);
  if (MO <> nil) then begin
    MessageDlg('Zmena viditelnosti bodu: ' + MO.Caption, mtInformation, [mbOk], 0);
    Test.SetMapObjectVisibility(MO, not MO.Visible);
  end else begin
    MessageDlg('Objekt nenalezen', mtError, [mbOk], 0);
  end;
end;

procedure TfrmDemo.btnZoomAtObjectClick(Sender: TObject);
var
  MO: TMapObject;
begin
  MO := Test.FindMapObject('', edtSearchLabel.Text);
  if (MO <> nil) then begin
    Test.ZoomAtMapObject('', edtSearchLabel.Text);
  end else begin
    MessageDlg('Objekt nenalezen', mtError, [mbOk], 0);
  end;
end;

procedure TfrmDemo.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  (*CachingThread.Terminate;
  while CachingThread.Downloading do begin
    Sleep(200);
    Application.ProcessMessages;
  end;
  FreeAndNil(CachingThread);*)

  FreeAndNil(Test);
end;

procedure TfrmDemo.FormCreate(Sender: TObject);
var
  DbFile: string;
  TmpInt: Integer;
  SL: TStringList;
  UrlData: string;
begin
  DbFile := ExtractFilePath(Application.ExeName) + 'data\db\data.fdb';

  Test := TSimpleOsmMap.CreateOsmMap(nil, DbFile);
  Test.Parent := pnlMapa;
  Test.Align := alClient;

  Test.OnMouseWheel := MapMouseWheel; //handling zoom changes
  Test.ShowCoordinates := True;

  Test.UseCache := True;
  chkUseCache.Checked := Test.UseCache;

  Test.WorkOffline := False;
  chkOffline.Checked := Test.WorkOffline;

  //tile server list
  SL := TStringList.Create;
  try
    UrlData := ExtractFilePath(Application.ExeName) + 'data\osm_map_url.txt';
    if FileExists(UrlData) then begin
      SL.LoadFromFile(UrlData, TEncoding.ASCII);
      if (SL.Count > 0) then begin

        //data into combo
        cbbMapServer.Items.Text := SL.Text;
        cbbMapTileUrl.Items.Text := SL.Text;
        cbbMapTileUrlObj.Items.Text := SL.Text;

        //use "http://c.tile.openstreetmap.org/[zoom]/[x]/[y].png" if exist
        TmpInt := cbbMapServer.Items.IndexOf('http://c.tile.openstreetmap.org/[zoom]/[x]/[y].png');
        if (TmpInt >= 0) then begin
          cbbMapServer.ItemIndex := TmpInt;
        end;
        TmpInt := cbbMapTileUrl.Items.IndexOf('http://c.tile.openstreetmap.org/[zoom]/[x]/[y].png');
        if (TmpInt >= 0) then begin
          cbbMapTileUrl.ItemIndex := TmpInt;
        end;
        TmpInt := cbbMapTileUrlObj.Items.IndexOf('http://c.tile.openstreetmap.org/[zoom]/[x]/[y].png');
        if (TmpInt >= 0) then begin
          cbbMapTileUrlObj.ItemIndex := TmpInt;
        end;

      end; //end SL.count > 0
    end;
  finally
    FreeAndNil(SL);
  end;

  //CachingThread := TCachingThread.Create;
end;

procedure TfrmDemo.scrlbrZoomChange(Sender: TObject);
begin
  edtZoom.Text := IntToStr(scrlbrZoom.Position);
end;

end.
