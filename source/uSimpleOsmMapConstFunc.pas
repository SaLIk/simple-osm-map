(* uSimpleOsmMapConstFunc.pas -- simple OpenStreetMap viewer
  version 0.0.1, 15.09.2013

  Copyright (C) 2014 Pavel Sala

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Pavel Sala        
  email@salikovi.cz
*)

unit uSimpleOsmMapConstFunc;

interface

uses
  Math                    //Power()
  ,System.Types           //CompareValue
  ,SysUtils               //Trim
  ,Classes                //TMemoryStream
  ,Windows                //GetXValue
  ,GraphicEx              //loading images
  ,Graphics               //TCanvas
  ,GR32                   //TBitmap32
  ,jpeg                   //TJPEGImage
  ,pngimage               //TPNGImage
  ;

const
  cCRLF = #13#10;
  clNone32 = TColor32($00FFFFFF);

  cDrawSamplerRadius = 5;
  cPointDefRadius = 10;
  cPointDefWidth = 10;
  cBorderDefWidth = 2;
  cPointDefHeight = 10;
  cPointDefAngle = 90; //90� = 3 hours
  cPointDefColor = clRed;
  cDefLineLengthMeters = 600000; //600 km
  cDefLineWidth = 3;

  cCachingRadius = 100; //meters

  cEmptyZoom = -1;
  cEmptyRadius = -1;  //for drawings
  cEmptyWidth = -1;   //for drawings
  cEmptyHeight = -1;  //for drawings

  cErrZoom = -999999; //error value for zoom
  cErrResolution = -999999; //error value for resolution
  cErrCoord = -999999; //error value for coordinates
  cErrDistance = -999999; //error distance between coordinates
  cErrPixel = -999999; //error value for pixels
  cErrTile = -999999;
  cErrAzimuth = -999999;

  cClrCacheIntInfo = -1; //initial value for caching info progress
  cClrCacheStrInfo = ''; //initial value for caching info progress
  cCacheTimeOut = -1; //in days, -1 = infinite

  cMaxZoom = 19;
  cMinZoom = 0;
  cMaxAbsLat = 85.0511; //latitude in WGS84 in range from -85.0511 to 85.0511
  cMaxAbsLon = 180.0; //longitude in WGS84 in range from -180 to 180
  cTileX = '[x]';
  cTileY = '[y]';
  cTileZoom = '[zoom]';
  cEarthRadius = 6372797.560856; //earth diameter in meters
  cEquatorLength = 40075016.68557849; //equator at 0�, WGS84, in meters (2*pi*6378,1370 km)
  cMeridianLength = 39940652.65274004; //meridian at 0�, WGS84, in meters (2*pi*6356,7523 km)
  cTileSize = 256; //256x256 pixels

  cMaxScaleWidth: Integer = 100; //px
  cMapScaleHeight: Integer = 16; //px

  cDbLogin = 'SYSDBA';
  cDbPassword = 'masterkey';
  cDbEncoding = 'WIN1250';
  cDbCollate = 'PXW_CSY';

  cMultiPointSize = 12;
  cMapObjectCaptionDelimiter = '|;|';
  cMultiPointColor = clBlue;

  cMinPanDiff = 4; //map pan treshold

  cBtnZoomWidth = 40;
  cBtnZoomHeight = 40;
  cBtnZoomRound = 5;
  cBtnZoomPadding = 8;
  cBtnZoomMarginLeft = 10;
  cBtnZoomMarginRight = 10;
  cBtnZoomMarginTop = 10;
  cBntZoomMarginBottom = 10;

  cBtnMoveWidth = 40;
  cBtnMoveHeight = 40;
  cBtnMoveRound = 5;
  cBtnMovePadding = 6;
  cBtnMoveMarginLeft = 10;
  cBtnMoveMarginRight = 10;
  cBtnMoveMarginTop = 10;
  cBntMoveMarginBottom = 10;


type
  TMapLayer = (
    mlMap         //map tiles
    ,mlPoints     //map objects
    ,mlDraw       //map drawings (line, circe, rectangle ... )
    ,mlGps        //gps position
    ,mlCoord      //coordinates
    ,mlInfo       //map info layer (credits etc.)
    ,mlMapControl //map control
  );
  TMapControlType = (mcUnknown, mcZoomIn, mcZoomOut, mcMoveLeft, mcMoveRight, mcMoveTop, mcMoveBottom);
  TCachingType = (ctUndefined, ctRectange, ctPoints);
  TProgressProc = procedure of object;
  TTileProc = TProgressProc;

  TProgressInfo = record
    CntDwnTiles,
    CntAllTiles,
    ProgressProc: Integer;
    LastTileUrl: string;
    RemainTime: string;
    ImgStream: TMemoryStream;
  end;

  TTile = record
    DownloadError: Boolean;
    XTile, YTile, Zoom,
    PosXpx, PosYpx: Integer;
    TileUrl: string;
    TileStream: TMemoryStream;
  end;

  TTileDef = record
    XTile, YTile, Zoom,
    PosXpx, PosYpx: Integer;
    TileUrl: string;
  end;
  TTilesArray = array of TTileDef;

  TStringArray = array of string;

  TDecLonLatPosition = record
    LonDec, LatDec: Double;
  end;
  TDecLonLatPositionArray = array of TDecLonLatPosition;

  TCenterPosition = record
    XTile, YTile: Double;
    LonDec, LatDec: Double;
    X, Y: Integer;
  end;

  TMapPosition = record
    LonDec, LatDec: Double;
    X, Y: Integer;
  end;
  PTMapPosition = ^TMapPosition;



function MaxXYTile(AZoom: Integer): Integer;

function RangeLonOk(ALonDec: Double): Boolean;
function RangeLatOk(ALatDec: Double): Boolean;
function AzimuthOk(AAzimuth: Double): Boolean;

function XTileToLon(AXtile: Double; AZoom: Integer): Double;
function YTileToLat(AYtile: Double; AZoom: Integer): Double;

function LatToYtile(ALatDec: Double; AZoom: Integer): Double;
function LonToXtile(ALonDec: Double; AZoom: Integer): Double;

function LatAddDistance(ALatDec: Double; AMeters: Integer): Double;
function LonAddDistance(ALonDec: Double; AMeters: Integer): Double;

function CalculateCoordDistance(aFromLatDec: Double; aFromLonDec: Double; aToLatDec: Double; aToLonDec: Double): Double;
function CalculateMapResolution(ALatDec: Double; AZoom: Integer): Double;
function CalculatePixelRation(AFromZoom: Integer; AToZoom: Integer): Double;

function TrimUrlBase(AUrl: string): string;
function TrimTileExt(AUrl: string): string;

function FilePicToBitmap32(AFilePath: string; ABitmap: TBitmap32): Boolean;
function StreamPicToBitmap32(AImgStream: TMemoryStream; ABitmap: TBitmap32): Boolean;
function StreamPicToBitmap(AMS: TMemoryStream; ABitmap: TBitmap): Boolean;
function BrighterColor(AColor: TColor32; APercent: Byte): TColor32;
function Blend(Color1, Color2: TColor; ABlendLevel: Byte): TColor;

function CorrectPlural(const AString: string; ACount: Integer; ALang: string = 'cz'): string;
function HumanReadableTime(ADateTime: Double; ALang: string = 'cz'): string;

function MapZoomIsOk(aMapZoom: Integer): Boolean;

function GetLineEnd(aLineStart: TPoint; aAngle: Double; aMapResolution: Double; aLineMeterLength: Integer = cDefLineLengthMeters): TPoint;
function IsLineInRect(aLineStart: TPoint; aAngle: Double; aRect: TRect; aMapResolution: Double;
  var aLineEnd: TPoint; aLineMeterLength: Integer = cDefLineLengthMeters): Boolean;

implementation

function MaxXYTile(AZoom: Integer): Integer;
begin
  { X and Y goes from "0" to "2^zoom - 1" }
  Result := Round(Power(2, AZoom) - 1);
end;

function RangeLonOk(ALonDec: Double): Boolean;
var
  comp: TValueRelationship;
begin
  comp := CompareValue(Abs(ALonDec), cMaxAbsLon);
  Result := (comp = EqualsValue)
    or (comp = LessThanValue);
end;

function RangeLatOk(ALatDec: Double): Boolean;
var
  comp: TValueRelationship;
begin
  comp := CompareValue(Abs(ALatDec), cMaxAbsLat);
  Result := (comp = EqualsValue)
    or (comp = LessThanValue);
end;

function AzimuthOk(AAzimuth: Double): Boolean;
begin
  Result := (AAzimuth >= 0) and (AAzimuth <= 360);
end;

function XTileToLon(AXtile: Double; AZoom: Integer): Double;
begin
  { n := Power(2, zoom);
    lon_deg := ((xtile / n) * 360.0) - 180.0; }

  if ( (AXtile <> cErrTile)
    and (AXtile >= 0)
    and (AZoom <> cErrZoom)
    and (Abs(AZoom) <= cMaxZoom)
    and (AZoom >= cMinZoom) )
  then begin
    try
      Result := ((AXtile / Power(2, AZoom)) * 360) - 180;
    except
      Result := cErrCoord;
    end;
  end else begin
    Result := cErrCoord;
  end;
end;

function YTileToLat(AYtile: Double; AZoom: Integer): Double;
begin
  {n := Power(2, zoom);
  lat_rad := Arctan (Sinh (Pi * (1 - 2 * ytile / n)));
  lat_deg := RadtoDeg (lat_rad); }

  if ( (AYtile <> cErrTile)
    and (AYtile >= 0)
    and (AZoom <> cErrZoom)
    and (Abs(AZoom) <= cMaxZoom)
    and (AZoom >= cMinZoom) )
  then begin
    try
      Result := RadToDeg(Arctan(Sinh( Pi*(1 -(2*(AYtile / Power(2, AZoom)))) )));
    except
      Result := cErrCoord;
    end;
  end else begin
    Result := cErrCoord;
  end;
end;

function LatToYtile(ALatDec: Double; AZoom: Integer): Double;
begin
  { lat_rad := DegToRad(lat_deg);
    n := Power(2, zoom);
    ytile := Trunc((1 - (ln(Tan(lat_rad) + (1 /Cos(lat_rad))) / Pi)) / 2 * n); }

  if ( (ALatDec <> cErrCoord)
    and RangeLatOk(ALatDec)
    and (AZoom <> cErrZoom)
    and (Abs(AZoom) <= cMaxZoom)
    and (AZoom >= cMinZoom) )
  then begin
    Result := ((1 - (ln(Tan(DegToRad(ALatDec)) + (1/Cos(DegToRad(ALatDec))))/Pi)) / 2) * Power(2, AZoom);
  end else begin
    Result := cErrTile
  end;
end;

function LonToXtile(ALonDec: Double; AZoom: Integer): Double;
begin
  { lat_rad := DegToRad(lat_deg);
    n := Power(2, zoom);
    xtile := Trunc(((lon_deg + 180) / 360) * n); }

  if ( (ALonDec <> cErrCoord)
    and RangeLonOk(ALonDec)
    and (AZoom <> cErrZoom)
    and (Abs(AZoom) <= cMaxZoom)
    and (AZoom >= cMinZoom) )
  then begin
    Result := ((ALonDec + 180) / 360) * Power(2, AZoom);
  end else begin
    Result := cErrTile;
  end;
end;

function LatAddDistance(ALatDec: Double; AMeters: Integer): Double;
var
  Equator,
  MeterPerDeg,
  DegPerMeter: Double;
begin
  try
    Equator := cEquatorLength * Cos(DegToRad(ALatDec));
    MeterPerDeg := Equator / 360;
    DegPerMeter := 1 / MeterPerDeg;

    Result := ALatDec + (AMeters * DegPerMeter);

    if (not RangeLatOk(Result)) then begin
      if (ALatDec >= 0) then begin
        Result := cMaxAbsLat;
      end else begin
        Result := - cMaxAbsLat;
      end;
    end;
  except
    Result := cErrCoord;
  end;
end;

function LonAddDistance(ALonDec: Double; AMeters: Integer): Double;
var
  Meridian,
  MeterPerDeg,
  DegPerMeter: Double;
begin
  try
    Meridian := cMeridianLength;
    MeterPerDeg := Meridian / 360;
    DegPerMeter := 1 / MeterPerDeg;

    Result := ALonDec + (AMeters * DegPerMeter);

    if (not RangeLonOk(Result)) then begin
      if (ALonDec >= 0) then begin
        Result := cMaxAbsLon;
      end else begin
        Result := - cMaxAbsLon;
      end;
    end;
  except
    Result := cErrCoord;
  end;
end;

function CalculateCoordDistance(aFromLatDec: Double; aFromLonDec: Double; aToLatDec: Double; aToLonDec: Double): Double;
var
  dLat, dLon: Double;
  FromLatRad, ToLatRad: Double;
  a, c: Double;
begin
  Result := cErrDistance;

  if (aFromLatDec = cErrDistance)
    or (aFromLonDec = cErrDistance)
    or (aToLatDec = cErrDistance)
    or (aToLonDec = cErrDistance)
    //
    or (not RangeLonOk(aFromLonDec))
    or (not RangeLatOk(aFromLatDec))
    or (not RangeLonOk(aToLonDec))
    or (not RangeLatOk(aToLatDec))
  then begin
    EXIT;
  end;

  try
    dLat := DegToRad(aToLatDec - aFromLatDec);
    dLon := DegToRad(aToLonDec - aFromLonDec);

    FromLatRad := DegToRad(aFromLatDec);
    ToLatRad := DegToRad(aToLatDec);

    a :=  (Sin(dLat/2) * Sin(dLat/2))
      + ( (Sin(dLon/2) * Sin(dLon/2)) * Cos(FromLatRad) * Cos(ToLatRad) );
    c := 2 * ArcSin(Sqrt(a));

    Result := cEarthRadius * c;
  except
    Result := cErrDistance;
  end;
end;

function CalculateMapResolution(ALatDec: Double; AZoom: Integer): Double;
begin
  { http://wiki.openstreetmap.org/wiki/Slippy_map_tilenames#Tile_servers

    Exact length of the equator (according to wikipedia) is 40075.016686 km in
    WGS-84. A horizontal tile size at zoom 0 would be 156543.034 meters. Which
    gives us a formula to calculate resolution at any given zoom:

    resolution = (156543.034 meters/pixel * cos(latitude)) / (2 ^ zoomlevel)
  }
  if (ALatDec <> cErrCoord)
    and (AZoom <> cErrZoom)
  then begin
    Result := ( cEquatorLength/(cTileSize * Power(2, AZoom)) ) * Cos(DegToRad(ALatDec));
  end else begin
    Result := cErrResolution;
  end;
end;

function CalculatePixelRation(AFromZoom: Integer; AToZoom: Integer): Double;
begin
  Result := cErrPixel;

  if MapZoomIsOk(AFromZoom)
    and MapZoomIsOk(AToZoom)
  then begin
    if (AFromZoom = AToZoom) then begin
      Result := 1;
    end else begin
      Result := Power(2, AToZoom) / Power(2, AFromZoom);
    end;
  end;
end;

function TrimUrlBase(AUrl: string): string;
begin
  Delete(AUrl, 1, Pos('//', AUrl) + Length('//') - 1);
  Result := Trim(Copy(AUrl, 1, Pos('/', AUrl) - 1));
end;

function TrimTileExt(AUrl: string): string;
begin
  AUrl := Copy(AUrl, Length(AUrl) - 3, 4);
  if (Pos('.', AUrl) > 0) then begin
    Delete(AUrl, Pos('.', AUrl), 1);
  end;
  Result := Trim(AUrl);
end;

function FilePicToBitmap32(AFilePath: string; ABitmap: TBitmap32): Boolean;
var
  TmpGraphicClass: TGraphicClass;
  TmpGraphic: TGraphic;
  TmpPng: TPNGIMage;
  ImgData: TPersistent;
  AMS: TMemoryStream;
  AlphaChannelUsed: Boolean;
  TransparentColor: TColor32;
  PixelPtr: PColor32;
  AlphaPtr: PByte;
  X, Y: Integer;
begin
  Result := False;
  if (ABitmap = nil) then begin
    EXIT; //no bitmap, nothing to do
  end;

  AMS := TMemoryStream.Create;

  TmpGraphic := nil;
  TmpPng := nil;
  //ImgData := nil;
  AlphaChannelUsed := False;

  Result := True;

  try
    AMS.LoadFromFile(AFilePath);
    TmpGraphicClass := FileFormatList.GraphicFromContent(AMS);

    if (TmpGraphicClass = nil) then begin
      try //nejdriv zkousim JPG
        TmpGraphic := TJPEGImage.Create;
        AMS.Position := 0;
        TmpGraphic.LoadFromStream(AMS);
        ABitmap.Assign(TmpGraphic);
      except
        try  //pak BMP
          AMS.Position := 0;
          ABitmap.LoadFromStream(AMS);
        except
          Result := False;
        end;
      end;

    end else begin
      AMS.Position := 0;
      TmpGraphic := TmpGraphicClass.Create;

      if (TmpGraphic is TPNGGraphic) then begin
        TmpPng := TPngImage.Create;
        TmpPng.LoadFromStream(AMS);
        ImgData := TmpPng;
      end else begin
        TmpGraphic.LoadFromStream(AMS);
        ImgData := TmpGraphic;
      end;

      ABitmap.Assign(ImgData);

      if (TmpGraphic is TPNGGraphic) then begin //extra action for PNG

        ABitmap.ResetAlpha;
        case TmpPng.TransparencyMode of
          ptmPartial:
            begin
              if (TmpPng.Header.ColorType = COLOR_GRAYSCALEALPHA) or
                 (TmpPng.Header.ColorType = COLOR_RGBALPHA) then
              begin
                PixelPtr := PColor32(@ABitmap.Bits[0]);
                for Y := 0 to ABitmap.Height - 1 do
                begin
                  AlphaPtr := PByte(TmpPng.AlphaScanline[Y]);
                  for X := 0 to ABitmap.Width - 1 do
                  begin
                    PixelPtr^ := (PixelPtr^ and $00FFFFFF) or (TColor32(AlphaPtr^) shl 24);
                    Inc(PixelPtr);
                    Inc(AlphaPtr);
                  end;
                end;

                AlphaChannelUsed := True;
              end;
            end;
          ptmBit:
            begin
              TransparentColor := Color32(TmpPng.TransparentColor);
              PixelPtr := PColor32(@ABitmap.Bits[0]);
              for X := 0 to ABitmap.Height * ABitmap.Width - 1 do
              begin
                if PixelPtr^ = TransparentColor then
                  PixelPtr^ := PixelPtr^ and $00FFFFFF;
                Inc(PixelPtr);
              end;

              AlphaChannelUsed := True;
            end;
          ptmNone:
            AlphaChannelUsed := False;
        end; //end - case

        if AlphaChannelUsed then begin
          ABitmap.DrawMode := dmBlend;
        end else begin
          ABitmap.DrawMode := dmOpaque;
        end;
      end; //edn - TPNGImage

    end;

  finally
    FreeAndNil(AMS);

    {if (TmpGraphicClass <> nil) then begin
      TmpGraphicClass := nil;
    end;}
    if Assigned(TmpGraphic) then begin
      FreeAndNil(TmpGraphic);
    end;
    if (TmpPng <> nil) then begin
      FreeAndNil(TmpPng);
    end;
    {if (ImgData <> nil) then begin
      ImgData := nil
    end;}
  end;
end;

function StreamPicToBitmap32(AImgStream: TMemoryStream; ABitmap: TBitmap32): Boolean;
var
  TmpGraphicClass: TGraphicClass;
  TmpGraphic: TGraphic;
  TmpPng: TPNGIMage;
  ImgData: TPersistent;
  AMS: TMemoryStream;
  AlphaChannelUsed: Boolean;
  TransparentColor: TColor32;
  PixelPtr: PColor32;
  AlphaPtr: PByte;
  X, Y: Integer;
begin
  Result := False;
  if (ABitmap = nil) or (AImgStream = nil) then begin
    EXIT; //no bitmap or stream, nothing to do
  end;

  AMS := TMemoryStream.Create;

  TmpGraphic := nil;
  TmpPng := nil;
  //ImgData := nil;
  AlphaChannelUsed := False;

  Result := True;

  try
    TmpGraphicClass := FileFormatList.GraphicFromContent(AMS);

    if (TmpGraphicClass = nil) then begin
      try //nejdriv zkousim JPG
        TmpGraphic := TJPEGImage.Create;
        AMS.Position := 0;
        TmpGraphic.LoadFromStream(AMS);
        ABitmap.Assign(TmpGraphic);
      except
        try  //pak BMP
          AMS.Position := 0;
          ABitmap.LoadFromStream(AMS);
        except
          Result := False;
        end;
      end;

    end else begin
      AMS.Position := 0;
      TmpGraphic := TmpGraphicClass.Create;

      if (TmpGraphic is TPNGGraphic) then begin
        TmpPng := TPngImage.Create;
        TmpPng.LoadFromStream(AMS);
        ImgData := TmpPng;
      end else begin
        TmpGraphic.LoadFromStream(AMS);
        ImgData := TmpGraphic;
      end;

      ABitmap.Assign(ImgData);

      if (TmpGraphic is TPNGGraphic) then begin //extra action for PNG

        ABitmap.ResetAlpha;
        case TmpPng.TransparencyMode of
          ptmPartial:
            begin
              if (TmpPng.Header.ColorType = COLOR_GRAYSCALEALPHA) or
                 (TmpPng.Header.ColorType = COLOR_RGBALPHA) then
              begin
                PixelPtr := PColor32(@ABitmap.Bits[0]);
                for Y := 0 to ABitmap.Height - 1 do
                begin
                  AlphaPtr := PByte(TmpPng.AlphaScanline[Y]);
                  for X := 0 to ABitmap.Width - 1 do
                  begin
                    PixelPtr^ := (PixelPtr^ and $00FFFFFF) or (TColor32(AlphaPtr^) shl 24);
                    Inc(PixelPtr);
                    Inc(AlphaPtr);
                  end;
                end;

                AlphaChannelUsed := True;
              end;
            end;
          ptmBit:
            begin
              TransparentColor := Color32(TmpPng.TransparentColor);
              PixelPtr := PColor32(@ABitmap.Bits[0]);
              for X := 0 to ABitmap.Height * ABitmap.Width - 1 do
              begin
                if PixelPtr^ = TransparentColor then
                  PixelPtr^ := PixelPtr^ and $00FFFFFF;
                Inc(PixelPtr);
              end;

              AlphaChannelUsed := True;
            end;
          ptmNone:
            AlphaChannelUsed := False;
        end; //end - case

        if AlphaChannelUsed then begin
          ABitmap.DrawMode := dmBlend;
        end else begin
          ABitmap.DrawMode := dmOpaque;
        end;
      end; //edn - TPNGImage

    end;

  finally
    FreeAndNil(AMS);

    {if (TmpGraphicClass <> nil) then begin
      TmpGraphicClass := nil;
    end;}
    if Assigned(TmpGraphic) then begin
      FreeAndNil(TmpGraphic);
    end;
    if (TmpPng <> nil) then begin
      FreeAndNil(TmpPng);
    end;
    {if (ImgData <> nil) then begin
      ImgData := nil
    end;}
  end;
end;

function StreamPicToBitmap(AMS: TMemoryStream; ABitmap: TBitmap): Boolean;
var
  TmpGraphicClass: TGraphicClass;
  TmpGraphic: TGraphic;
  TmpPng: TPNGIMage;
  ImgData: TPersistent;
begin
  AMS.Position := 0;
  TmpGraphic := nil;
  TmpPng := nil;
  //ImgData := nil;

  Result := True;

  TmpGraphicClass := FileFormatList.GraphicFromContent(AMS);
  try
    if (TmpGraphicClass = nil) then begin
      try //nejdriv zkousim JPG
        TmpGraphic := TJPEGImage.Create;
        AMS.Position := 0;
        TmpGraphic.LoadFromStream(AMS);
        ABitmap.Assign(TmpGraphic);
      except
        try  //pak BMP
          AMS.Position := 0;
          ABitmap.LoadFromStream(AMS);
        except
          Result := False;
        end;
      end;

    end else begin
      AMS.Position := 0;
      TmpGraphic := TmpGraphicClass.Create;

      //downloaded PNG file is sometimes recognized as PCX ... why ?
      if (TmpGraphic is TPNGGraphic) or (TmpGraphic is TPCXGraphic) then begin
        TmpPng := TPngImage.Create;
        TmpPng.LoadFromStream(AMS);
        ImgData := TmpPng;
      end else begin
        TmpGraphic.LoadFromStream(AMS);
        ImgData := TmpGraphic;
      end;

      ABitmap.Assign(ImgData);
    end;
  finally
    {if (TmpGraphicClass <> nil) then begin
      TmpGraphicClass := nil;
    end;}
    if Assigned(TmpGraphic) then begin
      FreeAndNil(TmpGraphic);
    end;
    if (TmpPng <> nil) then begin
      FreeAndNil(TmpPng);
    end;
    {if (ImgData <> nil) then begin
      ImgData := nil
    end;}
  end;
end;

function BrighterColor(AColor: TColor32; APercent: Byte): TColor32;
var
  cRed, cGreen, cBlue: Byte;
begin
  cRed := GetRValue(AColor);
  cGreen := GetGValue(AColor);
  cBlue := GetBValue(AColor);

  (* a byte's range is from 0 to 255
     so Red, Green and Blue can have
     a value between 0 and 255 *)

  cRed := Round(cRed * APercent / 100) +
    Round(255 - APercent / 100 * 255);

  cGreen := Round(cGreen * APercent / 100) +
    Round(255 - APercent / 100 * 255);

  cBlue := Round(cBlue * APercent / 100) +
    Round(255 - APercent / 100 * 255);

  Result := RGB(cRed, cGreen, cBlue);
end;

function Blend(Color1, Color2: TColor; ABlendLevel: Byte): TColor;
var
  c1, c2: LongInt;
  r, g, b, v1, v2: byte;
begin
  ABlendLevel:= Round(2.55 * ABlendLevel);
  c1 := ColorToRGB(Color1);
  c2 := ColorToRGB(Color2);
  v1:= Byte(c1);
  v2:= Byte(c2);
  r:= ABlendLevel * (v1 - v2) shr 8 + v2;
  v1:= Byte(c1 shr 8);
  v2:= Byte(c2 shr 8);
  g:= ABlendLevel * (v1 - v2) shr 8 + v2;
  v1:= Byte(c1 shr 16);
  v2:= Byte(c2 shr 16);
  b:= ABlendLevel * (v1 - v2) shr 8 + v2;
  Result := (b shl 16) + (g shl 8) + r;
end;

function CorrectPlural(const AString: string; ACount: Integer; ALang: string = 'cz'): string;
begin
  if ((ALang = 'en') or (ALang = '')) then begin

    Result := IntToStr(ACount) + ' ' + AString;
    if (ACount <> 1) then begin
      Result := Result + 's';
    end;

  end else if (ALang = 'cz') then begin

    if (AString = 'rok') then begin
      if (ACount = 1) then begin
        Result := Format('%d %s', [ACount, 'rok']);
      end else if ((ACount > 1) and (ACount < 5)) then begin
        Result := Format('%d %s', [ACount, 'roky']);
      end else if (ACount > 4) then begin
        Result := Format('%d %s', [ACount, 'let']);
      end;
    end else if (AString = 't�den') then begin
      if (ACount = 1) then begin
        Result := Format('%d %s', [ACount, 't�den']);
      end else if ((ACount > 1) and (ACount < 5)) then begin
        Result := Format('%d %s', [ACount, 't�dny']);
      end else if (ACount > 4) then begin
        Result := Format('%d %s', [ACount, 't�dn�']);
      end;
    end else if (AString = 'den') then begin
      if (ACount = 1) then begin
        Result := Format('%d %s', [ACount, 'den']);
      end else if ((ACount > 1) and (ACount < 5)) then begin
        Result := Format('%d %s', [ACount, 'dny']);
      end else if (ACount > 4) then begin
        Result := Format('%d %s', [ACount, 'dn�']);
      end;
    end else if (AString = 'hodina') then begin
      if (ACount = 1) then begin
        Result := Format('%d %s', [ACount, 'hodina']);
      end else if ((ACount > 1) and (ACount < 5)) then begin
        Result := Format('%d %s', [ACount, 'hodiny']);
      end else if (ACount > 4) then begin
        Result := Format('%d %s', [ACount, 'hodin']);
      end;
    end else if (AString = 'minuta') then begin
      if (ACount = 1) then begin
        Result := Format('%d %s', [ACount, 'minuta']);
      end else if ((ACount > 1) and (ACount < 5)) then begin
        Result := Format('%d %s', [ACount, 'minuty']);
      end else if (ACount > 4) then begin
        Result := Format('%d %s', [ACount, 'minut']);
      end;
    end else if (AString = 'sekunda') then begin
      if (ACount = 1) then begin
        Result := Format('%d %s', [ACount, 'sekunda']);
      end else if ((ACount > 1) and (ACount < 5)) then begin
        Result := Format('%d %s', [ACount, 'sekundy']);
      end else if (ACount > 4) then begin
        Result := Format('%d %s', [ACount, 'sekund']);
      end;
    end;

  end;
end;

function HumanReadableTime(ADateTime: Double; ALang: string = 'cz'): string;
//Time in "TDateTime" format
const
  SecondsPerMinute = 60;
  SecondsPerHour = 60 * SecondsPerMinute;
  SecondsPerDay = 24 * SecondsPerHour;
  SecondsPerWeek = 7 * SecondsPerDay;
  SecondsPerYear = 365 * SecondsPerDay;
var
  Years, Weeks, Days, Hours, Minutes, Seconds: Int64;
  ATime: Double;
begin
  try
    ATime := ADateTime * 24 * 60 * 60; //time in seconds

    Years := Trunc(ATime/SecondsPerYear);
    ATime := ATime - Years*SecondsPerYear;
    Weeks := Trunc(ATime/SecondsPerWeek);
    ATime := ATime - Weeks*SecondsPerWeek;
    Days := Trunc(ATime/SecondsPerDay);
    ATime := ATime - Days*SecondsPerDay;
    Hours := Trunc(ATime/SecondsPerHour);
    ATime := ATime - Hours*SecondsPerHour;
    Minutes := Trunc(ATime/SecondsPerMinute);
    ATime := ATime - Minutes*SecondsPerMinute;
    Seconds := Trunc(ATime);

    if ((ALang = 'en') or (ALang = '')) then begin

      if (Years > 5000) then begin
        Result := IntToStr(Round(Years/1000))+' millennia';
      end else if (Years > 500) then begin
        Result := IntToStr(Round(Years/100))+' centuries';
      end else if (Years > 0) then begin
        Result := CorrectPlural('year', Years) + ' ' + CorrectPlural('week', Weeks);
      end else if (Weeks > 0) then begin
        Result := CorrectPlural('week', Weeks) + ' ' + CorrectPlural('day', Days);
      end else if (Days > 0) then begin
        Result := CorrectPlural('day', Days) + ' ' + CorrectPlural('hour', Hours);
      end else if (Hours > 0) then begin
        Result := CorrectPlural('hour', Hours) + ' ' + CorrectPlural('minute', Minutes);
      end else if (Minutes > 0) then begin
        Result := CorrectPlural('minute', Minutes);
      end else begin
        Result := CorrectPlural('second', Seconds);
      end;

    end else if (ALang = 'cz') then begin

      if (Years > 5000) then begin
        Result := IntToStr(Round(Years/1000))+' tis�cilet�';
      end else if (Years > 500) then begin
        Result := IntToStr(Round(Years/100))+' stolet�';
      end else if (Years > 0) then begin
        Result := CorrectPlural('rok', Years) + ' ' + CorrectPlural('t�den', Weeks);
      end else if (Weeks > 0) then begin
        Result := CorrectPlural('t�den', Weeks) + ' ' + CorrectPlural('den', Days);
      end else if (Days > 0) then begin
        Result := CorrectPlural('den', Days) + ' ' + CorrectPlural('hodina', Hours);
      end else if (Hours > 0) then begin
        Result := CorrectPlural('hodina', Hours) + ' ' + CorrectPlural('minuta', Minutes);
      end else if (Minutes > 0) then begin
        Result := CorrectPlural('minuta', Minutes);
      end else begin
        Result := CorrectPlural('sekunda', Seconds);
      end;

    end;

  except
    if ((ALang = 'en') or (ALang = '')) then begin
      Result := 'an eternity';
    end else if (ALang = 'cz') then begin
      Result := 'v��nost';
    end;
  end; //try
end;

function MapZoomIsOk(aMapZoom: Integer): Boolean;
begin
  Result := (aMapZoom >= cMinZoom) and (aMapZoom <= cMaxZoom);
end;

function GetLineEnd(aLineStart: TPoint; aAngle: Double; aMapResolution: Double; aLineMeterLength: Integer = cDefLineLengthMeters): TPoint;
var
  k: Double;
  q: Double;
  linePixelLength: Integer;
begin
  Result.X := cErrPixel;
  Result.Y := cErrPixel;

  //  y = k.x + q
  //  0 = k.x + q - y
  //
  //    k = tan(alfa)

  //compute line params
  k := Tan(DegToRad(aAngle));
  q := aLineStart.Y - (aLineStart.X * k);

  //compute line length in pixels
  linePixelLength := Round(aLineMeterLength / aMapResolution);

  //                        //
  //                      //
  //                     x  END
  //                   // |
  //             c   //   |
  //               //     |  b
  //             //       |
  //           //         |
  //          x --------- x
  // START  //     a
  //      //
  //    //

  if (Round(aAngle) = 90)
    or (Round(aAngle) = 270)
  then begin
    Result.X := aLineStart.X;
    if (Round(aAngle) = 90) then begin
      Result.Y := aLineStart.Y + linePixelLength;
    end else begin
      Result.Y := aLineStart.Y - linePixelLength;
    end;
  end else begin
    //compute end point
    Result.X := Round( aLineStart.X + Cos(DegToRad(aAngle)) * linePixelLength );
    Result.Y := Round(k * Result.X + q);
  end;
end;

function IsLineInRect(aLineStart: TPoint; aAngle: Double; aRect: TRect; aMapResolution: Double;
  var aLineEnd: TPoint; aLineMeterLength: Integer = cDefLineLengthMeters): Boolean;
var
  arrP: array[1..4] of Integer;
  arrQ: array[1..4] of Integer;
begin
  Result := False;

  aLineEnd := GetLineEnd(aLineStart, aAngle, aMapResolution, aLineMeterLength);

  // Liang-Barsky - check if line intersect with rectange ( https://www.geeksforgeeks.org/liang-barsky-algorithm/, https://en.wikipedia.org/wiki/Liang�Barsky_algorithm )
  arrP[1] := -(aLineEnd.X - aLineStart.X);
  arrP[2] := (aLineEnd.X - aLineStart.X);
  arrP[3] := -(aLineEnd.Y - aLineStart.Y);
  arrP[4] := (aLineEnd.Y - aLineStart.Y);

  arrQ[1] := aLineStart.X - aRect.Left;;
  arrQ[2] := aRect.Right - aLineStart.X;
  arrQ[3] := aLineStart.Y - aRect.Bottom;
  arrQ[4] := aRect.Top - aLineStart.Y;

  //check position
  if (not (
      ((arrP[1] = 0) and (arrQ[1] < 0))    //line is outside of LEFT boundary
      and ((arrP[2] = 0) and (arrQ[2] < 0))  //line is outside of RIGHT boundary
      and ((arrP[3] = 0) and (arrQ[3] < 0))  //line is outside of BOTTOM boundary
      and ((arrP[4] = 0) and (arrQ[4] < 0))  //line is outside of TOP boundary
    ))
  then begin
    //part of the line intersect wiht recangle
    Result := True;
  end
end;

end.
