(* uSimpleOsmMap.pas -- simple OpenStreetMap viewer
  version 0.0.2, 08.06.2020

  Copyright (C) 2014 Pavel Sala

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.

  Pavel Sala        
  email@salikovi.cz
*)

unit uSimpleOsmMap;

interface

uses
  Classes                 //TComponent
  ,Math                   //Power()
  ,uSimpleOsmMapConstFunc //constants and functions
  ,Windows                //cause of -> H2443 Inline function 'DeleteFile' has not been expanded because unit 'Windows' is not specified in USES list
  ,SyncObjs               //TCriticalSection
  ,Dialogs                //ShowMessage
  ,Forms                  //Application
  ,Controls               //TWinControl
  ,SysUtils               //FreeAndNil, StringReplace
  ,Graphics               //TColor
  ,Generics.Collections   //TObjectList<type>
  ,DB                     //TBlobField
  ,IBDatabase             //TIBDatabase, TIBTransaction
  ,IBQuery                //TIBQuery
  ,jpeg                   //TJpegImage
  ,GR32                   //TBitmap32
  ,GR32_VectorUtils       //circle
  ,GR32_Polygons          //TSampleFiller
  ,GR32_Image             //TImage32
  ,GR32_Layers            //TCustomLayer
  ,GR32_Backends          //cause of -> [DCC Hint] H2443 Inline function 'TBitmap32.GetCanvas' has not been expanded because unit 'GR32_Backends' is not specified in USES list
  ,GR32_ColorGradients    //TRadialGradientSampler
  ,GR32_Lines             //TLine32
  //,GR32_ArrowHeads        //arrows
  //,GR32_Misc              //rounded rectagle, TFloatRect utils, ...
  ,pngimage               //TPNGImage
  ,GraphicEx              //read images from stream
  ,httpsend               //THTTPSend
  ,synautil
  ,synacode
  ,Clipbrd                //clipboard
  ;

type
  TLayerAccess = class(TCustomLayer);

type
  TMapObjectKind = (mokUnknown = 0,
    moAll,
    mokGeneral,   //default map point type
    mokPOI,       //point of interest
    mokDraw       //drawings
  );
  TMapObjectDrawKind = (modkUnknown = 0,
    modkLine,
    modkCircle,
    modkRectangle
  );
  TMapObject = class
    private
      FOwner: TImage32;
      //
      FKind: TMapObjectKind;
      FDrawKind: TMapObjectDrawKind;
      // --
      FVisible: Boolean;
      FImgBitmap: TBitmap32;  //marker image in bitmap
      FName, FCaption: string;
      FPosition: TMapPosition;
      FColor: TColor;
      FBorderWidth: Integer;
      FRadius: Integer;
      FWidth: Integer;
      FHeight: Integer;
      FAngle: Double;
      FZoom: Integer; //zoom if DRAWING (retangle, circel) was added
      // --
      procedure ClearPosition;
      procedure CheckDrawingsParams;
      //
      function GetLonDec: Double;
      function GetLatDec: Double;
      function GetX: Integer;
      function GetY: Integer;
      // --
      procedure SetVisibility(AState: Boolean);
      procedure SetName(AName: string);
      procedure SetCaption(ACaption: string);
      procedure SetImgBitmap(ABitmap32: TBitmap32);
      procedure SetColor(AColor: TColor);
      procedure SetBorderWidth(aBorderWidht: Integer);
      procedure SetRadius(ARadius: Integer);
      procedure SetWidth(AWidth: Integer);
      procedure SetHeight(AHeight: Integer);
      procedure SetAngle(AAngle: Double);

      procedure Init;

    public
      constructor Create; reintroduce; overload;
      constructor Create(aOwner: TImage32); overload; virtual;

      {Use this contructor for clasic "circle" point}
      constructor CreateMapObject(AOwner: TImage32; AName: string; ACaption: string;
        ALatDec: Double; ALonDec: Double; AX: Integer = 0; AY: Integer = 0;
        AVisible: Boolean = True; AColor: TColor = clNone; ARadius: Integer = -1;
        AKind: TMapObjectKind = mokGeneral); overload; virtual;

      {Use this contructor for drawing point from image file}
      constructor CreateMapObject(AOwner: TImage32; AName: string; ACaption: string;
        ALatDec: Double; ALonDec: Double; AX: Integer = 0; AY: Integer = 0;
        AImgPath: string = ''; AVisible: Boolean = True;
        AKind: TMapObjectKind = mokGeneral); overload; virtual;

      {Use this contructor for drawing point from image stream}
      constructor CreateMapObject(AOwner: TImage32; AName: string; ACaption: string;
        ALatDec: Double; ALonDec: Double; AX: Integer = 0; AY: Integer = 0;
        AImgStream: TMemoryStream = nil; AVisible: Boolean = True;
        AKind: TMapObjectKind = mokGeneral); overload; virtual;

      {Use this contructor for drawing point from image bitmap}
      constructor CreateMapObject(AOwner: TImage32; AName: string; ACaption: string;
        ALatDec: Double; ALonDec: Double; AX: Integer = 0; AY: Integer = 0;
        AImgBitmap: TBitmap32 = nil; AVisible: Boolean = True;
        AKind: TMapObjectKind = mokGeneral); overload; virtual;

      {Use this contructor for add drawing object - line, rectangle, circe ...}
      constructor CreateMapObject(AOwner: TImage32; AMapZoom: Integer; AName: string; ACaption: string;
        ALatDec: Double; ALonDec: Double; AX: Integer = 0; AY: Integer = 0;
        ADrawKind: TMapObjectDrawKind = modkCircle; AColor: TColor = clNone;
        ARadius: Integer = -1; ABroderWidth: Integer = cBorderDefWidth; AWidth: Integer = -1;
        AHeight: Integer = -1; AAngle: Double = -1; AVisible: Boolean = True); overload; virtual;


      destructor Destroy; override;

      function SetLonLatDec(ALonDec, ALatDec: Double): Boolean;
      function SetPxPosition(AX, AY: Integer): Boolean;
      //
      property Kind: TMapObjectKind read FKind write Fkind;
      property DrawKind: TMapObjectDrawKind read FDrawKind write FDrawKind;
      //
      property LonDec: Double read GetLonDec;
      property LatDec: Double read GetLatDec;
      property X: Integer read GetX;
      property Y: Integer read GetY;
      //
      property Name: string read FName write SetName;
      property Caption: string read FCaption write SetCaption;
      //
      property Color: TColor read FColor write SetColor;
      property BorderWidth: Integer read FBorderWidth write SetBorderWidth;
      property Radius: Integer read FRadius write SetRadius;
      property Width: Integer read FWidth write SetWidth;
      property Height: Integer read FHeight write SetHeight;
      property Angle: Double read FAngle write SetAngle;
      property Zoom: Integer read FZoom write FZoom;
      //
      property Visible: Boolean read FVisible write SetVisibility;
      //
      property ImgBitmap: TBitmap32 read FImgBitmap write SetImgBitmap;
    end;

type TGetTileThread = class(TThread)
  private
    FWorking,
    FObsoleteTile: Boolean; //if arary is cleared with "EmptyRequestQueue"
        //tile downloading can be in progress, but this tile
        //is no needed now -> OnTileDownloaded will not be triggered
    FCS,
    FCSHTTP: TCriticalSection;
    FHTTP: THTTPSend;
    FDownloadedTile: TTile;
    FTileArray: TTilesArray;
    FTileProc: TTileProc;
    // --
    procedure ClearTileArray;
    procedure ClearTile(var ATile: TTile);
    function GetArrayLength: Integer;
    function GetDownloadedTile: TTile;
    procedure DeleteFirstArrayTile;
    function GetFirstTileDef: TTileDef;
    function DownloadTile(ATileDef: TTileDef): Boolean;
    procedure SetWoringState(AState: Boolean);
    function GetWorkingState: Boolean;
    function GetObsolete: Boolean;

  protected
    procedure Execute; override;

  public
    constructor Create; virtual;
    destructor Destroy; override;
    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
    // --
    procedure SetupProxy(AHost, APort, AUsr, APassword: string);
    procedure AddTileForDownload(ATileUrl: string; AXTile, AYTile, AZoom,
      APosXpx, APosYpx: Integer);
    procedure EmptyRequestQueue;
    // --
    property OnTileDownloaded: TTileProc read FTileProc write FTileProc;
    property DownloadedTile: TTile read GetDownloadedTile;
    property Working: Boolean read GetWorkingState;
end;

type TCachingThread = class(TThread)
  private
    FCS: TCriticalSection;
    FHTTP: THTTPSend;
    FCachingType: TCachingType;
    FDecLonLeft, FDecLatTop, FDecLonRight, FDecLatBottom: Double; //for rectangle caching
    FPointsArray: TDecLonLatPositionArray; //for point caching
    FDbFilePath: string;
    FIBQuerySearch,
    FIBQueryInsert,
    FIBQueryDelete: TIBQuery;
    FIBDatabase: TIBDatabase;
    FIBTransaction: TIBTransaction;
    FStartZoom, FStopZoom: Integer;
    FMeterRadius: Integer;
    // --
    FMapUrl, FLastError: string;
    FDownloading: Boolean;
    FProgressInfo: TProgressInfo;
    // --
    FProgressProc: TProgressProc;
    // --
    function ConnectDb(ADbFilePath: string): Boolean;
    procedure DisconnectDb;
    function DownloadAndSaveTile(ABaseMapUrl, ATileExt: string;
      AZoom, Ax, Ay: Integer): string;
    function ExistsTile(ABaseMapUrl, ATileExt: string;
      AZoom, Ax, Ay: Integer; ACacheTimeout: Integer = cCacheTimeOut): Boolean;
    // --
    function GetDbConnected: Boolean;
    procedure SetDbConnected(AState: Boolean);
    procedure SetDbPath(ADbFilePath: string);

    procedure SetCachingType(AType: TCachingType);
    function GetCachingType: TCachingType;

    //rectangle caching
    procedure SetDecLonLeft(ADecLonLeft: Double);
    procedure SetDecLonRight(ADecLonRight: Double);
    procedure SetDecLatTop(ADecLattop: Double);
    procedure SetDecLatBottom(ADecLatBottom: Double);

    //points caching
    procedure ClearPointsArray;
    function GetArrayLength: Integer;
    function GetArrayPoint(AID: Integer = -1): TDecLonLatPosition;

    procedure SetMeterRadius(AMeterRadius: Integer);

    procedure SetStartZoom(AZoom: Integer);
    procedure SetStopZoom(AZoom: Integer);
    function GetDownloading: Boolean;
    procedure SetDownloading(AState: Boolean);
    function GetProgressInfo: TProgressInfo;
    procedure SetProgressInfo(AProgress: Integer; ARemainTime: string;
      ALastTileUrl: string = cClrCacheStrInfo; ACntAll: Integer = cClrCacheIntInfo;
      ACntDown: Integer = cClrCacheIntInfo; AImgStream: TMemoryStream = nil);
    function GetMapUrl: string;
    procedure SetMapUrl(AMapUrl: string);
    function GetLastError: string;
    procedure SetLastError(AError: string);

  protected
    procedure Execute; override;

  public
    constructor Create; virtual;
    destructor Destroy; override;

    procedure AfterConstruction; override;
    procedure BeforeDestruction; override;
    // --
    procedure SetupProxy(AHost, APort, AUsr, APassword: string);
    // --
    function DefineRectangleForDownload(ADecLonLeft, ADecLonRight, ADecLatTop,
      ADecLatBottom: Double; AZoomStart, AZoomStop: Integer; AMapUrl: string): Boolean;
    function DefineLonLatDecPointsForDownload(ALonLatDecPoints: TDecLonLatPositionArray;
      AZoomStart, AZoomStop: Integer; AMeterRadius: Integer; AMapUrl: string): Boolean;
    // --
    function StartDownload: Boolean;
    function StopDownload: Boolean;
    // --
    property LastError: string read GetLastError;
    property DbConnected: Boolean read GetDbConnected write SetDbConnected;
    property Downloading: Boolean read GetDownloading;
    property ProgressInfo: TProgressInfo read GetProgressInfo;
    property DbFilePath: string read FDbFilePath write SetDbPath;
    // --
    property MapUrl: string read GetMapUrl;
    // --
    property OnProgress: TProgressProc read FProgressProc write FProgressProc;
end;

type TSimpleOsmMap = class(TImage32)
  private
    FTileDownloadThread: TGetTileThread;
    FCachingThread: TCachingThread;
    //
    FOsmMapUrl: string;
    FCacheTimeOut: Integer;
    FCenterPoint: TCenterPosition; //map center point
    FMousePoint: TMapPosition; //mouse point
    FZoom: Integer;
    FMapResolution: Double; //metr/pixel
    FMapObjectList: TObjectList<TMapObject>;
    FGPSPosition: TMapObject; //gps object
    FHTTP: THTTPSend;
    FIBDatabase: TIBDatabase;
    FIBTransaction: TIBTransaction;
    FIBQuerySearch,
    FIBQueryInsert,
    //FIBQueryUpdate,
    FIBQueryDelete,
    FIBQueryClear: TIBQuery;
    FLastError, FDbFilepath: string;
    FShowCoordinates,
    FShowMapObjects,
    FShowObjectsLabel,
    FUseCache,
    FWorkOffline,
    FMapLayerCanPaint,
    FObjectMerging: Boolean;
    FPanMap, FSelectArea: Boolean;
    FPanFrom, FSelectAreaFrom: TPoint;
    FShowMapZoomControl: Boolean;
    FShowMapMoveControl: Boolean;
    FMapControlType: TMapControlType;
    // --
    function CreateDb(ADbFilePath: string): string;
    function ConnectDb(ADbFilePath: string): string;
    procedure DisconnectDb;
    procedure SetDbFilePath(ADbFilePath: string);
    procedure ClearPosition(var APosition: TMapPosition);
    procedure ClearCenterPosition(var APosition: TCenterPosition);
    procedure ClearQuery(AQuery: TIBQuery);
    procedure CommitTransaction(ARestart: Boolean = False);
    procedure RollbackTransaction(ARestart: Boolean = False);

    procedure DrawMapScaleDist(ABitmapLayer: TBitmapLayer; AXPos, AYPos: Integer);
    procedure DrawSelectionRectangle(ABitmapLayer: TBitmapLayer; AXFrom, AYFrom, AXTo, AYTo: Integer);
    procedure DrawZoomControl(ABitmapLayer: TBitmapLayer; AXPos, AYPos: Integer);
    procedure DrawMoveControl(ABitmapLayer: TBitmapLayer; AXPos, AYPos: Integer);

    procedure SetMapCoordAndZoomFromRectangle(AX1, AY1, AX2, AY2: Integer);
    function GetTileUrl(Ax, Ay, AZoom: integer): string;
    procedure SaveTileToCache(Ax, Ay: Integer; ABaseMapUrl: string; APictureStream: TMemoryStream; ATimeStamp: TDateTime;
      AZoom: Integer = cErrZoom);
    function TileInCache(Ax, Ay: Integer; AZoom: Integer = cErrZoom; ACacheTimeout: Integer = cCacheTimeOut;
      ABaseMapUrl: string = ''): Boolean;
    procedure ClearCache(ACacheDayTimeout: Integer = cCacheTimeOut);
    function GetImage(Ax, Ay: Integer; ATileUrl: string; AImageBitmap: TBitmap; ALeftPx, ATopPx: Integer): Boolean;
    //
    function MapObjectInVisibleArea(AMapObject: TMapObject): Boolean;
    procedure DrawMapObject(ABitmap: TBitmap32; AMapObject: TMapObject);
    procedure DrawMapObjectDrawing(ABitmap: TBitmap32; AMapObject: TMapObject);
    procedure DrawMultiMapObject(ABitmap: TBitmap32; AMapObject: TMapObject);
    //
    procedure OnTileDownloadComplete; //get tile from thread
    //
    {function LatDecToPixelY(ALatDec: Double): Integer;
    function LonDecToPixelX(ALonDec: Double): Integer;}
    function CalculatePxX(APointLonDec: Double): Integer;
    function CalculatePxY(APointLatDec: Double): Integer;
    function PanMinDiffOk(AX: Integer; AY: Integer): Boolean;
    //
    procedure GetZoomBtnRect(var ARect: TRect);
    procedure GetZoomInRect(var ARect: TRect);
    procedure GetZoomOutRect(var ARect: TRect);
    //
    procedure GetMoveBtnRect(var ARect: TRect);
    procedure GetMoveTopRect(var ARect: TRect);
    procedure GetMoveBottomRect(var ARect: TRect);
    procedure GetMoveLeftRect(var ARect: TRect);
    procedure GetMoveRightRect(var ARect: TRect);
    //
    function MouseOverMapControl(AX: Integer; AY: Integer): Boolean;
    function MouseInRect(AX: Integer; AY: Integer; ARect: TRect): Boolean;
    function MouseOverBtnZoomIn(AX: Integer; AY: Integer): Boolean;
    function MouseOverBtnZoomOut(AX: Integer; AY: Integer): Boolean;
    function MouseOverBtnMoveTop(AX: Integer; AY: Integer): Boolean;
    function MouseOverBtnMoveBottom(AX: Integer; AY: Integer): Boolean;
    function MouseOverBtnMoveLeft(AX: Integer; AY: Integer): Boolean;
    function MouseOverBtnMoveRight(AX: Integer; AY: Integer): Boolean;
    function GetMapControlTypeUnderMouse(AX: Integer; AY: Integer): TMapControlType;
    //
    //procedure UpdateLayers(Shift: TShiftState; X, Y: Integer);
    procedure UpdateMapLayer(Shift: TShiftState; X, Y: Integer);
    procedure UpdateMapObjectLayer(Shift: TShiftState; X, Y: Integer);
    procedure UpdateDrawLayer(Shift: TShiftState; X, Y: Integer); //will called in UpdateMapObjectLayer
    procedure UpdateGpsLayer(Shift: TShiftState; X, Y: Integer);
    procedure UpdateCoordLayer(Shift: TShiftState; X, Y: Integer);
    procedure UpdateInfoLayer(Shift: TShiftState; X, Y: Integer);
    procedure UpdateMapControlLayer(Shift: TShiftState; X, Y: Integer);
    // --
    procedure SetCenterLonLatDec(ACenterLonDec, ACenterLatDec: Double);
    procedure SetZoom(AZoom: Integer); overload;
    procedure SetZoom(AZoom: Integer; AUseCenterCoord: Boolean); overload;
    procedure SetZoomInt(AZoom: Integer); //for internal usage, without "ShowMap" calling
    procedure SetMapResolution;
    procedure SetCacheTimeOut(ATimeOut: Integer);
    procedure SetShowCoordinates(AState: Boolean);
    procedure SetShowMapObjects(AState: Boolean);
    procedure SetMergeMapObject(AState: Boolean);
    procedure SetShowObjectsLabel(AState: Boolean);
    procedure SetShowMapZoomControl(AState: Boolean);
    procedure SetShowMapMoveControl(AState: Boolean);
    // --
    function CloneMapObject(AMO: TMapObject): TMapObject;
    function PointsCountFromCaption(ACaption: string): Integer;
    function MapObjectDistance(AMOFrom, AMOTo: TMapObject): Integer;
    function SetObjectPosition(AMapObject: TMapObject; AlonDec, ALatDec: Double): Boolean;
    //function SetObjectCaption(AMapObject: TMapObject; ACaption: string): Boolean;
    //function SetObjectName(AMapObject: TMapObject; AName: string): Boolean;
    //function SetObjectColor(AMapObject: TMapObject; AColor: TColor): Boolean;
    //function SetObjectSize(AMapObject: TMapObject; ASize: Integer): Boolean;
    // --
    procedure ZoomIn(ACenter: Boolean = False);
    procedure ZoomOut(ACenter: Boolean = False);
    procedure MoveLeft;
    procedure MoveRight;
    procedure MoveTop;
    procedure MoveBottom;

    procedure InitVariables(ADbFilePath: string);
    function ComponentStateOk: Boolean;

    function AddMapObject(AMapZoom: Integer; AName: string; ALatDec: Double; ALonDec: Double; ADrawKind: TMapObjectDrawKind;
      AWidth: Integer; AHeight: Integer; ARadius: Integer; AAngle: Double; AColor: TColor = clBlack;
      ACaption: string = ''; ABorderWidth: Integer = cBorderDefWidth; ALengthMeters: Integer = cDefLineLengthMeters;
      AVisibility: Boolean = True): TMapObject; overload;
    function GetMouseCoordinates: TMapPosition;

  protected
    procedure MouseMove(Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer); override;
    procedure MouseDown(Button: TMouseButton; Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer); override;
    procedure MouseUp(Button: TMouseButton; Shift: TShiftState; X, Y: Integer; Layer: TCustomLayer); override;
    procedure MouseLeave; override;
    function DoMouseWheel(Shift: TShiftState; WheelDelta: Integer; MousePos: TPoint): Boolean; override;

  public
    constructor Create(AOwner: TComponent); override;
    constructor CreateOsmMap(AOwner: TComponent; ADbFilePath: string); virtual;
    destructor Destroy; override;
    procedure Resize; override;
    //
    procedure SetupProxy(AHost, APort, AUsr, APassword: string);
    procedure ShowMap(ACenterLonDec: Double = cErrCoord; ACenterLatDec: Double = cErrCoord; AZoom: Integer = cErrZoom);
    //
    function SetGpsPosition(ALonDec, ALatDec: Double): Boolean;
    function SetGpsVisibility(AState: Boolean): Boolean;
    //
    function SetMapObjectPosition(AMapObject: TMapObject; AlonDec, ALatDec: Double): Boolean;
    function SetMapObjectVisibility(AMapObject: TMapObject; AVisibility: Boolean): Boolean;
    //
    procedure DisableMapObjectsRepaint;
    procedure EnableMapObjectsRepaint;

    function ClearMapObjectList(AKind: TMapObjectKind = moAll): Boolean;

    function FirstMapObject(AKind: TMapObjectKind = moAll): TMapObject;
    function NextMapObject(APrevMapObject: TMapObject; AKind: TMapObjectKind = moAll): TMapObject;

    function FindMapObject(AName: string; ACaption: string = ''; ALonDec: Double = cErrCoord;
      ALatDec: Double = cErrCoord; AKind: TMapObjectKind = moAll): TMapObject;
    procedure ZoomAtMapObject(AName: string; ACaption: string = ''; AZoom: Integer = 16;
      ALonDec: Double = cErrCoord; ALatDec: Double = cErrCoord; AKind: TMapObjectKind = moAll);

    function AddMapObject(AName: string; ALatDec: Double; ALonDec: Double; ACaption: string = '';
      AColor: TColor = clBlue; AVisibility: Boolean = True; ASize: Integer = 6;
      AKind: TMapObjectKind = mokGeneral): TMapObject; overload;
    //
    function AddMapObject(AName: string; ALatDec: Double; ALonDec: Double; AImgPath: string;
      ACaption: string = ''; AVisibility: Boolean = True; AKind: TMapObjectKind = mokGeneral): TMapObject; overload;
    //
    function AddMapObjectDrawLine(AName: string; ALatDec: Double; ALonDec: Double; AAngle: Double;
      ACaption: string = ''; AWidth: Integer = cDefLineWidth; AColor: TColor = clBlack;
      ALengthMeters: Integer = cDefLineLengthMeters): TMapObject;
    function AddMapObjectDrawCircle(AName: string; ALatDec: Double; ALonDec: Double; ARadius: Integer;
      ACaption: string = ''; AColor: TColor = clBlack; ABorderWidth: Integer = cBorderDefWidth): TMapObject;
    function AddMapObjectDrawRectangle(AName: string; ALatDec: Double; ALonDec: Double; AWidth: Integer;
      AHeight: Integer; ACaption: string = ''; AColor: TColor = clBlack; ABorderWidth: Integer = cBorderDefWidth): TMapObject;

    function DeleteMapObject(AMapObject: TMapObject): Boolean;

    procedure CopyToClipboard;

  published
    property LastError: string read FLastError;
    property DatabaseFilePath: string read FDbFilepath write SetDbFilePath;
    property CacheDayTimeout: Integer read FCacheTimeOut write SetCacheTimeOut;
    property UseCache: Boolean read FUseCache write FUseCache;
    property WorkOffline: Boolean read FWorkOffline write FWorkOffline;
    property OsmMapUrl: string read FOsmMapUrl write FOsmMapUrl;
    property GpsPosition: TMapObject read FGPSPosition;
    property CenterPoint: TCenterPosition read FCenterPoint;
    property MouseCoordinates: TMapPosition read GetMouseCoordinates;
    property Zoom: Integer read FZoom write SetZoom;
    property ShowCoordinates: Boolean read FShowCoordinates write SetShowCoordinates;
    property ShowMapObjects: Boolean read FShowMapObjects write SetShowMapObjects;
    property ShowObjectsLabel: Boolean read FShowObjectsLabel write SetShowObjectsLabel;
    property MergeMapObject: Boolean read FObjectMerging write SetMergeMapObject;
    property CachingThread: TCachingThread read FCachingThread;
    property ShowMapZoomControl: Boolean read FShowMapZoomControl write SetShowMapZoomControl;
    property ShowMapMoveControl: Boolean read FShowMapMoveControl write SetShowMapMoveControl;
end;

procedure Register;

implementation

procedure Register;
begin
  RegisterComponents('SaLIk', [TSimpleOsmMap]);
end;


{ TMapObject ----------------------------------------------------------------- }
constructor TMapObject.CreateMapObject(AOwner: TImage32; AName: string; ACaption: string;
  ALatDec: Double; ALonDec: Double; AX: Integer = 0; AY: Integer = 0;
  AVisible: Boolean = True; AColor: TColor = clNone; ARadius: Integer = -1;
  AKind: TMapObjectKind = mokGeneral);
begin
  Create(AOwner);

  FImgBitmap := nil;

  SetName(AName);
  SetCaption(ACaption);

  ClearPosition;
  SetLonLatDec(ALonDec, ALatDec);
  SetPxPosition(AX, AY);

  if AColor <> clNone then begin
    FColor := AColor;
  end else begin
    FColor := cPointDefColor;
  end;

  if ARadius <> -1 then begin
    FRadius := ARadius;
  end else begin
    FRadius := cPointDefRadius;
  end;

  if AKind in [mokUnknown, mokDraw] then begin
    AKind := mokGeneral;
  end;
  FKind := AKind;

  FVisible := AVisible;
end;

constructor TMapObject.CreateMapObject(AOwner: TImage32; AName: string; ACaption: string;
  ALatDec: Double; ALonDec: Double; AX: Integer = 0; AY: Integer = 0;
  AImgPath: string = ''; AVisible: Boolean = True; AKind: TMapObjectKind = mokGeneral);
begin
  Create(AOwner);

  if FileExists(AImgPath) then begin
    FImgBitmap := TBitmap32.Create;
    FImgBitmap.Clear(clNone32);
    FImgBitmap.CombineMode := cmBlend;
    FImgBitmap.DrawMode := dmBlend;
    FImgBitmap.OuterColor := clNone32;
    //
    FilePicToBitmap32(AImgPath, FImgBitmap);  //read image from file
    //
    FColor := clNone;
    FRadius := -1;
  end else begin  //image doesn't exist -> create clasic "circle" point
    FImgBitmap := nil;
    FColor := cPointDefColor;
    FRadius := cPointDefRadius;
  end;

  SetName(AName);
  SetCaption(ACaption);

  ClearPosition;
  SetLonLatDec(ALonDec, ALatDec);
  SetPxPosition(AX, AY);

  if AKind in [mokUnknown, mokDraw] then begin
    AKind := mokGeneral;
  end;
  FKind := AKind;

  FVisible := AVisible;
end;

constructor TMapObject.CreateMapObject(AOwner: TImage32; AName: string; ACaption: string;
  ALatDec: Double; ALonDec: Double; AX: Integer = 0; AY: Integer = 0;
  AImgStream: TMemoryStream = nil; AVisible: Boolean = True; AKind: TMapObjectKind = mokGeneral);
begin
  Create(AOwner);

  if (AImgStream <> nil) then begin
    FImgBitmap := TBitmap32.Create;
    FImgBitmap.Clear(clNone32);
    FImgBitmap.CombineMode := cmBlend;
    FImgBitmap.DrawMode := dmBlend;
    FImgBitmap.OuterColor := clNone32;
    //

    StreamPicToBitmap32(AImgStream, FImgBitmap);  //read image from file
    //
    FColor := clNone;
    FRadius := -1;
  end else begin  //image doesn't exist -> create clasic "circle" point
    FImgBitmap := nil;
    FColor := cPointDefColor;
    FRadius := cPointDefRadius;
  end;

  SetName(AName);
  SetCaption(ACaption);

  ClearPosition;
  SetLonLatDec(ALonDec, ALatDec);
  SetPxPosition(AX, AY);

  if AKind in [mokUnknown, mokDraw] then begin
    AKind := mokGeneral;
  end;
  FKind := AKind;

  FVisible := AVisible;
end;

constructor TMapObject.Create(aOwner: TImage32);
begin
  Create;

  FOwner := aOwner;
end;

constructor TMapObject.Create;
begin
  inherited Create;

  Init;
end;

constructor TMapObject.CreateMapObject(AOwner: TImage32; AName: string; ACaption: string;
  ALatDec: Double; ALonDec: Double; AX: Integer = 0; AY: Integer = 0;
  AImgBitmap: TBitmap32 = nil; AVisible: Boolean = True; AKind: TMapObjectKind = mokGeneral);
begin
  Create(AOwner);

  if (AImgBitmap <> nil) then begin
    FImgBitmap := TBitmap32.Create;
    FImgBitmap.Assign(AImgBitmap); //assign bitmap
    FColor := clNone;
    FRadius := -1;
  end else begin  //image doesn't exist -> create clasic "circle" point
    FImgBitmap := nil;
    FColor := cPointDefColor;
    FRadius := cPointDefRadius;
  end;

  SetName(AName);
  SetCaption(ACaption);

  ClearPosition;
  SetLonLatDec(ALonDec, ALatDec);
  SetPxPosition(AX, AY);

  if AKind in [mokUnknown, mokDraw] then begin
    AKind := mokGeneral;
  end;
  FKind := AKind;

  FVisible := AVisible;
end;

constructor TMapObject.CreateMapObject(AOwner: TImage32; AMapZoom: Integer; AName: string; ACaption: string;
  ALatDec: Double; ALonDec: Double; AX: Integer = 0; AY: Integer = 0;
  ADrawKind: TMapObjectDrawKind = modkCircle; AColor: TColor = clNone;
  ARadius: Integer = -1; ABroderWidth: Integer = cBorderDefWidth; AWidth: Integer = -1;
  AHeight: Integer = -1; AAngle: Double = -1; AVisible: Boolean = True);
begin
  Create(AOwner);

  SetName(AName);
  SetCaption(ACaption);

  ClearPosition;
  SetLonLatDec(ALonDec, ALatDec);
  SetPxPosition(AX, AY);

  FKind := mokDraw;
  FVisible := AVisible;

  //drawings params
  FDrawKind := ADrawKind;
  FColor := AColor;
  FBorderWidth := ABroderWidth;
  FRadius := ARadius;
  FWidth := AWidth;
  FHeight := AHeight;
  FAngle := AAngle;
  FZoom := AMapZoom;

  CheckDrawingsParams;
end;

destructor TMapObject.Destroy;
begin
  ClearPosition;

  if (FImgBitmap <> nil) then begin
    FreeAndNil(FImgBitmap);
  end;

  inherited Destroy;
end;

function TMapObject.SetLonLatDec(ALonDec, ALatDec: Double): Boolean;
begin
  if RangeLonOk(ALonDec)
    and RangeLatOk(ALatDec)
  then begin
    Result := True;
    FPosition.LonDec := ALonDec;
    FPosition.LatDec := ALatDec;
  end else begin
    Result := False;
    ClearPosition;
  end;
end;

function TMapObject.SetPxPosition(AX, AY: Integer): Boolean;
begin
  Result := True;
  FPosition.X := AX;
  FPosition.Y := AY;
end;

procedure TMapObject.SetVisibility(AState: Boolean);
begin
  if (FVisible <> AState) then begin
    FVisible := AState;
  end;
end;

procedure TMapObject.SetColor(AColor: TColor);
begin
  if (FColor <> AColor) then begin
    FColor := AColor;
  end;
end;

procedure TMapObject.SetRadius(ARadius: Integer);
begin
  if (FRadius <> ARadius) then begin
    FRadius := ARadius;
  end;
end;

procedure TMapObject.SetWidth(AWidth: Integer);
begin
  if (FWidth <> AWidth) then begin
    FWidth := AWidth;
  end;
end;

procedure TMapObject.SetHeight(AHeight: Integer);
begin
  if (FHeight <> AHeight) then begin
    FHeight := AHeight;
  end;
end;

procedure TMapObject.SetAngle(AAngle: Double);
begin
  if (FAngle <> AAngle) then begin
    while ((AAngle - 360) > 360) do begin
      AAngle := AAngle - 360;
    end;
    FAngle := AAngle;
  end;
end;

procedure TMapObject.SetBorderWidth(aBorderWidht: Integer);
begin
  if FBorderWidth <> aBorderWidht then begin
    FBorderWidth := aBorderWidht;
  end;
end;

procedure TMapObject.SetCaption(ACaption: string);
begin
  ACaption := StringReplace(ACaption, cCRLF, ' ', [rfReplaceAll]);

  if (FCaption <> ACaption) then begin
    FCaption := ACaption;
  end;
end;

procedure TMapObject.SetName(AName: string);
begin
  AName := StringReplace(AName, cCRLF, ' ', [rfReplaceAll]);

  if (FName <> AName) then begin
    FName := AName;
  end;
end;

procedure TMapObject.SetImgBitmap(ABitmap32: TBitmap32);
begin
  if (ABitmap32 <> nil) then begin
    if FImgBitmap = nil then begin
      FImgBitmap := TBitmap32.Create;
      FImgBitmap.Clear(clNone32);
    end;
    FImgBitmap.SetSize(ABitmap32.Width, ABitmap32.Height);
    FImgBitmap.Assign(ABitmap32);
  end else begin
    if (FImgBitmap <> nil) then begin
      FreeAndNil(FImgBitmap);
    end;
  end;
end;

procedure TMapObject.CheckDrawingsParams;
begin
  if FDrawKind in [modkUnknown] then begin
    FDrawKind := modkCircle;
  end;

  if FColor = clNone then begin
    FColor := cPointDefColor;
  end;

  if FBorderWidth <= 0 then begin
    FBorderWidth := cBorderDefWidth;
  end;

  if FDrawKind in [modkLine] then begin
    FZoom := cErrZoom; //line not use zoom
  end else begin
    if (not MapZoomIsOk(FZoom)) then begin
      FZoom := cErrZoom;
    end;
  end;

  if (FDrawKind = modkLine) then begin
    if FWidth = -1 then begin
      FWidth := cDefLineWidth;
    end;
    if FAngle = -1 then begin
      FAngle := cPointDefAngle;
    end;

    FHeight := -1;
    FRadius := -1;
  end else if (FDrawKind = modkCircle) then begin
    if FRadius = -1 then begin
      FRadius := cPointDefRadius;
    end;

    FWidth := -1;
    FHeight := -1;
    FAngle := -1;
  end else if (FDrawKind = modkRectangle) then begin
    if FWidth = -1 then begin
      FWidth := cPointDefWidth;
    end;
    if FHeight = -1 then begin
      FHeight := cPointDefHeight;
    end;

    FRadius := -1;
    FAngle := -1;
  end;
end;

procedure TMapObject.ClearPosition;
begin
  FPosition.LonDec := cErrCoord;
  FPosition.LatDec := cErrCoord;
  FPosition.X := cErrPixel;
  FPosition.Y := cErrPixel;
end;

function TMapObject.GetLonDec: Double;
begin
  Result := FPosition.LonDec;
end;

function TMapObject.GetLatDec: Double;
begin
  Result := FPosition.LatDec;
end;

function TMapObject.GetX: Integer;
begin
  Result := FPosition.X;
end;

function TMapObject.GetY: Integer;
begin
  Result := FPosition.Y;
end;

procedure TMapObject.Init;
begin
  FOwner := nil;
  FKind := mokUnknown;
  FDrawKind := modkUnknown;
  FVisible := False;
  FImgBitmap := nil;
  FName := '';
  FCaption := '';
  ClearPosition;
  FRadius := -1;
  FWidth := -1;
  FHeight := -1;
  FAngle := -1;
  FColor := clNone;
  FBorderWidth := cBorderDefWidth;
  FZoom := cErrZoom;
end;

{ TMapObject ----------------------------------------------------------------- }



{ TGetTileThread ------------------------------------------------------------- }
procedure TGetTileThread.ClearTileArray;
begin
  FCS.Acquire;
  try
    SetLength(FTileArray, 0);
  finally
    FCS.Release;
  end;
end;

procedure TGetTileThread.ClearTile(var ATile: TTile);
begin
  ATile.DownloadError := False;
  ATile.XTile := cErrTile;
  ATile.YTile := cErrTile;
  ATile.Zoom := cErrZoom;
  ATile.PosXpx := cErrPixel;
  ATile.PosYpx := cErrPixel;
  ATile.TileUrl := '';
  if (ATile.TileStream <> nil) then begin
    FreeAndNil(ATile.TileStream);
  end;
end;

function TGetTileThread.GetArrayLength: Integer;
begin
  FCS.Acquire;
  try
    if (FTileArray <> nil) then begin
      Result := Length(FTileArray);
    end else begin
      Result := -1;
    end;
  finally
    FCS.Release;
  end;
end;

function TGetTileThread.GetDownloadedTile: TTile;
begin
  FCS.Acquire;
  try
    Result.DownloadError := FDownloadedTile.DownloadError;
    Result.XTile := FDownloadedTile.XTile;
    Result.YTile := FDownloadedTile.YTile;
    Result.Zoom := FDownloadedTile.Zoom;
    Result.PosXpx := FDownloadedTile.PosXpx;
    Result.PosYpx := FDownloadedTile.PosYpx;
    Result.TileUrl := FDownloadedTile.TileUrl;
    if (FDownloadedTile.TileStream <> nil) then begin
      Result.TileStream := TMemoryStream.Create;
      FDownloadedTile.TileStream.Position := 0;
      Result.TileStream.LoadFromStream(FDownloadedTile.TileStream);
    end else begin
      Result.TileStream := nil;
    end;
  finally
    FCS.Release;
  end;
end;

procedure TGetTileThread.DeleteFirstArrayTile;
var
  TmpLength, I: Integer;
begin
  FCS.Acquire;
  try
    TmpLength := Length(FTileArray);
    if (TmpLength > 0) then begin
      if (TmpLength > 1) then begin
        for I := 1 to (TmpLength - 1) do begin
          FTileArray[i-1] := FTileArray[i];
        end;
      end;
      SetLength(FTileArray, TmpLength - 1);
    end;
  finally
    FCS.Release;
  end;
end;

function TGetTileThread.GetFirstTileDef: TTileDef;
begin
  Result.XTile := cErrTile;
  Result.YTile := cErrTile;
  Result.Zoom := cErrZoom;
  Result.PosXpx := cErrPixel;
  Result.PosYpx := cErrPixel;
  Result.TileUrl := '';

  FCS.Acquire;
  try
    if FObsoleteTile then begin
      FObsoleteTile := False;
    end;

    if (Length(FTileArray) > 0) then begin
      Result := FTileArray[0];
    end;
  finally
    FCS.Release;
  end;
end;

function TGetTileThread.DownloadTile(ATileDef: TTileDef): Boolean;
begin
  Result := False;

  if Self.Terminated then begin
    EXIT;
  end;

  FCSHTTP.Acquire;
  try

    if GetObsolete then begin
      EXIT;
    end;

    FHTTP.Clear;
    FHTTP.HTTPMethod('GET', ATileDef.TileUrl);

    if Self.Terminated or GetObsolete then begin
      EXIT;
    end;

    FCS.Acquire;
    try
      ClearTile(FDownloadedTile);

      if FHTTP.ResultCode = 200 then begin
        FDownloadedTile.TileUrl := ATileDef.TileUrl;
        FDownloadedTile.XTile := ATileDef.XTile;
        FDownloadedTile.YTile := ATileDef.YTile;
        FDownloadedTile.Zoom := ATileDef.Zoom;
        FDownloadedTile.PosXpx := ATileDef.PosXpx;
        FDownloadedTile.PosYpx := ATileDef.PosYpx;
        FDownloadedTile.TileStream := TMemoryStream.Create;
        FHTTP.Document.Position := 0;
        FDownloadedTile.TileStream.LoadFromStream(FHTTP.Document);
      end else begin
        FDownloadedTile.DownloadError := True;
      end;

      Result := (not FDownloadedTile.DownloadError) and (not FObsoleteTile);

    finally
      FCS.Release;
    end;

  finally
    FCSHTTP.Release;
  end;
end;

procedure TGetTileThread.SetWoringState(AState: Boolean);
begin
  FCS.Acquire;
  try
    if (FWorking <> AState) then begin
      FWorking := AState;
    end;
  finally
    FCS.Release;
  end;
end;

function TGetTileThread.GetWorkingState: Boolean;
begin
  Result := False;
  if (FCS <> nil) then begin
    FCS.Acquire;
    try
      Result := FWorking;
    finally
      FCS.Release;
    end;
  end;
end;

function TGetTileThread.GetObsolete: Boolean;
begin
  Result := False;
  if (FCS <> nil) then begin
    FCS.Acquire;
    try
      Result := FObsoleteTile;
    finally
      FCS.Release;
    end;
  end;
end;

constructor TGetTileThread.Create;
begin
  inherited Create(True);

  FObsoleteTile := False;

  Finalize(FDownloadedTile);
  ClearTile(FDownloadedTile);

  SetLength(FTileArray, 0);
  FTileProc := nil;

  FCS := TCriticalSection.Create;
  FCSHTTP := TCriticalSection.Create;

  FHTTP := THTTPSend.Create;
  FHTTP.UserAgent := 'Opera/9.51 (Windows NT 5.1; U; cs)';
  FHTTP.Timeout := 5000;
end;

destructor TGetTileThread.Destroy;
begin
  ClearTile(FDownloadedTile);
  Finalize(FDownloadedTile);

  ClearTileArray;
  FTileArray := nil;
  FTileProc := nil;

  FreeAndNil(FHTTP);
  FreeAndNil(FCSHTTP);
  FreeAndNil(FCS);

  inherited Destroy;
end;

procedure TGetTileThread.Execute;
var
  TmpTileDef: TTileDef;
begin
  SetWoringState(True);

  while (not Self.Terminated) do begin
    if (GetArrayLength <= 0) then begin
      Sleep(50);
    end else begin

      TmpTileDef := GetFirstTileDef;
      DeleteFirstArrayTile;

      if (TmpTileDef.TileUrl <> '') then begin
        if DownloadTile(TmpTileDef) then begin
          if (not Self.Terminated) then begin
            if Assigned(FTileProc) then begin
              Synchronize(FTileProc);
            end;
          end; //end - not terminated
        end; //end - downloaded
      end;

    end;
  end;

  Finalize(TmpTileDef);
  SetWoringState(False);
end;

procedure TGetTileThread.AfterConstruction;
begin
  SetWoringState(False);
  Self.FreeOnTerminate := False;
  Self.Start;
end;

procedure TGetTileThread.BeforeDestruction;
begin
  //
end;

procedure TGetTileThread.SetupProxy(AHost, APort, AUsr, APassword: string);
begin
  FCSHTTP.Acquire;
  try
    FHTTP.ProxyHost := AHost;
    FHTTP.ProxyPort := APort;
    FHTTP.ProxyUser := AUsr;
    FHTTP.ProxyPass := APassword;
  finally
    FCSHTTP.Release;
  end;
end;

procedure TGetTileThread.AddTileForDownload(ATileUrl: string; AXTile, AYTile,
  AZoom, APosXpx, APosYpx: Integer);
var
  TmpLength: Integer;
begin
  FCS.Acquire;
  try
    TmpLength := Length(FTileArray);
    SetLength(FTileArray, TmpLength + 1);

    FTileArray[TmpLength].XTile := AXTile;
    FTileArray[TmpLength].YTile := AYTile;
    FTileArray[TmpLength].Zoom := AZoom;
    FTileArray[TmpLength].PosXpx := APosXpx;
    FTileArray[TmpLength].PosYpx := APosYpx;
    FTileArray[TmpLength].TileUrl := ATileUrl;

  finally
    FCS.Release;
  end;
end;

procedure TGetTileThread.EmptyRequestQueue;
begin
  FCS.Acquire;
  try
    SetLength(FTileArray, 0);
    FObsoleteTile := True;
  finally
    FCS.Release;
  end;
end;
{ TGetTileThread ------------------------------------------------------------- }


{ TCachingThread ------------------------------------------------------------- }
function TCachingThread.ConnectDb(ADbFilePath: string): Boolean;
begin
  Result := False;

  try
    //ADbFilePath := 'localhost:' + ExtractFilePath(Application.ExeName) + 'db\file-name.fdb'; //clasic server
    //ADbFilePath := ExtractFilePath(Application.ExeName) + 'db\file-name.fdb'; //embedded server
    FIBDatabase.DatabaseName := ADbFilePath;
    FIBDatabase.DefaultTransaction := FIBTransaction;
    FIBDatabase.LoginPrompt := False;
    FIBDatabase.Params.Clear;
    FIBDatabase.Params.Add('user_name=' + cDbLogin);
    FIBDatabase.Params.Add('password=' + cDbPassword);
    FIBDatabase.Params.Add('lc_ctype=' + cDbEncoding);
    FIBDatabase.SQLDialect := 3;
    FIBDatabase.Open;

    if (FIBDatabase.Connected) then begin  //connected?

      if (not FIBTransaction.Active) then begin //start the transaction
        FIBTransaction.DefaultDatabase := FIBDatabase;
        //read commited data from other transactions
        FIBTransaction.Params.Clear;
        FIBTransaction.Params.Add('read_committed');
        FIBTransaction.Params.Add('rec_version');
        FIBTransaction.Params.Add('nowait');
        FIBTransaction.StartTransaction;
      end;

      if FIBTransaction.Active then begin //setup FIBQuery

        FIBQuerySearch.Database := FIBDatabase;
        FIBQuerySearch.Transaction := FIBDatabase.DefaultTransaction;
        FIBQuerySearch.Close;
        FIBQuerySearch.SQL.Text := 'SELECT ot.*, DATEDIFF(DAY, upd_ts, cast(''NOW'' as timestamp)) as day_age'
          + ' FROM osm_tiles ot'
          + ' WHERE (ot.X = :x)'
          + '   and (ot.Y = :y)'
          + '   and (ot.ZOOM = :zoom)'
          + '   and (ot.url_base = :url_base)'
          + '   and (ot.img_extension = :img_ext)';
        FIBQuerySearch.Prepare;

        FIBQueryInsert.Database := FIBDatabase;
        FIBQueryInsert.Transaction := FIBDatabase.DefaultTransaction;
        FIBQueryInsert.Close;
        FIBQueryInsert.SQL.Text := 'INSERT INTO osm_tiles'
          + ' (url_base, zoom, x, y, img_extension, img_data, upd_ts)'
          + ' VALUES (:url_base, :zoom, :x, :y, :img_extension, :img_data, :upd_ts)';
        FIBQueryInsert.Prepare;

        FIBQueryDelete.Database := FIBDatabase;
        FIBQueryDelete.Transaction := FIBDatabase.DefaultTransaction;
        FIBQueryDelete.Close;
        FIBQueryDelete.SQL.Text := 'DELETE FROM osm_tiles WHERE (id = :id)';
        FIBQueryDelete.Prepare;

        Result := True;
      end;
    end;

  except
    Result := False;
  end;
end;

procedure TCachingThread.DisconnectDb;
begin
  FIBQuerySearch.Close;
  FIBQuerySearch.SQL.Clear;

  FIBQueryInsert.Close;
  FIBQueryInsert.SQL.Clear;

  FIBQueryDelete.Close;
  FIBQueryDelete.SQL.Clear;

  if FIBTransaction.Active then begin
    FIBTransaction.Commit;
  end;

  if FIBDatabase.Connected then begin
    FIBDatabase.Close;
  end;
end;

function TCachingThread.ExistsTile(ABaseMapUrl, ATileExt: string;
  AZoom, Ax, Ay: Integer; ACacheTimeout: Integer = cCacheTimeOut): Boolean;
begin
  Result := False;

  FIBQuerySearch.Close;
  FIBQuerySearch.ParamByName('url_base').AsString := TrimUrlBase(ABaseMapUrl);
  FIBQuerySearch.ParamByName('zoom').AsInteger := AZoom;
  FIBQuerySearch.ParamByName('x').AsInteger := Ax;
  FIBQuerySearch.ParamByName('y').AsInteger := Ay;
  FIBQuerySearch.ParamByName('img_ext').AsString := ATileExt;
  FIBQuerySearch.Open;

  if FIBQuerySearch.RecordCount > 0 then begin

    if (ACacheTimeout <> cCacheTimeOut) then begin
      if FIBQuerySearch.FieldByName('day_age').AsInteger >= ACacheTimeout then begin

        //delete old tile
        FIBQueryDelete.Close;
        FIBQueryDelete.ParamByName('id').AsLargeInt := FIBQuerySearch.FieldByName('id').AsLargeInt;
        FIBQueryDelete.ExecSQL;

        Result := False; //download again

      end else begin
        Result := FIBQuerySearch.FieldByName('id').AsLargeInt > 0;
      end;
    end else begin
      Result := FIBQuerySearch.FieldByName('id').AsLargeInt > 0;
    end;

  end;

  //FIBTransaction.CommitRetaining;
end;

function TCachingThread.DownloadAndSaveTile(ABaseMapUrl, ATileExt: string;
  AZoom, Ax, Ay: Integer): string;
var
  ATileUrl: string;
begin
  ATileUrl := StringReplace(ABaseMapUrl, cTileX, IntToStr(Ax), [rfReplaceAll]);
  ATileUrl := StringReplace(ATileUrl, cTileY, IntToStr(Ay), [rfReplaceAll]);
  ATileUrl := StringReplace(ATileUrl, cTileZoom, IntToStr(AZoom), [rfReplaceAll]);
  Result := ATileUrl;

  if (not ExistsTile(ABaseMapUrl, ATileExt, AZoom, Ax, Ay)) then begin

    FHTTP.Clear;
    FHTTP.HTTPMethod('GET', ATileUrl);

    if FHTTP.ResultCode = 200 then begin
      FHTTP.Document.Position := 0;

      FIBQueryInsert.Close;
      FIBQueryInsert.ParamByName('url_base').AsString := TrimUrlBase(ABaseMapUrl);
      FIBQueryInsert.ParamByName('zoom').AsInteger := AZoom;
      FIBQueryInsert.ParamByName('x').AsInteger := Ax;
      FIBQueryInsert.ParamByName('y').AsInteger := Ay;
      FIBQueryInsert.ParamByName('img_extension').AsString := ATileExt;
      FIBQueryInsert.ParamByName('img_data').LoadFromStream(FHTTP.Document, ftBlob);
      FIBQueryInsert.ParamByName('upd_ts').AsDateTime := Now;
      FIBQueryInsert.ExecSQL;

      FIBTransaction.CommitRetaining;
    end else begin
      FLastError := Format('Error downloading file "%s"', [ATileUrl]);
    end;

  end;
end;

function TCachingThread.GetDbConnected: Boolean;
begin
  FCS.Acquire;
  try
    Result := FIBDatabase.Connected;
  finally
    FCS.Release;
  end;
end;

procedure TCachingThread.SetDbConnected(AState: Boolean);
begin
  FCS.Acquire;
  try
    if AState then begin
      //work with the FIBDatabase is always in the critical section, so i can acces it directly here
      if (not FIBDatabase.Connected) then begin
        ConnectDb(FDbFilePath);
      end;
    end else begin
      DisconnectDb;
    end;
  finally
    FCS.Release;
  end;
end;

procedure TCachingThread.SetDbPath(ADbFilePath: string);
var
  ConectState: Boolean;
begin
  if (not GetDownloading) then begin

    ConectState := GetDbConnected;

    FCS.Acquire;
    try
      DisconnectDb;
      FDbFilePath := ADbFilePath;
      if ConectState then begin
        ConnectDb(ADbFilePath);
      end;
    finally
      FCS.Release;
    end;

  end else begin
    SetLastError('Can not to change the database path - downloading is runnig!');
  end;
end;

procedure TCachingThread.SetCachingType(AType: TCachingType);
begin
  FCS.Acquire;
  try
    FCachingType := AType;
  finally
    FCS.Release;
  end;
end;

function TCachingThread.GetCachingType: TCachingType;
begin
  FCS.Acquire;
  try
    Result := FCachingType;
  finally
    FCS.Release;
  end;
end;

procedure TCachingThread.SetDecLonLeft(ADecLonLeft: Double);
begin
  if ADecLonLeft <> cErrCoord then begin
    if RangeLonOk(ADecLonLeft) then begin
      if (not GetDownloading) then begin
        FDecLonLeft := ADecLonLeft;
      end else begin
        SetLastError('Can not to change "DecLonLeft" value - downloading is runnig!');
      end;
    end else begin
      FDecLonLeft := cErrCoord;
      SetLastError(Format('Wrong LeftLongitude value "%f4.6"!', [ADecLonLeft]));
    end;
  end;
end;

procedure TCachingThread.SetDecLonRight(ADecLonRight: Double);
begin
  if ADecLonRight <> cErrCoord then begin
    if RangeLonOk(ADecLonRight) then begin
      if (not GetDownloading) then begin
        FDecLonRight := ADecLonRight;
      end else begin
        SetLastError('Can not to change "DecLonRight" value - downloading is runnig!');
      end;
    end else begin
      FDecLonRight := cErrCoord;
      SetLastError(Format('Wrong RightLongitude value "%f4.6"!', [ADecLonRight]));
    end;
  end;
end;

procedure TCachingThread.SetDecLatTop(ADecLatTop: Double);
begin
  if ADecLatTop <> cErrCoord then begin
    if RangeLatOk(ADecLatTop) then begin
      if (not GetDownloading) then begin
        FDecLatTop := ADecLatTop;
      end else begin
        SetLastError('Can not to change "DecLatTop" value - downloading is runnig!');
      end;
    end else begin
      FDecLatTop := cErrCoord;
      SetLastError(Format('Wrong TopLatitude value "%f4.6"!', [ADecLatTop]));
    end;
  end;
end;

procedure TCachingThread.SetDecLatBottom(ADecLatBottom: Double);
begin
  if ADecLatBottom <> cErrCoord then begin
    if RangeLatOk(ADecLatBottom) then begin
      if (not GetDownloading) then begin
        FDecLatBottom := ADecLatBottom;
      end else begin
        SetLastError('Can not to change "DecLatRight" value - downloading is runnig!');
      end;
    end else begin
      FDecLatBottom := cErrCoord;
      SetLastError(Format('Wrong BottomLatitude value "%f4.6"!', [ADecLatBottom]));
    end;
  end;
end;

procedure TCachingThread.ClearPointsArray;
begin
  FCS.Acquire;
  try
    SetLength(FPointsArray, 0);
  finally
    FCS.Release;
  end;
end;

function TCachingThread.GetArrayLength: Integer;
begin
  FCS.Acquire;
  try
    Result := Length(FPointsArray);
  finally
    FCS.Release;
  end;
end;

function TCachingThread.GetArrayPoint(AID: Integer = -1): TDecLonLatPosition;
begin
  Result.LonDec := cErrCoord;
  Result.LatDec := cErrCoord;

  FCS.Acquire;
  try
    if (AID <> -1) then begin
      //next item
      if (Length(FPointsArray) > AID) then begin
        Result := FPointsArray[AID];
      end;
    end else begin
      //first array item
      if (Length(FPointsArray) > 0) then begin
        Result := FPointsArray[0];
      end;
    end;
  finally
    FCS.Release;
  end;
end;

procedure TCachingThread.SetMeterRadius(AMeterRadius: Integer);
begin
  if (not GetDownloading) then begin
    if (AMeterRadius > 0) then begin
      FMeterRadius := AMeterRadius;
    end else begin
      SetLastError('"MeterRadius" must be higher than 0!');
    end;
  end else begin
    SetLastError('Can not to set "MeterRadius" value - downloading is runnig!');
  end;
end;

procedure TCachingThread.SetStartZoom(AZoom: Integer);
begin
  if AZoom <> cErrZoom then begin
    if MapZoomIsOk(AZoom) then begin
      if (not GetDownloading) then begin
        FStartZoom := AZoom;
      end else begin
        SetLastError('Can not to change "StartZoom" value - downloading is runnig!');
      end;
    end else begin
      FStartZoom := cErrZoom;
      SetLastError(Format('Wrong zoom value "%d"!', [AZoom]));
    end;
  end;
end;

procedure TCachingThread.SetStopZoom(AZoom: Integer);
begin
  if AZoom <> cErrZoom then begin
    if MapZoomIsOk(AZoom) then begin
      if (not GetDownloading) then begin
        FStopZoom := AZoom;
      end else begin
        SetLastError('Can not to change "StopZoom" value - downloading is runnig!');
      end;
    end else begin
      FStopZoom := cErrZoom;
      SetLastError(Format('Wrong zoom value "%d"!', [AZoom]));
    end;
  end;
end;

function TCachingThread.GetDownloading: Boolean;
begin
  FCS.Acquire;
  try
    Result := FDownloading;
  finally
    FCS.Release;
  end;
end;

procedure TCachingThread.SetDownloading(AState: Boolean);
begin
  FCS.Acquire;
  try
    FDownloading := AState;
  finally
    FCS.Release;
  end;
end;

function TCachingThread.GetProgressInfo: TProgressInfo;
begin
  FCS.Acquire;
  try
    Result.CntDwnTiles := FProgressInfo.CntDwnTiles;
    Result.CntAllTiles := FProgressInfo.CntAllTiles;
    Result.ProgressProc := FProgressInfo.ProgressProc;
    Result.LastTileUrl := FProgressInfo.LastTileUrl;
    Result.RemainTime := FProgressInfo.RemainTime;
    if (FProgressInfo.ImgStream <> nil) then begin
      Result.ImgStream := TMemoryStream.Create;
      FProgressInfo.ImgStream.Position := 0;
      Result.ImgStream.LoadFromStream(FProgressInfo.ImgStream);
    end else begin
      Result.ImgStream := nil;
    end;
  finally
    FCS.Release;
  end;
end;

procedure TCachingThread.SetProgressInfo(AProgress: Integer; ARemainTime: string;
  ALastTileUrl: string = cClrCacheStrInfo; ACntAll: Integer = cClrCacheIntInfo;
  ACntDown: Integer = cClrCacheIntInfo; AImgStream: TMemoryStream = nil);
begin
  FCS.Acquire;
  try
    FProgressInfo.ProgressProc := AProgress;

    if (AProgress = cClrCacheIntInfo) then begin

      FProgressInfo.LastTileUrl := cClrCacheStrInfo;
      FProgressInfo.CntAllTiles := cClrCacheIntInfo;
      FProgressInfo.CntDwnTiles := cClrCacheIntInfo;
      FProgressInfo.RemainTime := '';

      if FProgressInfo.ImgStream <> nil then begin
        FProgressInfo.ImgStream.Clear;
        FreeAndNil(FProgressInfo.ImgStream);
      end;

    end else begin
      if (ALastTileUrl <> cClrCacheStrInfo) then begin
        FProgressInfo.LastTileUrl := ALastTileUrl;
      end;

      if (ACntAll <> cClrCacheIntInfo) then begin
        FProgressInfo.CntAllTiles := ACntAll;
      end;

      if (ACntDown <> cClrCacheIntInfo) then begin
        FProgressInfo.CntDwnTiles := ACntDown;
      end;

      if (ARemainTime <> '') then begin
        FProgressInfo.RemainTime := ARemainTime;
      end;

      if (AImgStream <> nil) and (AImgStream.Size > 0) then begin
        if FProgressInfo.ImgStream = nil then begin
          FProgressInfo.ImgStream := TMemoryStream.Create;
        end else begin
          FProgressInfo.ImgStream.Clear;
        end;
        AImgStream.Position := 0;
        FProgressInfo.ImgStream.LoadFromStream(AImgStream);
      end else begin
        if FProgressInfo.ImgStream <> nil then begin
          FProgressInfo.ImgStream.Clear;
          FreeAndNil(FProgressInfo.ImgStream);
        end;
      end;

    end;

  finally
    FCS.Release;
  end;
end;

function TCachingThread.GetMapUrl: string;
begin
  FCS.Acquire;
  try
    Result := FMapUrl;
  finally
    FCS.Release;
  end;
end;

procedure TCachingThread.SetMapUrl(AMapUrl: string);
begin
  if (not GetDownloading) then begin
    FCS.Acquire;
    try
      FMapUrl := AMapUrl;
    finally
      FCS.Release;
    end;
  end else begin
    SetLastError('Can not to change "MapUrl" value - downloading is runnig!');
  end;
end;

function TCachingThread.GetLastError: string;
begin
  FCS.Acquire;
  try
    Result := FLastError;
  finally
    FCS.Release;
  end;
end;

procedure TCachingThread.SetLastError(AError: string);
begin
  FCS.Acquire;
  try
    FLastError := AError;
  finally
    FCS.Release;
  end;
end;

constructor TCachingThread.Create;
begin
  inherited Create(True);

  FCS := TCriticalSection.Create;
  FHTTP := THTTPSend.Create;
  FHTTP.UserAgent := 'Opera/9.51 (Windows NT 5.1; U; cs)';
  FHTTP.Timeout := 5000;

  FIBQuerySearch := TIBQuery.Create(nil);
  FIBQueryInsert := TIBQuery.Create(nil);
  FIBQueryDelete := TIBQuery.Create(nil);

  FIBDatabase := TIBDatabase.Create(nil);
  FIBTransaction := TIBTransaction.Create(nil);
end;

destructor TCachingThread.Destroy;
begin
  DisconnectDb;

  SetLength(FPointsArray, 0);
  FPointsArray := nil;

  SetProgressInfo(-1, ''); //free memory stream, if it's assigned
  Finalize(FProgressInfo);

  FreeAndNil(FIBQuerySearch);
  FreeAndNil(FIBQueryInsert);
  FreeAndNil(FIBQueryDelete);

  FreeAndNil(FIBTransaction);
  FreeAndNil(FIBDatabase);

  FreeAndNil(FHTTP);
  FreeAndNil(FCS);

  inherited Destroy;
end;

procedure TCachingThread.Execute;
const
  MeasCountInterval: Integer = 20;
var
  MaxProgressCount,
  CurrentProgressCount,
  IntZoom,
  IntX, IntY, I: Integer;
  TileExt,
  BaseMapUrl,
  LastTileUrl: string;
  // --
  LastPointPos: TDecLonLatPosition;
  // --
  StarMeasTime, StopMeasTime, LastMeasTime, LastRemainTime: Double;
  XLonLeft, XLonRight, YLatTop, YLatBottom: Double;
  MeasTimeCount: Integer;
begin
  while (not Self.Terminated) do begin

    if GetDownloading then begin

      if GetCachingType = ctRectange then begin
        LastRemainTime := -1;
        LastMeasTime := -1;
        MeasTimeCount := 0;
        CurrentProgressCount := 0;
        MaxProgressCount := 0;

        for IntZoom := FStartZoom to FStopZoom do begin
          XLonLeft := LonToXtile(FDecLonLeft, IntZoom);
          XLonRight := LonToXtile(FDecLonRight, IntZoom);
          YLatTop := LatToYtile(FDecLatTop, IntZoom);
          YLatBottom := LatToYtile(FDecLatBottom, IntZoom);

          MaxProgressCount := MaxProgressCount
            + ((Trunc(XLonRight) - Trunc(XLonLeft) + 1) * (Trunc(YLatBottom) - Trunc(YLatTop) + 1) );
        end;

        BaseMapUrl := GetMapUrl;
        TileExt := TrimTileExt(BaseMapUrl);

        for IntZoom := FStartZoom to FStopZoom do begin

          XLonLeft := LonToXtile(FDecLonleft, IntZoom);
          XLonRight := LonToXtile(FDecLonRight, IntZoom);
          YLatTop := LatToYtile(FDecLatTop, IntZoom);
          YLatBottom := LatToYtile(FDecLatBottom, IntZoom);

          for IntX := Trunc(XLonLeft) to Trunc(XLonRight) do begin
            for IntY := Trunc(YLatTop) to Trunc(YLatBottom) do begin

              CurrentProgressCount := CurrentProgressCount + 1;

              StarMeasTime := Now;
              LastTileUrl := DownloadAndSaveTile(BaseMapUrl, TileExt, IntZoom, IntX, IntY);
              StopMeasTime := Now;

              if (LastRemainTime < 0) then begin
                LastMeasTime := (StopMeasTime - StarMeasTime);
              end else begin
                LastMeasTime := LastMeasTime + (StopMeasTime - StarMeasTime);
              end;

              if (MeasTimeCount = 0) then begin
                LastRemainTime := LastMeasTime * (MaxProgressCount - CurrentProgressCount);
              end else begin
                if (MeasTimeCount = MeasCountInterval) then begin
                  MeasTimeCount := 0;
                  LastMeasTime := (LastMeasTime / MeasCountInterval);
                  LastRemainTime := LastMeasTime * (MaxProgressCount - CurrentProgressCount);
                end;
              end;
              MeasTimeCount := MeasTimeCount + 1;

              if FHTTP.ResultCode = 200 then begin //image downloaded succesfully
                SetProgressInfo(Trunc((CurrentProgressCount/MaxProgressCount) * 100),
                  HumanReadableTime(LastRemainTime),
                  LastTileUrl, MaxProgressCount, CurrentProgressCount,
                  FHTTP.Document);
              end else begin //error during downloading
                SetProgressInfo(Trunc((CurrentProgressCount/MaxProgressCount) * 100),
                  HumanReadableTime(LastRemainTime),
                  LastTileUrl, MaxProgressCount, CurrentProgressCount,
                  nil);
              end;

              if Assigned(FProgressProc) then begin
                Synchronize(FProgressProc);
              end;

              if (not GetDownloading) or (Self.Terminated) then begin //end downloading ?
                BREAK;
              end;
            end; //edt-for-lat_to_y

            //save changes and clear temporary data in database (for performance)
            FIBTransaction.Commit;
            FIBTransaction.StartTransaction;
            // --
            if (not GetDownloading) or (Self.Terminated) then begin //end downloading ?
              BREAK;
            end;
          end;  //end-for-lon_to_y

          // --
          if (not GetDownloading) or (Self.Terminated) then begin //end downloading ?
            BREAK;
          end;
        end;  //end-for-zoom

        if GetDownloading then begin
          SetDownloading(False); //downloading finished
        end;

      end else if (GetCachingType = ctPoints) then begin  //stahovani obrazku kolem bodu v rozsahu zoomu
        LastRemainTime := -1;
        LastMeasTime := -1;
        MeasTimeCount := 0;
        CurrentProgressCount := 0;
        MaxProgressCount := 0;

        for I := 0 to (GetArrayLength - 1) do begin
          LastPointPos := GetArrayPoint(I);

          if ((LastPointPos.LonDec <> cErrCoord) and (LastPointPos.LatDec <> cErrCoord)) then begin
            for IntZoom := FStartZoom to FStopZoom do begin

              XLonLeft := LonToXtile(LonAddDistance(LastPointPos.LonDec, -FMeterRadius), IntZoom);
              XLonRight := LonToXtile(LonAddDistance(LastPointPos.LonDec, FMeterRadius), IntZoom);
              YLatTop := LatToYtile(LatAddDistance(LastPointPos.LatDec, FMeterRadius), IntZoom);
              YLatBottom := LatToYtile(LatAddDistance(LastPointPos.LatDec, -FMeterRadius), IntZoom);

              MaxProgressCount := MaxProgressCount + ((Trunc(XLonRight) - Trunc(XLonLeft) + 1) * (Trunc(YLatBottom) - Trunc(YLatTop) + 1));
            end;
          end;
        end; //end for

        BaseMapUrl := GetMapUrl;
        TileExt := TrimTileExt(BaseMapUrl);

        for I := 0 to (GetArrayLength - 1) do begin
          LastPointPos := GetArrayPoint(I);
          if ((LastPointPos.LonDec <> cErrCoord) and (LastPointPos.LatDec <> cErrCoord)) then begin //bod ma platne souradnice

            for IntZoom := FStartZoom to FStopZoom do begin

              XLonLeft := LonToXtile(LonAddDistance(LastPointPos.LonDec, -FMeterRadius), IntZoom);
              XLonRight := LonToXtile(LonAddDistance(LastPointPos.LonDec, FMeterRadius), IntZoom);
              YLatTop := LatToYtile(LatAddDistance(LastPointPos.LatDec, FMeterRadius), IntZoom);
              YLatBottom := LatToYtile(LatAddDistance(LastPointPos.LatDec, - FMeterRadius), IntZoom);

              for IntX := Trunc(XLonLeft) to Trunc(XLonRight) do begin
                for IntY := Trunc(YLatTop) to Trunc(YLatBottom) do begin

                  CurrentProgressCount := CurrentProgressCount + 1;

                  StarMeasTime := Now;
                  LastTileUrl := DownloadAndSaveTile(BaseMapUrl, TileExt, IntZoom, IntX, IntY);
                  StopMeasTime := Now;

                  if (LastRemainTime < 0) then begin
                    LastMeasTime := (StopMeasTime - StarMeasTime);
                  end else begin
                    LastMeasTime := LastMeasTime + (StopMeasTime - StarMeasTime);
                  end;

                  if (MeasTimeCount = 0) then begin
                    LastRemainTime := LastMeasTime * (MaxProgressCount - CurrentProgressCount);
                  end else begin
                    if (MeasTimeCount = MeasCountInterval) then begin
                      MeasTimeCount := 0;
                      LastMeasTime := (LastMeasTime / MeasCountInterval);
                      LastRemainTime := LastMeasTime * (MaxProgressCount - CurrentProgressCount);
                    end;
                  end;
                  MeasTimeCount := MeasTimeCount + 1;

                  if FHTTP.ResultCode = 200 then begin //image downloaded succesfully
                    SetProgressInfo(Trunc((CurrentProgressCount/MaxProgressCount) * 100),
                      HumanReadableTime(LastRemainTime),
                      LastTileUrl, MaxProgressCount, CurrentProgressCount,
                      FHTTP.Document);
                  end else begin //error during downloading
                    SetProgressInfo(Trunc((CurrentProgressCount/MaxProgressCount) * 100),
                      HumanReadableTime(LastRemainTime),
                      LastTileUrl, MaxProgressCount, CurrentProgressCount,
                      nil);
                  end;

                  if Assigned(FProgressProc) then begin
                    Synchronize(FProgressProc);
                  end;

                  if (not GetDownloading) or (Self.Terminated) then begin //end downloading ?
                    BREAK;
                  end;
                end; //edt-for-lat_to_y

                //save changes and clear temporary data in database (for performance)
                FIBTransaction.Commit;
                FIBTransaction.StartTransaction;
                // --
                if (not GetDownloading) or (Self.Terminated) then begin //end downloading ?
                  BREAK;
                end;
              end;  //end-for-lon_to_y

              // --
              if (not GetDownloading) or (Self.Terminated) then begin //end downloading ?
                BREAK;
              end;
            end;  //end-for-zoom

          end;  //end - bod nema platne souradnice
        end; //end for

        if GetDownloading then begin
          SetDownloading(False); //downloading finished
        end;

      end else if (GetCachingType = ctUndefined) then begin
        //caching type undefined
      end;

    end else begin  //end - downloading
      Sleep(200);
    end;

  end;  //end-while
end;

procedure TCachingThread.AfterConstruction;
begin
  Self.FreeOnTerminate := False;
  FProgressProc := nil;
  FDbFilePath := '';

  SetDownloading(False);

  SetProgressInfo(cClrCacheIntInfo, '');
  SetLastError('');
  SetCachingType(ctUndefined); //caching type

  // --- parameters for points downloading
  SetLength(FPointsArray, 0);
  // --- parameters for points downloading

  // --- parameters for downloading
  FMapUrl := '';
  FStartZoom := cErrZoom;
  FStopZoom := cErrZoom;
  FMeterRadius := cCachingRadius;
  FDecLonLeft := cErrCoord;
  FDecLatTop := cErrCoord;
  FDecLonRight := cErrCoord;
  FDecLatBottom := cErrCoord;
  // --- parameters for downloading

  Self.Start; //start suspended thread
end;

procedure TCachingThread.BeforeDestruction;
begin
  //
end;

procedure TCachingThread.SetupProxy(AHost, APort, AUsr, APassword: string);
begin
  FCS.Acquire;
  try
    FHTTP.ProxyHost := AHost;
    FHTTP.ProxyPort := APort;
    FHTTP.ProxyUser := AUsr;
    FHTTP.ProxyPass := APassword;
  finally
    FCS.Release;
  end;
end;

function TCachingThread.DefineRectangleForDownload(ADecLonLeft, ADecLonRight,
  ADecLatTop, ADecLatBottom: Double; AZoomStart, AZoomStop: Integer;
  AMapUrl: string): Boolean;
begin
  Result := True;

  ClearPointsArray;
  SetLastError('');

  SetMapUrl(AMapUrl);
  Result := Result and (GetLastError = '');

  SetStartZoom(AZoomStart);
  Result := Result and (GetLastError = '');

  SetStopZoom(AZoomStop);
  Result := Result and (GetLastError = '');

  SetDecLonLeft(ADecLonLeft);
  Result := Result and (GetLastError = '');

  SetDecLonRight(ADecLonRight);
  Result := Result and (GetLastError = '');

  SetDecLatTop(ADecLatTop);
  Result := Result and (GetLastError = '');

  SetDecLatBottom(ADecLatBottom);
  Result := Result and (GetLastError = '');

  if Result then begin
    SetCachingType(ctRectange);
  end else begin
    SetCachingType(ctUndefined);
  end;
end;

function TCachingThread.DefineLonLatDecPointsForDownload(ALonLatDecPoints: TDecLonLatPositionArray;
  AZoomStart, AZoomStop: Integer; AMeterRadius: Integer; AMapUrl: string): Boolean;
var
  I: Integer;
begin
  Result := True;

  ClearPointsArray;
  SetLastError('');

  SetMapUrl(AMapUrl);
  Result := Result and (GetLastError = '');

  SetStartZoom(AZoomStart);
  Result := Result and (GetLastError = '');

  SetStopZoom(AZoomStop);
  Result := Result and (GetLastError = '');

  SetMeterRadius(AMeterRadius);
  Result := Result and (GetLastError = '');

  FCS.Acquire;
  try
    try
      SetLength(FPointsArray, Length(ALonLatDecPoints));
      for I := 0 to (Length(ALonLatDecPoints) - 1) do begin
        FPointsArray[I] := ALonLatDecPoints[I];
      end; //end - for
    except
      Result := False;
    end;
  finally
    FCS.Release;
  end;

  if Result then begin
    SetCachingType(ctPoints);
  end else begin
    SetCachingType(ctUndefined);
  end;
end;

function TCachingThread.StartDownload: Boolean;
begin
  Result := False;

  if GetDbConnected then begin
    if (not GetDownloading) then begin

      if (GetCachingType = ctRectange) then begin //rectangle caching

        if (GetMapUrl <> '')
          and MapZoomIsOk(FStartZoom)
          and MapZoomIsOk(FStopZoom)
          and (FStartZoom <= FStopZoom)
          and (FDecLonLeft <> cErrCoord) and RangeLonOk(FDecLonLeft)
          and (FDecLonRight <> cErrCoord) and RangeLonOk(FDecLonRight)
          and (FDecLatTop <> cErrCoord) and RangeLatOk(FDecLatTop)
          and (FDecLatBottom <> cErrCoord) and RangeLatOk(FDecLatBottom)
        then begin
          if (FDecLonLeft > FDecLonRight)     //from -180 to 180, see "cMaxAbsLon"
            or (FDecLatTop < FDecLatBottom)   //from -85.0511 to 85.0511, see "cMaxAbsLat"
          then begin
            Result := False;
            SetLastError('LeftTop coordinates must be "lower" then RightBottom coordinates!');
          end else begin
            SetProgressInfo(cClrCacheIntInfo, '');
            SetDownloading(True);
            Result := True;
          end;
        end else begin
          Result := False;
          SetLastError('Some of the downloads parameters are undefined!');
        end;

      end else if (GetCachingType = ctPoints) then begin

        if (GetMapUrl <> '')
          and MapZoomIsOk(FStartZoom)
          and MapZoomIsOk(FStopZoom)
          and (FStartZoom <= FStopZoom)
          and (GetArrayLength > 0)
        then begin
          SetProgressInfo(cClrCacheIntInfo, '');
          SetDownloading(True);
          Result := True;
        end else begin
          Result := False;
          SetLastError('Some of the downloads parameters are undefined!');
        end;

      end else if (GetCachingType = ctUndefined) then begin

        Result := False;
        SetLastError('Some of the downloads parameters are undefined!');

      end;

    end else begin
      Result := False;
      SetLastError('Downloading is running!');
    end;
  end; //db is connected
end;

function TCachingThread.StopDownload: Boolean;
begin
  if GetDbConnected then begin
    if GetDownloading then begin

      SetDownloading(False);
      Result := True;

    end else begin
      Result := False;
      SetLastError('Downloading is not running.');
    end;
  end else begin
    Result := False;
    SetLastError('Connection to database is not established.');
  end;
end;
{ TCachingThread ------------------------------------------------------------- }



{ TSimpleOsmMap -------------------------------------------------------------- }
constructor TSimpleOsmMap.Create(AOwner: TComponent);
begin
  inherited Create(AOwner);

  InitVariables('');
end;

constructor TSimpleOsmMap.CreateOsmMap(AOwner: TComponent; ADbFilePath: string);
begin
  inherited Create(AOwner);

  InitVariables(ADbFilePath);
end;

procedure TSimpleOsmMap.InitVariables(ADbFilePath: string);
var
  BmpLayer: TBitmapLayer;
begin
  FTileDownloadThread := TGetTileThread.Create;
  FTileDownloadThread.OnTileDownloaded := OnTileDownloadComplete;

  FCachingThread := TCachingThread.Create;

  FShowCoordinates := True;
  FShowMapObjects := True;
  FShowObjectsLabel := True;
  FMapLayerCanPaint := True;
  FObjectMerging := False;
  FUseCache := False;
  FWorkOffline := False;
  FShowMapZoomControl := True;
  FShowMapMoveControl := True;
  FMapControlType := mcUnknown;

  FZoom := cErrZoom;
  FMapResolution := cErrResolution;

  ClearPosition(FMousePoint);
  ClearCenterPosition(FCenterPoint);

  FPanMap := False;
  FPanFrom := Point(cErrPixel, cErrPixel);

  FSelectArea := False;
  FSelectAreaFrom := Point(cErrPixel, cErrPixel);

  FCacheTimeOut := cCacheTimeOut; //infinite, download tile only once

  FLastError := '';
  FDbFilepath := ADbFilePath;

  FOsmMapUrl := 'http://c.tile.openstreetmap.org/[zoom]/[x]/[y].png'; //default map URL

  FIBDatabase := TIBDatabase.Create(nil);
  FIBTransaction := TIBTransaction.Create(nil);
  FIBQuerySearch := TIBQuery.Create(nil);
  FIBQueryInsert := TIBQuery.Create(nil);
  //FIBQueryUpdate := TIBQuery.Create(nil);
  FIBQueryDelete := TIBQuery.Create(nil);
  FIBQueryClear := TIBQuery.Create(nil);

  FHTTP := THTTPSend.Create;
  FHTTP.UserAgent := 'Opera/9.51 (Windows NT 5.1; U; cs)';
  FHTTP.Timeout := 5000;

  FMapObjectList := TObjectList<TMapObject>.Create;

  FGPSPosition := TMapObject.CreateMapObject(Self, 'GPS', '',
    cErrCoord, cErrCoord, cErrPixel, cErrPixel, False, clRed, 8);

  SetDbFilePath(ADbFilePath);

  Self.TabStop := True;
  Self.ScaleMode := smNormal;

  //create layers
  BmpLayer := TBitmapLayer.Create(Self.Layers); //map layer
  BmpLayer.Bitmap.Assign(nil);
  BmpLayer.Bitmap.CombineMode := cmBlend;
  BmpLayer.Bitmap.DrawMode := dmTransparent;
  BmpLayer.Bitmap.OuterColor := clNone32;
  BmpLayer.Tag := Ord(mlMap);

  BmpLayer := TBitmapLayer.Create(Self.Layers); //points layer
  BmpLayer.Bitmap.Assign(nil);
  BmpLayer.Bitmap.CombineMode := cmBlend;
  BmpLayer.Bitmap.DrawMode := dmTransparent;
  BmpLayer.Bitmap.OuterColor := clNone32;
  BmpLayer.Tag := Ord(mlPoints);

  BmpLayer := TBitmapLayer.Create(Self.Layers); //draw layer
  BmpLayer.Bitmap.Assign(nil);
  BmpLayer.Bitmap.CombineMode := cmBlend;
  BmpLayer.Bitmap.DrawMode := dmTransparent;
  BmpLayer.Bitmap.OuterColor := clNone32;
  BmpLayer.Tag := Ord(mlDraw);

  BmpLayer := TBitmapLayer.Create(Self.Layers); //gps layer
  BmpLayer.Bitmap.Assign(nil);
  BmpLayer.Bitmap.CombineMode := cmBlend;
  BmpLayer.Bitmap.DrawMode := dmTransparent;
  BmpLayer.Bitmap.OuterColor := clNone32;
  BmpLayer.Tag := Ord(mlGps);

  BmpLayer := TBitmapLayer.Create(Self.Layers); //info coordination
  BmpLayer.Bitmap.Assign(nil);
  BmpLayer.Bitmap.CombineMode := cmBlend;
  BmpLayer.Bitmap.DrawMode := dmTransparent;
  BmpLayer.Bitmap.OuterColor := clNone32;
  BmpLayer.Tag := Ord(mlCoord);

  BmpLayer := TBitmapLayer.Create(Self.Layers); //info layer
  BmpLayer.Bitmap.Assign(nil);
  BmpLayer.Bitmap.CombineMode := cmBlend;
  BmpLayer.Bitmap.DrawMode := dmTransparent;
  BmpLayer.Bitmap.OuterColor := clNone32;
  BmpLayer.Tag := Ord(mlInfo);

  BmpLayer := TBitmapLayer.Create(Self.Layers); //map control layer
  BmpLayer.Bitmap.Assign(nil);
  BmpLayer.Bitmap.CombineMode := cmBlend;
  BmpLayer.Bitmap.DrawMode := dmTransparent;
  BmpLayer.Bitmap.OuterColor := clNone32;
  BmpLayer.Tag := Ord(mlMapControl);
end;

destructor TSimpleOsmMap.Destroy;
begin
  FTileDownloadThread.OnTileDownloaded := nil;
  FTileDownloadThread.Terminate;
  try
    while (FTileDownloadThread.Working) do begin
      Sleep(20);
    end;
  finally
    FreeAndNil(FTileDownloadThread);
  end;

  FCachingThread.OnProgress := nil;
  FCachingThread.Terminate;
  while FCachingThread.Downloading do begin
    Sleep(20);
  end;
  FreeAndNil(FCachingThread);

  DisconnectDB;

  FreeAndNil(FIBQuerySearch);
  FreeAndNil(FIBQueryInsert);
  //FreeAndNil(FIBQueryUpdate);
  FreeAndNil(FIBQueryDelete);
  FreeAndNil(FIBQueryClear);

  FreeAndNil(FIBTransaction);
  FreeAndNil(FIBDatabase);

  Self.Layers.Clear;

  FreeAndNil(FMapObjectList); //object owned by list is automaticly freeed
  FreeAndNil(FGPSPosition);

  FreeAndNil(FHTTP);

  inherited Destroy;
end;

procedure TSimpleOsmMap.Resize;
begin
  ShowMap;

  inherited Resize;
end;

procedure TSimpleOsmMap.MouseMove(Shift: TShiftState; X, Y: Integer;
  Layer: TCustomLayer);
var
  NewXTile, NewYTile: Double;
begin
  try
    if (not ComponentStateOk) then begin
      EXIT;
    end;

    if (not Self.Focused) then begin
      //for capturing mouse wheel i need get focus
      Self.SetFocus;
    end;

    if FPanMap then begin
      //pan map
      UpdateMapLayer(Shift, X, Y);
      UpdateGpsLayer(Shift, X, Y);
      UpdateMapObjectLayer(Shift, X, Y);

      if PanMinDiffOk(X, Y) then begin
        Screen.Cursor := crHandPoint;
      end;
    end else if FSelectArea then begin
      //select map area
      UpdateInfoLayer(Shift, X, Y);
    end else begin
      //"simple" mouse move
      //save mouse pixel ant lon/lat position
      if(FCenterPoint.LonDec <> cErrCoord)
        and (FCenterPoint.LatDec <> cErrCoord)
        and (FCenterPoint.XTile <> cErrTile)
        and (FCenterPoint.YTile <> cErrTile)
        and (FZoom <> cErrZoom)
      then begin
        FMousePoint.X := X;
        FMousePoint.Y := Y;

        NewXTile := FCenterPoint.XTile - (((Self.Width/2) - X)/cTileSize);
        NewYTile := FCenterPoint.YTile - (((Self.Height/2) - Y)/cTileSize);

        FMousePoint.LonDec := XTileToLon(NewXTile, FZoom);
        FMousePoint.LatDec := YTileToLat(NewYTile, FZoom);
      end else begin
        ClearPosition(FMousePoint);
      end;

      UpdateCoordLayer(Shift, X, Y); //update coordinates
      UpdateMapControlLayer(Shift, X, Y);
    end; //end - no pannig map

  finally
    inherited MouseMove(Shift, X, Y, Layer);
  end;
end;

function TSimpleOsmMap.MouseOverBtnMoveBottom(AX, AY: Integer): Boolean;
var
  MoveBottomRect: TRect;
begin
  GetMoveBottomRect(MoveBottomRect);
  Result := MouseInRect(AX, AY, MoveBottomRect);
end;

function TSimpleOsmMap.MouseOverBtnMoveLeft(AX, AY: Integer): Boolean;
var
  MoveLeftRect: TRect;
begin
  GetMoveLeftRect(MoveLeftRect);
  Result := MouseInRect(AX, AY, MoveLeftRect);
end;

function TSimpleOsmMap.MouseOverBtnMoveRight(AX, AY: Integer): Boolean;
var
  MoveRightRect: TRect;
begin
  GetMoveRightRect(MoveRightRect);
  Result := MouseInRect(AX, AY, MoveRightRect);
end;

function TSimpleOsmMap.MouseOverBtnMoveTop(AX, AY: Integer): Boolean;
var
  MoveTopRect: TRect;
begin
  GetMoveTopRect(MoveTopRect);
  Result := MouseInRect(AX, AY, MoveTopRect);
end;

function TSimpleOsmMap.MouseOverBtnZoomIn(AX, AY: Integer): Boolean;
var
  ZoomInRect: TRect;
begin
  GetZoomInRect(ZoomInRect);
  Result := MouseInRect(AX, AY, ZoomInRect);
end;

function TSimpleOsmMap.MouseOverBtnZoomOut(AX, AY: Integer): Boolean;
var
  ZoomOutRect: TRect;
begin
  GetZoomOutRect(ZoomOutRect);
  Result := MouseInRect(AX, AY, ZoomOutRect);
end;

function TSimpleOsmMap.MouseOverMapControl(AX, AY: Integer): Boolean;
begin
  Result := FShowMapZoomControl;
  if Result then begin
    Result := MouseOverBtnZoomIn(AX, AY)
      or MouseOverBtnZoomOut(AX, AY);
  end;

  if (not Result) then begin
    Result := FShowMapMoveControl;
    if Result then begin
      Result := MouseOverBtnMoveTop(AX, AY)
      or MouseOverBtnMoveBottom(AX, AY)
      or MouseOverBtnMoveLeft(AX, AY)
      or MouseOverBtnMoveRight(AX, AY);
    end;
  end;
end;

function TSimpleOsmMap.MouseInRect(AX, AY: Integer; ARect: TRect): Boolean;
var
  p: TPoint;
begin
  p := Point(AX, AY);
  Result := ARect.Contains(p);
end;

procedure TSimpleOsmMap.MouseDown(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer; Layer: TCustomLayer);
begin
  try
    if (not ComponentStateOk) then begin
       EXIT;
    end;

    if (Button = mbLeft) then begin
      if MouseOverMapControl(X, Y) then begin
        FMapControlType := GetMapControlTypeUnderMouse(X, Y);
      end else begin
        if (ssDouble in Shift) then begin
          //double click = zoom
          ZoomIn;
        end else if (ssCtrl in Shift) then begin
          //select map area
          FSelectArea := True;
          FSelectAreaFrom := Point(X, Y);
        end else begin
          //pan map
          FPanMap := True;
          FPanFrom := Point(X, Y);
        end;
      end;
    end;

  finally
    inherited MouseDown(Button, Shift, X, Y, Layer);
  end;
end;

procedure TSimpleOsmMap.MouseUp(Button: TMouseButton; Shift: TShiftState;
  X, Y: Integer; Layer: TCustomLayer);
var
  PosX, PosY: Integer;
  NewYTile, NewXTile: Double;
  NewLatDec, NewLonDec: Double;
  MapControlType: TMapControlType;
begin
  try
    if (not ComponentStateOk) then begin
       EXIT;
    end;

    if FPanMap then begin
      //show moved map
      FPanMap := False;
      Screen.Cursor := crDefault;

      if(FCenterPoint.LonDec <> cErrCoord)
        and (FCenterPoint.LatDec <> cErrCoord)
        and (FCenterPoint.XTile <> cErrTile)
        and (FCenterPoint.YTile <> cErrTile)
        and (FZoom <> cErrZoom)
      then begin
        if PanMinDiffOk(X, Y) then begin
          PosX := FCenterPoint.X + (FPanFrom.X - X);
          PosY := FCenterPoint.Y + (FPanFrom.Y - Y);

          NewXTile := FCenterPoint.XTile - (((Self.Width/2) - PosX)/cTileSize);
          NewYTile := FCenterPoint.YTile - (((Self.Height/2) - PosY)/cTileSize);

          NewLatDec := YTileToLat(NewYTile, FZoom);
          NewLonDec := XTileToLon(NewXTile, FZoom);

          //update center coordinates
          if RangeLatOk(NewLatDec)
            and RangeLonOk(NewLonDec)
          then begin
            SetCenterLonLatDec(NewLonDec, NewLatDec);
          end;

          //repaint map
          ShowMap;
        end; //end - min pan diff
      end;
    end else if FSelectArea then begin
      //show is in selected area
      FSelectArea := False;
      UpdateInfoLayer([], cErrPixel, cErrPixel);

      //update center an zoom
      SetMapCoordAndZoomFromRectangle(FSelectAreaFrom.X, FSelectAreaFrom.Y, X, Y);

      //repaint map
      ShowMap;
    end else if (FMapControlType <> mcUnknown) then begin
      try
        //do map control action
        if MouseOverMapControl(X, Y) then begin
          MapControlType := GetMapControlTypeUnderMouse(X, Y);
          if (MapControlType = FMapControlType) then begin
            case MapControlType of
              mcUnknown: ; //no action
              mcZoomIn: ZoomIn(True);
              mcZoomOut: ZoomOut(True);
              mcMoveLeft: MoveLeft;
              mcMoveRight: MoveRight;
              mcMoveTop: MoveTop;
              mcMoveBottom: MoveBottom;
            end;
          end;
        end;
      finally
        FMapControlType := mcUnknown;
      end;
    end;

  finally
    inherited MouseUp(Button, Shift, X, Y, Layer);
  end;
end;

procedure TSimpleOsmMap.MoveLeft;
var
  MapPos: TDecLonLatPosition;
  Dist: Double;
begin
  if (FCenterPoint.LonDec <> cErrCoord)
    and (FCenterPoint.LatDec <> cErrCoord)
    and (FMapResolution <> cErrResolution)
  then begin
    MapPos.LonDec := FCenterPoint.LonDec;
    MapPos.LatDec := FCenterPoint.LatDec;

    Dist := FMapResolution * (cMaxScaleWidth/2);
    MapPos.LonDec := LonAddDistance(MapPos.LonDec, -Trunc(Dist));

    if (not RangeLonOk(MapPos.LonDec)) then begin
      if (MapPos.LonDec < 0) then begin
        MapPos.LonDec := -cMaxAbsLon;
      end else begin
        MapPos.LonDec := cMaxAbsLon;
      end;
    end;

    ShowMap(MapPos.LonDec, MapPos.LatDec);
  end;
end;

procedure TSimpleOsmMap.MoveRight;
var
  MapPos: TDecLonLatPosition;
  Dist: Double;
begin
  if (FCenterPoint.LonDec <> cErrCoord)
    and (FCenterPoint.LatDec <> cErrCoord)
    and (FMapResolution <> cErrResolution)
  then begin
    MapPos.LonDec := FCenterPoint.LonDec;
    MapPos.LatDec := FCenterPoint.LatDec;

    Dist := FMapResolution * (cMaxScaleWidth/2);
    MapPos.LonDec := LonAddDistance(MapPos.LonDec, Trunc(Dist));

    if (not RangeLonOk(MapPos.LonDec)) then begin
      if (MapPos.LonDec < 0) then begin
        MapPos.LonDec := -cMaxAbsLon;
      end else begin
        MapPos.LonDec := cMaxAbsLon;
      end;
    end;

    ShowMap(MapPos.LonDec, MapPos.LatDec);
  end;
end;

procedure TSimpleOsmMap.MoveTop;
var
  MapPos: TDecLonLatPosition;
  Dist: Double;
begin
  if (FCenterPoint.LonDec <> cErrCoord)
    and (FCenterPoint.LatDec <> cErrCoord)
    and (FMapResolution <> cErrResolution)
  then begin
    MapPos.LonDec := FCenterPoint.LonDec;
    MapPos.LatDec := FCenterPoint.LatDec;

    Dist := FMapResolution * (cMaxScaleWidth/2);
    MapPos.LatDec := LatAddDistance(MapPos.LatDec, Trunc(Dist));

    if (not RangeLatOk(MapPos.LatDec)) then begin
      if (MapPos.LatDec < 0) then begin
        MapPos.LatDec := -cMaxAbsLat;
      end else begin
        MapPos.LatDec := cMaxAbsLat;
      end;
    end;

    ShowMap(MapPos.LonDec, MapPos.LatDec);
  end;
end;

procedure TSimpleOsmMap.MoveBottom;
var
  MapPos: TDecLonLatPosition;
  Dist: Double;
begin
  if (FCenterPoint.LonDec <> cErrCoord)
    and (FCenterPoint.LatDec <> cErrCoord)
    and (FMapResolution <> cErrResolution)
  then begin
    MapPos.LonDec := FCenterPoint.LonDec;
    MapPos.LatDec := FCenterPoint.LatDec;

    Dist := FMapResolution * (cMaxScaleWidth/2);
    MapPos.LatDec := LatAddDistance(MapPos.LatDec, -Trunc(Dist));

    if (not RangeLatOk(MapPos.LatDec)) then begin
      if (MapPos.LatDec < 0) then begin
        MapPos.LatDec := -cMaxAbsLat;
      end else begin
        MapPos.LatDec := cMaxAbsLat;
      end;
    end;

    ShowMap(MapPos.LonDec, MapPos.LatDec);
  end;
end;

procedure TSimpleOsmMap.MouseLeave;
begin
  inherited MouseLeave;
end;

function TSimpleOsmMap.DoMouseWheel(Shift: TShiftState; WheelDelta: Integer;
  MousePos: TPoint): Boolean;
begin
  Result := False;

  if (not ComponentStateOk) then begin
     EXIT;
  end;

  if (WheelDelta < 0) then begin
    ZoomOut;
  end else if (WheelDelta > 0) then begin
    ZoomIn;
  end;

  if Assigned(OnMouseWheel) then begin
    OnMouseWheel(Self, Shift, WheelDelta, MousePos, Result);
  end;

  if (not Result) then begin
    Result := True;

    if (WheelDelta < 0) then begin
      DoMouseWheelDown(Shift, MousePos);
    end else if (WheelDelta > 0) then begin
      DoMouseWheelUp(Shift, MousePos);
    end;
  end;

  // WheelDelta returns you - or + values (eg. -120 and + 120 ;
  // It depends on control panel mouse wheel settings)
  // hide inherited procedure because of acumulating wheel turns (1 wheel turn cause 3 DoMouseWheel)
  //Result := inherited DoMouseWheel(Shift, WheelDelta, MousePos);
end;

procedure TSimpleOsmMap.SetupProxy(AHost, APort, AUsr, APassword: string);
begin
  FHTTP.ProxyHost := AHost;
  FHTTP.ProxyPort := APort;
  FHTTP.ProxyUser := AUsr;
  FHTTP.ProxyPass := APassword;

  if (FTileDownloadThread <> nil) then begin
    FTileDownloadThread.SetupProxy(AHost, APort, AUsr, APassword);
  end;

  if (FCachingThread <> nil) then begin
    FCachingThread.SetupProxy(AHost, APort, AUsr, APassword);
  end;
end;

procedure TSimpleOsmMap.ShowMap(ACenterLonDec: Double = cErrCoord; ACenterLatDec: Double = cErrCoord;
  AZoom: Integer = cErrZoom);
var
  PosXPx, PosYPx,
  StartX, StartY,
  EndX, EndY,
  OffsetX, OffsetY,
  x, y: Integer;
  TileUrl: string;
  TmpBitmap: TBitmap;
  PictureBitmapLayer: TBitmapLayer;
begin
  if (not ComponentStateOk) then begin
     EXIT;
  end;

  if (not FIBDatabase.Connected) then begin
    FLastError := 'Can not show the map - database is not connected !';
    EXIT;
  end;

  if (FTileDownloadThread <> nil) then begin
    //prewious tiles are not needed anymore
    FTileDownloadThread.EmptyRequestQueue;
  end;

  FLastError := '';
  PictureBitmapLayer := TBitmapLayer(Self.Layers[Ord(mlMap)]);

  if (Self.Width = 0) or (Self.Height = 0) then begin
    EXIT;
  end;

  SetCenterLonLatDec(ACenterLonDec, ACenterLatDec);
  SetZoomInt(AZoom);

  FCenterPoint.Y := Round(Self.Height/2);
  FCenterPoint.X := Round(Self.Width/2);

  if not ((FCenterPoint.LonDec = cErrCoord)
    or (FCenterPoint.LatDec = cErrCoord)
    or (FZoom = cErrZoom))
  then begin

    if (FOsmMapUrl <> '') then begin
      //center tile number (without trunc)
      FCenterPoint.XTile := LonToXtile(FCenterPoint.LonDec, FZoom);
      FCenterPoint.YTile := LatToYtile(FCenterPoint.LatDec, FZoom);

      //range calculation
      StartX := Trunc(FCenterPoint.XTile - ((Self.Width / cTileSize) / 2));
      StartY := Trunc(FCenterPoint.YTile - ((Self.Height / cTileSize) / 2));
      EndX := Round(FCenterPoint.XTile + ((Self.Width / cTileSize) / 2));
      EndY := Round(FCenterPoint.YTile + ((Self.Height / cTileSize) / 2));

      //offset calculation
      // center offset
      OffsetX := - Trunc( (FCenterPoint.XTile - Trunc(FCenterPoint.XTile)) * cTileSize);
      OffsetY := - Trunc( (FCenterPoint.YTile - Trunc(FCenterPoint.YTile)) * cTileSize);
      // tile offset
      OffsetX := OffsetX + Trunc( Self.Width / 2 );
      OffsetY := OffsetY + Trunc( Self.Height / 2 );
      OffsetX := OffsetX + ( StartX - Trunc(FCenterPoint.XTile) ) * cTileSize;
      OffsetY := OffsetY + ( StartY - Trunc(FCenterPoint.YTile) ) * cTileSize;

      TmpBitmap := TBitmap.Create;
      try
        PictureBitmapLayer.Location := FloatRect(0, 0, Self.Width, Self.Height);
        PictureBitmapLayer.Bitmap.SetSize(Self.Width, Self.Height);
        PictureBitmapLayer.Bitmap.Clear(clNone32);
        //paint background color
        PictureBitmapLayer.Bitmap.Canvas.Brush.Color := Self.Color; //clInfoBk;
        PictureBitmapLayer.Bitmap.Canvas.Brush.Style := bsSolid;
        PictureBitmapLayer.Bitmap.Canvas.FillRect(Rect(0, 0, Self.Width, Self.Height));


        for x := StartX to EndX do begin
          for y := StartY to EndY do begin

            //create tile url
            TileUrl := GetTileUrl(x, y, FZoom);

            PosXPx := ((x - StartX) * cTileSize) + OffsetX;
            PosYPx := ((y - StartY) * cTileSize) + OffsetY;

            //get tile image
            if GetImage(x, y, TileUrl, TmpBitmap, PosXPx, PosYPx) then begin
              //tile is in cache -> draw tile to main bitmap immediately
              PictureBitmapLayer.Bitmap.Canvas.Draw(PosXPx, PosYPx, TmpBitmap);
            end;

          end;
        end;

        UpdateMapObjectLayer([], cErrPixel, cErrPixel);
        UpdateGpsLayer([], cErrPixel, cErrPixel);
        UpdateCoordLayer([], cErrPixel, cErrPixel);
        UpdateInfoLayer([], cErrPixel, cErrPixel);
        UpdateMapControlLayer([], cErrPixel, cErrPixel);

      finally
        FreeAndNil(TmpBitmap);
      end;

    end else begin
      FLastError := Format('Undefined map source', []);
    end;

  end else begin
    if (FZoom = cErrZoom) then begin
      FLastError := Format('Zoom "%d" is not valid', [FZoom]);
    end else begin
      FLastError := Format('Center point "(%s, %s)" is not valid !', [
        FloatToStrF(FCenterPoint.LonDec, ffGeneral, 6, 9),
        FloatToStrF(FCenterPoint.LatDec, ffGeneral, 6, 9)
      ]);
    end;
  end;
end;

function TSimpleOsmMap.SetGpsPosition(ALonDec, ALatDec: Double): Boolean;
begin
  Result := SetObjectPosition(FGPSPosition, ALonDec, ALatDec);
  if Result then begin
    UpdateGpsLayer([], cErrPixel, cErrPixel);
  end;
end;

function TSimpleOsmMap.SetGpsVisibility(AState: Boolean): Boolean;
begin
  Result := False;
  if (FGPSPosition <> nil) then begin
    FGPSPosition.Visible := AState;
    Result := True;
    //
    UpdateGpsLayer([], cErrPixel, cErrPixel);
  end;
end;

function TSimpleOsmMap.CreateDb(ADbFilePath: string): string;
var
  TmpTran: TIBTransaction;
  TmpDb: TIBDatabase;
  TmpQuery: TIBQuery;
  SqlFile: TStringList;
  SqlFilePath: string;
  I: Integer;
begin
  Result := Format('Unable to create the database "%s"', [ADbFilePath]);

  if (not FileExists(ADbFilePath)) then begin
    if (not DirectoryExists(ExtractFileDir(ADbFilePath))) then begin
      if (not ForceDirectories(ExtractFileDir(ADbFilePath))) then begin
        Result := Format('Unable to create the database directory "%s"', [
          ExtractFileDir(ADbFilePath)]);
        EXIT;
      end;
    end;

    TmpDb := TIBDatabase.Create(nil);
    TmpTran := TIBTransaction.Create(nil);
    TmpQuery := TIBQuery.Create(nil);
    SqlFile := TStringList.Create;

    try
      //directory exists, create the database
      TmpQuery.Database := TmpDb;
      TmpQuery.Transaction := TmpTran;

      try
        // #1 - create empty database
        TmpDb.Params.Clear;
        TmpDb.DatabaseName := ADbFilePath;
        TmpDb.Params.Text  := 'USER ''' + cDbLogin + '''' + cCRLF
          + 'PASSWORD ''' + cDbPassword + '''' + cCRLF
          + 'PAGE_SIZE 4096' + cCRLF
          + 'DEFAULT CHARACTER SET ' + cDbEncoding;          
        TmpDb.SQLDialect   := 3;
        TmpDb.CreateDatabase;

        // #2 - connect to the database
        TmpDb.Close;
        TmpDb.Params.Clear;
        TmpDb.LoginPrompt := False;
        TmpDb.Params.Add('user_name=' + cDbLogin);
        TmpDb.Params.Add('password=' + cDbPassword);
        TmpDb.Params.Add('lc_ctype=' + cDbEncoding);
        TmpDb.SQLDialect := 3;
        TmpDb.Open;
        TmpDb.DefaultTransaction := TmpTran;
        TmpTran.StartTransaction;

        // #3 - create database structure
        SqlFilePath := ExtractFileDir(ADbFilePath) + '\db_struct.sql';
        if FileExists(SqlFilePath) then begin
          SqlFile.LoadFromFile(SqlFilePath, TEncoding.ASCII);
        end else begin
          SqlFile.Add('CREATE TABLE OSM_TILES (ID BIGINT NOT NULL, URL_BASE VARCHAR(100) NOT NULL COLLATE ' + cDbCollate + ', '
            + 'ZOOM NUMERIC(2,0) NOT NULL, X INTEGER NOT NULL, Y INTEGER NOT NULL, IMG_EXTENSION '
            + 'VARCHAR(4) NOT NULL COLLATE ' + cDbCollate + ', IMG_DATA BLOB SUB_TYPE 0 SEGMENT SIZE 4096 NOT NULL, '
            + 'UPD_TS TIMESTAMP NOT NULL );');
          SqlFile.Add('ALTER table OSM_TILES add constraint PK_OSM_TILES primary key (ID);');
          SqlFile.Add('COMMENT ON COLUMN OSM_TILES.ID IS ''record id'';');
          SqlFile.Add('COMMENT ON COLUMN OSM_TILES.URL_BASE IS ''tile server URL'';');
          SqlFile.Add('COMMENT ON COLUMN OSM_TILES.ZOOM IS ''tile zoom'';');
          SqlFile.Add('COMMENT ON COLUMN OSM_TILES.X IS ''longitude of the picture'';');
          SqlFile.Add('COMMENT ON COLUMN OSM_TILES.Y IS ''latitude of the to picture'';');
          SqlFile.Add('COMMENT ON COLUMN OSM_TILES.IMG_DATA IS ''tile binary data'';');
          SqlFile.Add('COMMENT ON COLUMN OSM_TILES.IMG_EXTENSION IS ''tile file extension'';');
          SqlFile.Add('COMMENT ON COLUMN OSM_TILES.UPD_TS IS ''time of the record insert/update'';');
          SqlFile.Add('CREATE GENERATOR GEN_OSM_TILES_ID;');
          SqlFile.Add('CREATE TRIGGER OSM_TILES_BI FOR OSM_TILES ACTIVE BEFORE INSERT POSITION 0 AS BEGIN IF (NEW.ID IS NULL) THEN NEW.ID = GEN_ID(GEN_OSM_TILES_ID,1); END');
          SqlFile.Add('CREATE UNIQUE INDEX OSM_TILES_SEARCH ON OSM_TILES (X,Y,ZOOM,URL_BASE,IMG_EXTENSION)');
          SqlFile.Add('CREATE INDEX OSM_TILES_CACHE_DELAY ON OSM_TILES (UPD_TS)');
        end;

        for I := 0 to (SqlFile.Count - 1) do begin
          TmpQuery.Close;
          TmpQuery.SQL.Clear;
          TmpQuery.SQL.Text := SqlFile.Strings[i];
          TmpQuery.ExecSQL;
        end;
        Result := '';

      except
        TmpTran.Rollback;
        Result := Format('Unable to create the database structure from "%s"', [SqlFilePath]);
      end;

    finally
      ClearQuery(TmpQuery);

      if TmpTran.Active then begin
        TmpTran.Commit;
      end;

      if TmpDb.Connected then begin
        TmpDb.Close;
      end;

      FreeAndNil(TmpTran);
      FreeAndNil(TmpDb);
      FreeAndNil(SqlFile);
      FreeAndNil(TmpQuery);

      //clear uncomplete database
      if Result <> '' then begin
        DeleteFile(ADbFilePath);
      end;
    end;

  end else begin
    Result := ''; //database exists
  end;
end;

function TSimpleOsmMap.ConnectDb(ADbFilePath: string): string;
begin
  Result := CreateDb(ADbFilePath);
  if Result = '' then begin //database created or exists
    try
      //ADbFilePath := 'localhost:' + ExtractFilePath(Application.ExeName) + 'db\file-name.fdb'; //clasic server
      //ADbFilePath := ExtractFilePath(Application.ExeName) + 'db\file-name.fdb'; //embedded server
      FIBDatabase.DatabaseName := ADbFilePath;
      FIBDatabase.DefaultTransaction := FIBTransaction;
      FIBDatabase.LoginPrompt := False;
      FIBDatabase.Params.Clear;
      FIBDatabase.Params.Add('user_name=' + cDbLogin);
      FIBDatabase.Params.Add('password=' + cDbPassword);
      FIBDatabase.Params.Add('lc_ctype=' + cDbEncoding);
      FIBDatabase.SQLDialect := 3;
      FIBDatabase.Open;

      if (FIBDatabase.Connected) then begin  //connected?

        if (not FIBTransaction.Active) then begin //start the transaction
          FIBTransaction.DefaultDatabase := FIBDatabase;
          //read commited data from other transactions
          FIBTransaction.Params.Clear;
          FIBTransaction.Params.Add('read_committed');
          FIBTransaction.Params.Add('rec_version');
          FIBTransaction.Params.Add('nowait');
          FIBTransaction.StartTransaction;
        end;

        if FIBTransaction.Active then begin //setup FIBQuery

          FIBQuerySearch.Database := FIBDatabase;
          FIBQuerySearch.Transaction := FIBDatabase.DefaultTransaction;
          ClearQuery(FIBQuerySearch);
          FIBQuerySearch.SQL.Text := 'SELECT ot.*, DATEDIFF(DAY, upd_ts, cast(''NOW'' as timestamp)) as day_age'
          + ' FROM osm_tiles ot WHERE'
          + ' (X = :x) and (Y = :y) and (ZOOM = :zoom) and (url_base = :url_base)'
          + ' and (img_extension = :img_ext)';
          FIBQuerySearch.Prepare;

          FIBQueryInsert.Database := FIBDatabase;
          FIBQueryInsert.Transaction := FIBDatabase.DefaultTransaction;
          ClearQuery(FIBQueryInsert);
          FIBQueryInsert.SQL.Text := 'INSERT INTO osm_tiles'
            + ' (url_base, zoom, x, y, img_extension, img_data, upd_ts)'
            + ' VALUES (:url_base, :zoom, :x, :y, :img_extension, :img_data, :upd_ts)';
          FIBQueryInsert.Prepare;

          {FIBQueryUpdate.Database := FIBDatabase;
          FIBQueryUpdate.Transaction := FIBDatabase.DefaultTransaction;
          ClearQuery(FIBQueryUpdate);
          FIBQueryUpdate.SQL.Text := 'UPDATE osm_tiles'
            + ' SET img_data = :img_data, upd_ts = cast(''NOW'' as timestamp)'
            + ' WHERE (url_base = :url_base) and (zoom = :zoom) and (x = :x)'
            + ' and (y = :y) and (img_extension = :img_extension)';
          FIBQueryUpdate.Prepare;}

          FIBQueryDelete.Database := FIBDatabase;
          FIBQueryDelete.Transaction := FIBDatabase.DefaultTransaction;
          ClearQuery(FIBQueryDelete);
          FIBQueryDelete.SQL.Text := 'DELETE FROM osm_tiles WHERE (id = :id)';
          FIBQueryDelete.Prepare;

          FIBQueryClear.Database := FIBDatabase;
          FIBQueryClear.Transaction := FIBDatabase.DefaultTransaction;
          ClearQuery(FIBQueryClear);
          FIBQueryClear.SQL.Text := 'DELETE FROM osm_tiles WHERE'
            + ' (:cache_timeout <> -1)' //cCacheTimeOut = -1, nothing to clear
            + ' and (DATEDIFF(DAY, upd_ts, cast(''NOW'' as timestamp)) >= :cache_timeout)'; //clear older then..
          FIBQueryClear.Prepare;

          Result := ''; //succesfully connected -> return empty string
        end;
      end;

    except
      Result := Format('Unable to connect to the database "%s"', [
        FIBDatabase.DatabaseName]);
    end;
  end;
end;

procedure TSimpleOsmMap.DisconnectDb;
begin
  ClearQuery(FIBQuerySearch);
  ClearQuery(FIBQueryInsert);
  //ClearQuery(FIBQueryUpdate);
  ClearQuery(FIBQueryDelete);
  ClearQuery(FIBQueryClear);

  if FIBTransaction.Active then begin
    FIBTransaction.Commit;
  end;

  if FIBDatabase.Connected then begin
    FIBDatabase.Close;
  end;
end;

procedure TSimpleOsmMap.SetDbFilePath(ADbFilePath: string);
begin
  if (ADbFilePath <> '') then begin
    FLastError := ConnectDb(ADbFilePath);

    if (FLastError = '') then begin
      if (FCachingThread <> nil) then begin
        FCachingThread.DbFilePath := ADbFilePath;
      end;
    end;
  end else begin
    FLastError := 'Undefined DB path!';
  end;
end;

procedure TSimpleOsmMap.ClearPosition(var APosition: TMapPosition);
begin
  APosition.LonDec := cErrCoord;
  APosition.LatDec := cErrCoord;
  APosition.X := cErrPixel;
  APosition.Y := cErrPixel;
end;

procedure TSimpleOsmMap.ClearCenterPosition(var APosition: TCenterPosition);
begin
  APosition.XTile := cErrCoord;
  APosition.YTile := cErrCoord;
  APosition.LonDec := cErrCoord;
  APosition.LatDec := cErrCoord;
  APosition.X := cErrPixel;
  APosition.Y := cErrPixel;
end;

procedure TSimpleOsmMap.ClearQuery(AQuery: TIBQuery);
begin
  AQuery.Close;
  AQuery.SQL.Text := '';
end;

procedure TSimpleOsmMap.CommitTransaction(ARestart: Boolean = False);
begin
  if ARestart then begin
    FIBTransaction.Commit;
    FIBTransaction.StartTransaction;
  end else begin
    FIBTransaction.CommitRetaining;
  end;
end;

procedure TSimpleOsmMap.RollbackTransaction(ARestart: Boolean = False);
begin
  if ARestart then begin
    FIBTransaction.Rollback;
    FIBTransaction.StartTransaction;
  end else begin
    FIBTransaction.RollbackRetaining;
  end;
end;

procedure TSimpleOsmMap.DrawMapScaleDist(ABitmapLayer: TBitmapLayer; AXPos, AYPos: Integer);
var
  Distance: Int64;
  TmpInt, I: Integer;
  StrDistance, ZeroStr, TmpStr: string;
  IndicatorWidth, IndicatorHeight: Integer;
begin

  if (AXPos <> cErrPixel) and (AYPos <> cErrPixel)
    and (ABitmapLayer <> nil)
  then begin
    if (FMapResolution <> cErrResolution) then begin

      Distance := Trunc(cMaxScaleWidth * FMapResolution);
      StrDistance := IntToStr(Distance);

      ZeroStr := '';
      for I := 1 to  Length(StrDistance) - 1 do begin
        ZeroStr := ZeroStr + '0';
      end;
      TmpStr := StrDistance[1];
      TmpInt := StrToInt(TmpStr);

      if (TmpInt >= 5) then begin
        Distance := StrToInt('5' + ZeroStr);
      end else if (TmpInt >= 3) then begin
        Distance := StrToInt('3' + ZeroStr);
      end else if (TmpInt >= 1) then begin
        Distance := StrToInt('1' + ZeroStr);
      end;

      IndicatorWidth := Trunc(Distance / FMapResolution);
      IndicatorHeight := cMapScaleHeight;

      //text distance
      if (Distance >= 1000) then begin
        Distance := Round(Distance / 1000);
        TmpStr := Format('%d km', [ Distance ]);
      end else begin
        TmpStr := Format('%d m', [ Distance ]);
      end;

      ABitmapLayer.Bitmap.Canvas.Brush.Style := bsClear;
      ABitmapLayer.Bitmap.Canvas.Font.Color := clBlack;
      ABitmapLayer.Bitmap.Canvas.Font.Size := 8;
      ABitmapLayer.Bitmap.Canvas.TextOut(
        AXPos + 6,
        AYPos - ABitmapLayer.Bitmap.Canvas.TextHeight(TmpStr) - 4,
        //AXPos + IndicatorWidth - ABitmapLayer.Bitmap.Canvas.TextWidth(TmpStr) - 2,
        //AYPos - ABitmapLayer.Bitmap.Canvas.TextHeight(TmpStr) - 2,
        TmpStr);

      ABitmapLayer.Bitmap.Canvas.Pen.Color := clBlack;
      ABitmapLayer.Bitmap.Canvas.Pen.Style := psSolid;
      ABitmapLayer.Bitmap.Canvas.Pen.Width := 2;

      ABitmapLayer.Bitmap.Canvas.MoveTo(AXPos, AYPos - IndicatorHeight);
      ABitmapLayer.Bitmap.Canvas.LineTo(AXPos, AYPos);
      ABitmapLayer.Bitmap.Canvas.LineTo(AXPos + IndicatorWidth, AYPos);
      ABitmapLayer.Bitmap.Canvas.LineTo(AXPos + IndicatorWidth, AYPos - IndicatorHeight);

    end; //lat meter to px are ok
  end; //input data are ok
end;

procedure TSimpleOsmMap.DrawMoveControl(ABitmapLayer: TBitmapLayer; AXPos, AYPos: Integer);
var
  b: TBitmap32;
  MoveTopRect, MoveBottomRect, MoveLeftRect, MoveRightRect: TRect;
  MouseOnMoveTop, MouseOnMoveBottom, MouseOnMoveLeft, MouseOnMoveRight: Boolean;
  BtnRect: TRect;
  ShpPoints: TArrayOfFloatPoint;
  ArrowPoints: TArrayOfFloatPoint;
  ArrowClr: TColor32;
begin
  b := ABitmapLayer.Bitmap;


  //top button
  GetMoveTopRect(MoveTopRect);
  MouseOnMoveTop := MouseInRect(AXPos, AYPos, MoveTopRect);
  //
  BtnRect := TRect.Create(MoveTopRect);
  ShpPoints := RoundRect(FloatRect(BtnRect), cBtnMoveRound);
  PolygonFS(b, ShpPoints, clWhite32);
  //
  if MouseOnMoveTop then begin
    ArrowClr := clBlack32;
  end else begin
    ArrowClr := clLightGray32;
  end;
  SetLength(ArrowPoints, 0);
  ArrowPoints := BuildPolygonF([
    BtnRect.Left + (cBtnMoveWidth/2), BtnRect.Top + cBtnMovePadding,
    BtnRect.Right - cBtnMovePadding, BtnRect.Bottom - cBtnMovePadding,
    BtnRect.Left + (cBtnMoveWidth/2), BtnRect.Top + (cBtnMoveHeight/2) + cBtnMovePadding,
    BtnRect.Left + cBtnMovePadding, BtnRect.Bottom - cBtnMovePadding
  ]);
  PolygonFS(b, ArrowPoints, ArrowClr);


  //bottom button
  GetMoveBottomRect(MoveBottomRect);
  MouseOnMoveBottom := MouseInRect(AXPos, AYPos, MoveBottomRect);
  //
  BtnRect := TRect.Create(MoveBottomRect);
  ShpPoints := RoundRect(FloatRect(BtnRect), cBtnMoveRound);
  PolygonFS(b, ShpPoints, clWhite32);
  //
  if MouseOnMoveBottom then begin
    ArrowClr := clBlack32;
  end else begin
    ArrowClr := clLightGray32;
  end;
  SetLength(ArrowPoints, 0);
  ArrowPoints := BuildPolygonF([
    BtnRect.Left + (cBtnMoveWidth/2), BtnRect.Bottom - cBtnMovePadding,
    BtnRect.Left + cBtnMovePadding, BtnRect.Top + cBtnMovePadding,
    BtnRect.Left + (cBtnMoveWidth/2), BtnRect.Top + (cBtnMoveHeight/2) - cBtnMovePadding,
    BtnRect.Right - cBtnMovePadding, BtnRect.Top + cBtnMovePadding
  ]);
  PolygonFS(b, ArrowPoints, ArrowClr);


  //left button
  GetMoveLeftRect(MoveLeftRect);
  MouseOnMoveLeft := MouseInRect(AXPos, AYPos, MoveLeftRect);
  //
  BtnRect := TRect.Create(MoveLeftRect);
  ShpPoints := RoundRect(FloatRect(BtnRect), cBtnMoveRound);
  PolygonFS(b, ShpPoints, clWhite32);
  //
  if MouseOnMoveLeft then begin
    ArrowClr := clBlack32;
  end else begin
    ArrowClr := clLightGray32;
  end;
  SetLength(ArrowPoints, 0);
  ArrowPoints := BuildPolygonF([
    BtnRect.Left + cBtnMovePadding, BtnRect.Top + (cBtnMoveHeight/2),
    BtnRect.Right - cBtnMovePadding, BtnRect.Top + cBtnMovePadding,
    BtnRect.Left + (cBtnMoveWidth/2) + cBtnMovePadding, BtnRect.Top + (cBtnMoveHeight/2),
    BtnRect.Right - cBtnMovePadding, BtnRect.Bottom - cBtnMovePadding
  ]);
  PolygonFS(b, ArrowPoints, ArrowClr);


  //right button
  GetMoveRightRect(MoveRightRect);
  MouseOnMoveRight := MouseInRect(AXPos, AYPos, MoveRightRect);
  //
  BtnRect := TRect.Create(MoveRightRect);
  ShpPoints := RoundRect(FloatRect(BtnRect), cBtnMoveRound);
  PolygonFS(b, ShpPoints, clWhite32);
  //
  if MouseOnMoveRight then begin
    ArrowClr := clBlack32;
  end else begin
    ArrowClr := clLightGray32;
  end;
  SetLength(ArrowPoints, 0);
  ArrowPoints := BuildPolygonF([
    BtnRect.Right - cBtnMovePadding, BtnRect.Top + (cBtnMoveHeight/2),
    BtnRect.Left + cBtnMovePadding, BtnRect.Bottom - cBtnMovePadding,
    BtnRect.Left + (cBtnMoveWidth/2) - cBtnMovePadding, BtnRect.Top + (cBtnMoveHeight/2),
    BtnRect.Left + cBtnMovePadding, BtnRect.Top + cBtnMovePadding
  ]);
  PolygonFS(b, ArrowPoints, ArrowClr);
end;

procedure TSimpleOsmMap.DrawSelectionRectangle(ABitmapLayer: TBitmapLayer; AXFrom, AYFrom, AXTo, AYTo: Integer);
begin
  ABitmapLayer.Bitmap.Canvas.Brush.Style := bsClear;
  ABitmapLayer.Bitmap.Canvas.Pen.Color := clRed;
  ABitmapLayer.Bitmap.Canvas.Pen.Style := psSolid;
  ABitmapLayer.Bitmap.Canvas.Pen.Width := 2;

  ABitmapLayer.Bitmap.Canvas.Rectangle(AXFrom, AYFrom, AXTo, AYTo);
end;

procedure TSimpleOsmMap.DrawZoomControl(ABitmapLayer: TBitmapLayer; AXPos, AYPos: Integer);
var
  b: TBitmap32;
  ZoomInRect, ZoomOutRect: TRect;
  MouseOnZoomIn, MouseOnZoomOut: Boolean;
  BtnRect: TRect;
  ShpPoints: TArrayOfFloatPoint;
  LineClr: TColor32;
  LineWidth: Integer;
begin
  b := ABitmapLayer.Bitmap;


  //zoom in button
  GetZoomInRect(ZoomInRect);
  MouseOnZoomIn := MouseInRect(AXPos, AYPos, ZoomInRect);
  //
  BtnRect := TRect.Create(ZoomInRect);
  ShpPoints := RoundRect(FloatRect(BtnRect), cBtnZoomRound);
  PolygonFS(b, ShpPoints, clWhite32);
  //
  if MouseOnZoomIn then begin
    LineClr := clBlack32;
    LineWidth := 2;
  end else begin
    LineClr := clLightGray32;
    LineWidth := 2;
  end;
  //
  b.FillRectS(BtnRect.Left + cBtnZoomPadding, BtnRect.Top + Round(cBtnZoomHeight / 2) - LineWidth,
    BtnRect.Right - cBtnZoomPadding, BtnRect.Top + Round(cBtnZoomHeight / 2) + LineWidth,
    LineClr);
  b.FillRectS(BtnRect.Left + Round(cBtnZoomWidth / 2) - LineWidth, BtnRect.Top + cBtnZoomPadding,
    BtnRect.Left + Round(cBtnZoomWidth / 2) + LineWidth, BtnRect.Bottom - cBtnZoomPadding,
    LineClr);


  //zoom out button
  GetZoomOutRect(ZoomOutRect);
  MouseOnZoomOut := MouseInRect(AXPos, AYPos, ZoomOutRect);
  //
  BtnRect := TRect.Create(ZoomOutRect);
  ShpPoints := RoundRect(FloatRect(BtnRect), cBtnZoomRound);
  PolygonFS(b, ShpPoints, clWhite32);
  //
  if MouseOnZoomOut then begin
    LineClr := clBlack32;
    LineWidth := 2;
  end else begin
    LineClr := clLightGray32;
    LineWidth := 2;
  end;
  //
  b.FillRectS(BtnRect.Left + cBtnZoomPadding, BtnRect.Top + Round(cBtnZoomHeight / 2) - LineWidth,
    BtnRect.Right - cBtnZoomPadding, BtnRect.Top + Round(cBtnZoomHeight / 2) + LineWidth,
    LineClr);
end;

procedure TSimpleOsmMap.SetMapCoordAndZoomFromRectangle(AX1, AY1, AX2, AY2: Integer);
var
  TmpCenterXTile, TmpCenterYTile: Double;
  LeftPos, RightPos: TCenterPosition;
  I: Integer;
  TmpTile: Double;
begin
  if (FCenterPoint.LonDec <> cErrCoord)
    and (FCenterPoint.LatDec <> cErrCoord)
    and (FCenterPoint.XTile <> cErrTile)
    and (FCenterPoint.YTile <> cErrTile)
    and (FZoom <> cErrZoom)
  then begin

    (*
        x----------   LeftPos
        -----------
        -----------
        -----------
        ----------x   RightPos
    *)

    if (AX1 <= AX2) then begin
      LeftPos.X := AX1;
      RightPos.X := AX2;
    end else begin
      LeftPos.X := AX2;
      RightPos.X := AX1;
    end;

    if (AY1 <= AY2) then begin
      LeftPos.Y := AY1;
      RightPos.Y := AY2;
    end else begin
      LeftPos.Y := AY2;
      RightPos.Y := AY1;
    end;

    LeftPos.XTile := FCenterPoint.XTile - (((Self.Width/2) - LeftPos.X)/cTileSize);
    LeftPos.YTile := FCenterPoint.YTile - (((Self.Height/2) - LeftPos.Y)/cTileSize);

    RightPos.XTile := FCenterPoint.XTile - (((Self.Width/2) - RightPos.X)/cTileSize);
    RightPos.YTile := FCenterPoint.YTile - (((Self.Height/2) - RightPos.Y)/cTileSize);

    if ((Trunc(LeftPos.XTile) >= 0) and (Trunc(LeftPos.YTile) >= 0))
      and ((Trunc(LeftPos.XTile) <= MaxXYTile(FZoom)) and (Trunc(LeftPos.YTile) <= MaxXYTile(FZoom)))
      and ((Trunc(RightPos.XTile) >= 0) and (Trunc(RightPos.YTile) >= 0))
      and ((Trunc(RightPos.XTile) <= MaxXYTile(FZoom)) and (Trunc(RightPos.YTile) <= MaxXYTile(FZoom)))
    then begin

      LeftPos.LonDec := XTileToLon(LeftPos.XTile, FZoom);
      LeftPos.LatDec := YTileToLat(LeftPos.YTile, FZoom);

      RightPos.LonDec := XTileToLon(RightPos.XTile, FZoom);
      RightPos.LatDec := YTileToLat(RightPos.YTile, FZoom);

      if RangeLatOk(LeftPos.LatDec) and RangeLonOk(LeftPos.LonDec)
        and RangeLatOk(RightPos.LatDec) and RangeLonOk(RightPos.LonDec)
      then begin

        //set new center coordinates
        SetCenterLonLatDec(
          Leftpos.LonDec + (Abs(RightPos.LonDec - LeftPos.LonDec) / 2),
          LeftPos.LatDec - (Abs(RightPos.LatDec - LeftPos.LatDec) / 2) );

        //set new zoom
        if ( Abs(LeftPos.X - RightPos.X) >= Abs(LeftPos.Y - RightPos.Y) ) then begin
          //width is bigger - search right border
          for I := FZoom to cMaxZoom do begin
            TmpTile := LonToXtile(RightPos.LonDec, I);
            TmpCenterXTile := LonToXtile(FCenterPoint.LonDec, I);
            if ( ((TmpTile - TmpCenterXTile) * cTileSize) > (Self.Width/2) ) then begin
              SetZoomInt(I - 1);
              BREAK;
            end;
          end;

        end else begin
          for I := FZoom to cMaxZoom do begin
            //height is bigger - search bottom border
            TmpTile := LatToYtile(RightPos.LatDec, I);
            TmpCenterYTile := LatToYtile(FCenterPoint.LatDec, I);
            if ( ((TmpTile - TmpCenterYTile) * cTileSize) > (Self.Height/2) ) then begin
              SetZoomInt(I - 1);
              BREAK;
            end;
          end;

        end;

      end;  //cooridantes are ok
    end;  //rectangle coord are ok
  end;  //center is ok
end;

function TSimpleOsmMap.GetTileUrl(Ax, Ay, AZoom: integer): string;
begin
  Result := '';
  if (FOsmMapUrl <> '') then begin
    Result := FOsmMapUrl;
    Result := StringReplace(Result, cTileX, IntToStr(Ax), [rfReplaceAll]);
    Result := StringReplace(Result, cTileY, IntToStr(Ay), [rfReplaceAll]);
    Result := StringReplace(Result, cTileZoom, IntToStr(AZoom), [rfReplaceAll]);
  end;
end;

procedure TSimpleOsmMap.GetZoomBtnRect(var ARect: TRect);
begin
  ARect := Rect(0, 0, cBtnZoomWidth, cBtnZoomHeight);
end;

procedure TSimpleOsmMap.GetZoomInRect(var ARect: TRect);
var
  btnRect: TRect;
begin
  GetZoomBtnRect(btnRect);
  ARect := TRect.Create(btnRect);
  ARect.Offset(cBtnZoomMarginLeft, cBtnZoomMarginTop);
end;

procedure TSimpleOsmMap.GetZoomOutRect(var ARect: TRect);
begin
  GetZoomInRect(ARect);
  ARect.Offset(0, cBtnZoomHeight);
end;

procedure TSimpleOsmMap.SetCenterLonLatDec(ACenterLonDec, ACenterLatDec: Double);
begin
  if (ACenterLatDec <> cErrCoord)
    and (ACenterLonDec <> cErrCoord)
  then begin
    if (ACenterLonDec <> FCenterPoint.LonDec)
      or (ACenterLatDec <> FCenterPoint.LatDec)
    then begin

      if RangeLonOk(ACenterLonDec)
        and RangeLatOk(ACenterLatDec)
      then begin
        FCenterPoint.LonDec := ACenterLonDec;
        FCenterPoint.LatDec := ACenterLatDec;
      end else begin
        ClearCenterPosition(FCenterPoint);
      end;

      SetMapResolution;
    end;
  end;
end;

procedure TSimpleOsmMap.SetZoom(AZoom: Integer);
begin
  SetZoom(AZoom, False);
end;

procedure TSimpleOsmMap.SetZoom(AZoom: Integer; AUseCenterCoord: Boolean);
var
  NewXTile, NewYTile,
  NewLonDec, NewLatDec: Double;
  MapPt: TMapPosition;
  PtrMapPt: PTMapPosition;
begin
  if (AZoom <> cErrZoom) then begin
    if (AZoom <> FZoom) then begin
      if MapZoomIsOk(AZoom) then begin

        //after zooming map, mouse must be at te same lon/lat and left/top position
        //i must change center point to propper lon/lat

        if AUseCenterCoord then begin
          MapPt.LonDec := FCenterPoint.LonDec;
          MapPt.LatDec := FCenterPoint.LatDec;
          MapPt.X := FCenterPoint.X;
          MapPt.Y := FCenterPoint.Y;
          PtrMapPt := @MapPt;
        end else begin
          PtrMapPt := @FMousePoint;
        end;

        NewXTile := LonToXtile(PtrMapPt^.LonDec, AZoom);
        NewYTile := LatToYtile(PtrMapPt^.LatDec, AZoom);

        NewLonDec := XTileToLon(NewXTile + (((Self.Width/2) - PtrMapPt^.X)/cTileSize), AZoom);
        NewLatDec := YTileToLat(NewYTile + (((Self.Height/2) - PtrMapPt^.Y)/cTileSize), AZoom);

        SetCenterLonLatDec(NewLonDec, NewLatDec);
        SetZoomInt(AZoom);

        ShowMap;
      end else begin
        //FZoom := cErrZoom;
        FLastError := Format('Invalid zoom value "%d"', [AZoom]);
      end;
    end;
  end;
end;

procedure TSimpleOsmMap.SetZoomInt(AZoom: Integer);
begin
  if (AZoom <> cErrZoom) then begin
    if (AZoom <> FZoom) then begin
      if MapZoomIsOk(AZoom) then begin
        FZoom := AZoom;
        SetMapResolution;
      end else begin
        //FZoom := cErrZoom;
        FLastError := Format('Invalid zoom value "%d"', [AZoom]);
      end;
    end;
  end;
end;

procedure TSimpleOsmMap.SetCacheTimeOut(ATimeOut: Integer);
begin
  if ATimeOut < cCacheTimeOut then begin
    FCacheTimeOut := cCacheTimeOut;
  end else begin
    FCacheTimeOut := ATimeOut;
  end;

  if (FCacheTimeOut <> cCacheTimeOut) then begin //not infinite cache
    ClearCache(FCacheTimeOut); //clear old tiles
  end;
end;

procedure TSimpleOsmMap.SetShowCoordinates(AState: Boolean);
begin
  if (FShowCoordinates <> AState) then begin
    FShowCoordinates := AState;
    UpdateCoordLayer([], cErrPixel, cErrPixel);
  end;
end;

procedure TSimpleOsmMap.SetShowMapZoomControl(AState: Boolean);
begin
  if (FShowMapZoomControl <> AState) then begin
    FShowMapZoomControl := AState;
    UpdateMapControlLayer([], cErrPixel, cErrPixel);
  end;
end;

procedure TSimpleOsmMap.SetShowMapMoveControl(AState: Boolean);
begin
  if (FShowMapMoveControl <> AState) then begin
    FShowMapMoveControl := AState;
    UpdateMapControlLayer([], cErrPixel, cErrPixel);
  end;
end;

procedure TSimpleOsmMap.SetShowMapObjects(AState: Boolean);
begin
  if (FShowMapObjects <> AState) then begin
    FShowMapObjects := AState;
    UpdateMapObjectLayer([], cErrPixel, cErrPixel);
  end;
end;

procedure TSimpleOsmMap.SetMergeMapObject(AState: Boolean);
begin
  if (FObjectMerging <> AState) then begin
    FObjectMerging := AState;
    UpdateMapObjectLayer([], cErrPixel, cErrPixel);
  end;
end;

procedure TSimpleOsmMap.SetShowObjectsLabel(AState: Boolean);
begin
  if (FShowObjectsLabel <> AState) then begin
    FShowObjectsLabel := AState;
    UpdateMapObjectLayer([], cErrPixel, cErrPixel);
  end;
end;

function TSimpleOsmMap.CloneMapObject(AMO: TMapObject): TMapObject;
begin
  if (AMO.ImgBitmap = nil) then begin //normal object
    Result := TMapObject.CreateMapObject(nil, AMO.Name, AMO.Caption, AMO.LatDec,
      AMO.LonDec, AMO.X, AMO.Y, AMO.Visible, AMO.Color, AMO.Radius
    );
  end else begin //object with image from file
    Result := TMapObject.CreateMapObject(nil, AMO.Name, AMO.Caption, AMO.LatDec,
      AMO.LonDec, AMO.X, AMO.Y, AMO.ImgBitmap, AMO.Visible
    );
  end;
end;

function TSimpleOsmMap.PanMinDiffOk(AX, AY: Integer): Boolean;
var
  Dx, Dy: Integer;
begin
  Dx := AX - FPanFrom.X;
  Dy := AY - FPanFrom.Y;

  Result := (Abs(Dx) >= cMinPanDiff) or (Abs(Dy) >= cMinPanDiff);
end;

function TSimpleOsmMap.PointsCountFromCaption(ACaption: string): Integer;
begin
  Result := 1;
  while (Pos(cMapObjectCaptionDelimiter, ACaption) > 0) do begin
    Result := Result + 1;
    Delete(ACaption, 1, Pos(cMapObjectCaptionDelimiter, ACaption) + Length(cMapObjectCaptionDelimiter) - 1);
  end;
end;

function TSimpleOsmMap.MapObjectDistance(AMOFrom, AMOTo: TMapObject): Integer;
// --
  function GetMapObjectHalfSize(AMO: TMapObject): Integer;
  begin
    Result := 0;
    if (AMO <> nil) then begin
      if (AMO.ImgBitmap = nil) then begin
        Result := Trunc(AMO.Radius / 2);
      end else begin
        Result := Trunc(AMO.ImgBitmap.Width / 2); //assume square map object
      end;
    end;
  end;
// --
begin
  Result := 0;
  if ((AMOFrom <> nil) and (AMOTo <> nil)) then begin
    Result := Trunc(Power(Power(Abs(AMOFrom.X - AMOTo.X), 2)
      + Power(Abs(AMOFrom.Y - AMOTo.Y), 2), 0.5))
      - GetMapObjectHalfSize(AMOFrom)
      - GetMapObjectHalfSize(AMOTo)
      - Trunc(cMultiPointSize / 2);
  end;
end;

function TSimpleOsmMap.MapObjectInVisibleArea(AMapObject: TMapObject): Boolean;
var
  pntLeftTop: TPoint;
  pntRightBottom: TPoint;
  viewArea: TRect;
begin
  if AMapObject.Kind in [mokGeneral, mokPOI] then begin
    //standart map object (circle or image)

    if (AMapObject.ImgBitmap <> nil) then begin
      //image
      pntLeftTop.X := Trunc(AMapObject.X - (AMapObject.ImgBitmap.Width / 2));
      pntLeftTop.Y := Trunc(AMapObject.Y - (AMapObject.ImgBitmap.Height / 2));
      pntRightBottom.X := Trunc(AMapObject.X + (AMapObject.ImgBitmap.Width / 2));
      pntRightBottom.Y := Trunc(AMapObject.Y + (AMapObject.ImgBitmap.Height / 2));
    end else begin
      //circle
      pntLeftTop.X := AMapObject.X - AMapObject.Radius;
      pntLeftTop.Y := AMapObject.Y - AMapObject.Radius;
      pntRightBottom.X := AMapObject.X + AMapObject.Radius;
      pntRightBottom.Y := AMapObject.Y + AMapObject.Radius;
    end;

  end else if AMapObject.Kind in [mokDraw] then begin
    //drawing (circle, rectangle, line)

    if AMapObject.DrawKind = modkCircle then begin
      //circle
      pntLeftTop.X := AMapObject.X - AMapObject.Radius;
      pntLeftTop.Y := AMapObject.Y - AMapObject.Radius;
      pntRightBottom.X := AMapObject.X + AMapObject.Radius;
      pntRightBottom.Y := AMapObject.Y + AMapObject.Radius;
    end else if AMapObject.DrawKind = modkRectangle then begin
      //rectangle
      pntLeftTop.X := Trunc(AMapObject.X - (AMapObject.Width / 2));
      pntLeftTop.Y := Trunc(AMapObject.Y - (AMapObject.Height / 2));
      pntRightBottom.X := Trunc(AMapObject.X + (AMapObject.Width / 2));
      pntRightBottom.Y := Trunc(AMapObject.Y + (AMapObject.Height / 2));
    end else if AMapObject.DrawKind = modkLine then begin
      //line
      pntLeftTop.X := AMapObject.X;
      pntLeftTop.Y := AMapObject.Y;
      pntRightBottom.SetLocation(pntLeftTop.X, pntLeftTop.Y);
      viewArea := Rect(0, 0, Self.Width, Self.Height);

      if IsLineInRect(pntLeftTop, AMapObject.Angle, viewArea, FMapResolution, pntRightBottom) then begin
        Result := True;
        EXIT;
      end;
    end;

  end;

  // https://www.geeksforgeeks.org/find-two-rectangles-overlap/
  Result := (not(
      (pntLeftTop.X > Self.Width)       //object is too in the right
      or (pntRightBottom.X < 0)         //object is too in the left
      or (pntLeftTop.Y > Self.Height)   //object is too under
      or (pntRightBottom.Y < 0)         //object is too above
    ));
end;

function TSimpleOsmMap.SetObjectPosition(AMapObject: TMapObject; AlonDec, ALatDec: Double): Boolean;
begin
  Result := False;

  if (AMapObject <> nil) then begin
    if (AlonDec <> cErrCoord) and (ALatDec <> cErrCoord) and (FZoom <> cErrZoom)
      and (FCenterPoint.LonDec <> cErrCoord) and (FCenterPoint.LatDec <> cErrCoord)
    then begin
      Result := AMapObject.SetLonLatDec(AlonDec, ALatDec);

      //first way -> large marker position error in low zoom
      //Result := AMapObject.SetLeftTopPx(LonDecToPixelX(AlonDec), LatDecToPixelY(ALatDec));

      //second way -> smaller marker position error in low zoom, better way
      if Result then begin
        Result := AMapObject.SetPxPosition(CalculatePxX(AlonDec), CalculatePxY(ALatDec));
      end;
    end else begin
      //if center point or zoom is udefined set only Lon/lat
      AMapObject.SetLonLatDec(AlonDec, ALatDec);

      //if map will show, pixel position of the object will recalculate
      //AMapObject.SetLeftTopPx(cErrPixel, cErrPixel);
    end;
  end;
end;

{function TSimpleOsmMap.SetObjectSize(AMapObject: TMapObject; ASize: Integer): Boolean;
begin
  Result := False;
  if (AMapObject <> nil) then begin
    AMapObject.Size := ASize;
    Result := True;
  end;
end;}

{function TSimpleOsmMap.SetObjectColor(AMapObject: TMapObject; AColor: TColor): Boolean;
begin
  Result := False;
  if (AMapObject <> nil) then begin
    AMapObject.Color := AColor;
    Result := True;
  end;
end;}

{function TSimpleOsmMap.SetObjectName(AMapObject: TMapObject; AName: string): Boolean;
begin
  Result := False;
  if (AMapObject <> nil) then begin
    AMapObject.Name := AName;
    Result := True;
  end;
end;}

{function TSimpleOsmMap.SetObjectCaption(AMapObject: TMapObject; ACaption: string): Boolean;
begin
  Result := False;
  if (AMapObject <> nil) then begin
    AMapObject.Caption := ACaption;
    Result := True;
  end;
end;}

function TSimpleOsmMap.ComponentStateOk: Boolean;
begin
Result := not ((csDesigning in ComponentState)  //design time
  or (csLoading in ComponentState)              //loading
  or (csDestroying in ComponentState))          //destroing
end;

function TSimpleOsmMap.SetMapObjectPosition(AMapObject: TMapObject; ALonDec, ALatDec: Double): Boolean;
begin
  Result := SetObjectPosition(AMapObject, ALonDec, ALatDec);
  if Result then begin
    UpdateMapObjectLayer([], cErrPixel, cErrPixel);
  end;
end;

function TSimpleOsmMap.SetMapObjectVisibility(AMapObject: TMapObject; AVisibility: Boolean): Boolean;
begin
  Result := False;
  if (AMapObject <> nil) then begin
    AMapObject.Visible := AVisibility;
    Result := True;
    //
    UpdateMapObjectLayer([], cErrPixel, cErrPixel);
  end;
end;

procedure TSimpleOsmMap.SetMapResolution;
begin
  if (FCenterPoint.LonDec <> cErrCoord)
    and (FCenterPoint.LatDec <> cErrCoord)
    and (FZoom <> cErrZoom)
  then begin
    FMapResolution := CalculateMapResolution(FCenterPoint.LatDec, FZoom);
  end else begin
    FMapResolution := cErrResolution;
  end;
end;

procedure TSimpleOsmMap.DisableMapObjectsRepaint;
begin
  FMapLayerCanPaint := False;
end;

procedure TSimpleOsmMap.EnableMapObjectsRepaint;
begin
  FMapLayerCanPaint := True;
  UpdateMapObjectLayer([], cErrPixel, cErrPixel);
end;

function TSimpleOsmMap.ClearMapObjectList(AKind: TMapObjectKind = moAll): Boolean;
var
  I: Integer;
  mo: TMapObject;
begin
  Result := True;
  try
    if (FMapObjectList <> nil) then begin

      if AKind = moAll then begin
        FMapObjectList.Clear;
      end else begin
        I := 0;
        while (I < FMapObjectList.Count) do begin
          mo := FMapObjectList.Items[I];
          if mo.Kind = AKind then begin
            FMapObjectList.Delete(I);
          end else begin
            I := I + 1;
          end;
        end;
      end;

      UpdateMapObjectLayer([], cErrPixel, cErrPixel);
    end;
  except
    Result := False;
  end;
end;

function TSimpleOsmMap.FirstMapObject(AKind: TMapObjectKind = moAll): TMapObject;
var
  I: Integer;
  mo: TMapObject;
begin
  Result := nil;
  try
    if (FMapObjectList <> nil) then begin
      if (FMapObjectList.Count > 0) then begin

        for I := 0 to FMapObjectList.Count - 1 do begin
          mo := FMapObjectList.Items[I];
          if AKind = moAll then begin
            Result := mo;
            BREAK;
          end else begin
            if (mo.Kind = AKind) then begin
              Result := mo;
              BREAK;
            end;
          end;
        end;

      end;
    end;
  except
    Result := nil;
  end;
end;

function TSimpleOsmMap.NextMapObject(APrevMapObject: TMapObject; AKind: TMapObjectKind = moAll): TMapObject;
var
  TmpInt: Integer;
  I: Integer;
  mo: TMapObject;
begin
  Result := nil;
  try
    if (FMapObjectList <> nil) then begin
      TmpInt := FMapObjectList.IndexOf(APrevMapObject);
      if (TmpInt <> -1) then begin

        if (TmpInt = (FMapObjectList.Count - 1)) then begin //prev object is last object
          Result := nil;
        end else begin

          //find next object
          for I := TmpInt + 1 to FMapObjectList.Count - 1 do begin
            mo := FMapObjectList.Items[I];
            if AKind = moAll then begin
              Result := mo;
              BREAK;
            end else begin
              if (mo.Kind = AKind) then begin
                Result := mo;
                BREAK;
              end;
            end;
          end;

        end;

      end; //end - tmpint <> -1
    end; //map object list exists
  except
    Result := nil;
  end;
end;

function TSimpleOsmMap.FindMapObject(AName: string; ACaption: string = '';
  ALonDec: Double = cErrCoord; ALatDec: Double = cErrCoord; AKind: TMapObjectKind = moAll): TMapObject;
var
  I: Integer;
  Founded: Boolean;
  mo: TMapObject;
begin
  Result := nil;

  try
    if (FMapObjectList <> nil) then begin
      for I := 0 to (FMapObjectList.Count - 1) do begin
        mo := FMapObjectList.Items[I];
        Founded := True;

        if (AName <> '') then begin
          Founded := (LowerCase(AName) = LowerCase(mo.Name));
        end;

        if (ACaption <> '') then begin
          Founded := Founded and (LowerCase(ACaption) = LowerCase(mo.Caption));
        end;

        if (ALonDec <> cErrCoord) then begin
          Founded := Founded and (ALonDec = mo.LonDec);
        end;

        if (ALatDec <> cErrCoord) then begin
          Founded := Founded and (ALatDec = mo.LatDec);
        end;

        if (AKind <> moAll) then begin
          Founded := Founded and (AKind = mo.Kind);
        end;

        if Founded then begin
          Result := mo;
          BREAK;
        end;

      end; //for
    end;
  except
    Result := nil;
  end;
end;

function TSimpleOsmMap.AddMapObject(AName: string; ALatDec: Double; ALonDec: Double;
  ACaption: string = ''; AColor: TColor = clBlue; AVisibility: Boolean = True;
  ASize: Integer = 6; AKind: TMapObjectKind = mokGeneral): TMapObject;
begin
  Result := nil;

  if AKind in [mokUnknown, mokDraw] then begin
    EXIT;
  end;

  try
    if (FMapObjectList <> nil) then begin
      Result := TMapObject.CreateMapObject(Self, AName, ACaption, ALatDec, ALonDec,
        0, 0, AVisibility, AColor, ASize, AKind);
      SetObjectPosition(Result, ALonDec, ALatDec);
      // --
      FMapObjectList.Add(Result);
      // --
      UpdateMapObjectLayer([], cErrPixel, cErrPixel);
    end;
  except
    Result := nil;
  end;
end;

function TSimpleOsmMap.AddMapObject(AName: string; ALatDec: Double; ALonDec: Double;
  AImgPath: string; ACaption: string = ''; AVisibility: Boolean = True;
  AKind: TMapObjectKind = mokGeneral): TMapObject;
begin
  Result := nil;

  if AKind in [mokUnknown, mokDraw] then begin
    EXIT;
  end;

  try
    if (FMapObjectList <> nil) then begin
      if FileExists(AImgPath) then begin
        Result := TMapObject.CreateMapObject(Self, AName, ACaption, ALatDec,
          ALonDec, 0, 0, AImgPath, AVisibility, AKind);
        SetObjectPosition(Result, ALonDec, ALatDec);
        // --
        FMapObjectList.Add(Result);
        // --
        UpdateMapObjectLayer([], cErrPixel, cErrPixel);
      end;
    end;
  except
    Result := nil;
  end;
end;

function TSimpleOsmMap.AddMapObject(AMapZoom: Integer; AName: string; ALatDec: Double; ALonDec: Double; ADrawKind: TMapObjectDrawKind;
  AWidth: Integer; AHeight: Integer; ARadius: Integer; AAngle: Double; AColor: TColor = clBlack;
  ACaption: string = ''; ABorderWidth: Integer = cBorderDefWidth; ALengthMeters: Integer = cDefLineLengthMeters;
  AVisibility: Boolean = True): TMapObject;
begin
  Result := nil;

  if ADrawKind in [modkUnknown] then begin
    EXIT;
  end;

  try
    if (FMapObjectList <> nil) then begin

      if (ADrawKind = modkLine) then begin
        //line length will be stored in WIDTH property
        AWidth := ALengthMeters;
      end;

      Result := TMapObject.CreateMapObject(
        Self, AMapZoom, AName, ACaption, ALatDec, ALonDec, 0, 0, ADrawKind,
        AColor, ARadius, ABorderWidth, AWidth, AHeight, AAngle,
        True
      );

      SetObjectPosition(Result, ALonDec, ALatDec);
      // --
      FMapObjectList.Add(Result);
      // --
      UpdateDrawLayer([], cErrPixel, cErrPixel);

    end;
  except
    Result := nil;
  end;
end;

function TSimpleOsmMap.DeleteMapObject(AMapObject: TMapObject): Boolean;
var
  TmpInt: Integer;
begin
 Result := False;
  try
    if (FMapObjectList <> nil) then begin
      TmpInt := FMapObjectList.IndexOf(AMapObject);
      if (TmpInt <> -1) then begin
        FMapObjectList.Delete(TmpInt);
        Result := True;
        //
        UpdateMapObjectLayer([], cErrPixel, cErrPixel);
      end;
    end;
  except
    Result := False;
  end;
end;

procedure TSimpleOsmMap.CopyToClipboard;
const
  clNone32 = TColor32($00FFFFFF);
var
  BmpLayerBitmap: TBitmap32;
  OsmBitmap: TBitmap32;
  ClipboardBitmap: TBitmap;
  I: Integer;
begin
  BmpLayerBitmap := TBitmap32.Create;
  OsmBitmap := TBitmap32.Create;
  ClipboardBitmap := TBitmap.Create;

  BmpLayerBitmap.SetSize(Self.Width, Self.Height);
  OsmBitmap.SetSize(Self.Width, Self.Height);
  ClipboardBitmap.SetSize(Self.Width, Self.Height);

  BmpLayerBitmap.DrawMode := dmBlend;
  OsmBitmap.DrawMode := dmBlend;

  try
    OsmBitmap.Clear(clNone32);
    if (Self.Layers.Count > 0) then begin //kdyz mam vrstvy, pouziju je vsechny
      for I := 0 to (Self.Layers.Count - 1) do begin
        BmpLayerBitmap.Assign(TBitmapLayer(Self.Layers.Items[I]).Bitmap);
        BmpLayerBitmap.DrawTo(OsmBitmap, OsmBitmap.ClipRect, BmpLayerBitmap.ClipRect);
      end; //end-for
    end else begin //kdyz nemam vrstvy jen priradim bitmapu
      OsmBitmap.Assign(Self.Bitmap);
    end;

    ClipboardBitmap.Canvas.CopyRect(
      ClipboardBitmap.Canvas.ClipRect,
      OsmBitmap.Canvas,
      OsmBitmap.Canvas.ClipRect
    );

    Clipboard.Assign(ClipboardBitmap);

  finally
    FreeAndNil(ClipboardBitmap);
    FreeAndNil(OsmBitmap);
    FreeAndNil(BmpLayerBitmap);
  end;
end;

function TSimpleOsmMap.GetImage(Ax, Ay: Integer; ATileUrl: string; AImageBitmap: TBitmap;
  ALeftPx, ATopPx: Integer): Boolean;
  // --
  procedure CreateEmptyTile(ABitmap: TBitmap; Ax, Ay, AZoom: string);
  var
    TmpStr: string;
    TextX, TextY,
    IntAx, IntAy: Integer;
    MaxXY: Double;
  begin
    IntAx := StrToInt(Ax);
    IntAy := StrToInt(Ay);
    MaxXY := MaxXYTile(StrToInt(AZoom));

    // download error -> empty tilt
    AImageBitmap.Assign(nil);
    AImageBitmap.Width := cTileSize;
    AImageBitmap.Height := cTileSize;

    AImageBitmap.Canvas.Pen.Color := clBlack;
    AImageBitmap.Canvas.Pen.Width := 1;
    AImageBitmap.Canvas.Brush.Color := Self.Color; //clInfoBk;
    AImageBitmap.Canvas.Brush.Style := bsSolid;
    AImageBitmap.Canvas.FillRect(Rect(0, 0, AImageBitmap.Width, AImageBitmap.Height));
    //AImageBitmap.Canvas.Rectangle(0, 0, AImageBitmap.Width, AImageBitmap.Height);

    AImageBitmap.Canvas.Font.Size := 8;
    AImageBitmap.Canvas.Font.Color := clRed;
    AImageBitmap.Canvas.Font.Style := AImageBitmap.Canvas.Font.Style + [fsBold];

    //for wrong coordinates dont paint texts
    if ((IntAx >= 0) and (IntAy >= 0)) and ((IntAx <= MaxXY) and (IntAy <= MaxXY)) then begin
      TmpStr := Format('x=%s, y=%s, zoom=%s', [Ax, Ay, AZoom]);
      TextX := Trunc((AImageBitmap.Width - AImageBitmap.Canvas.TextWidth(TmpStr)) / 2);
      TextY := Trunc((AImageBitmap.Height - AImageBitmap.Canvas.TextHeight(TmpStr)) / 2);

      if (TextX < 0) then begin
        TextX := 0;
      end;
      if (TextY < 0) then begin
        TextY := 0;
      end;

      AImageBitmap.Canvas.TextOut(TextX, TextY, TmpStr);
    end;
  end;
  // --
var
  PictureStream: TMemoryStream;
  MaxXY: Double;
begin
  Result := False;

  PictureStream := TMemoryStream.Create;
  try
    MaxXY := MaxXYTile(FZoom); //valid coordinates ?
    if ((Ax >= 0) and (Ay >= 0)) and ((Ax <= MaxXY) and (Ay <= MaxXY)) then begin

      if FUseCache then begin
        if TileInCache(Ax, Ay) then begin
      
          // tile is selected in TIBQuery
          PictureStream.Clear;
          PictureStream.Position := 0;
          TBlobField(FIBQuerySearch.FieldByName('img_data')).SaveToStream(PictureStream);

          AImageBitmap.Assign(nil);
          AImageBitmap.Width := cTileSize;
          AImageBitmap.Height := cTileSize;
          StreamPicToBitmap(PictureStream, AImageBitmap);

          Result := True; //i have tile in cache

          EXIT; //picture loaded from DB
        end;
      end;

      if (not FWorkOffline) then begin //work online
        //tile is not in cache, cache is obsolete or the cache is not used
        //download tile by thread
        FTileDownloadThread.AddTileForDownload(ATileUrl, Ax, Ay, FZoom, ALeftPx, ATopPx);

        {FHTTP.Clear;
        FHTTP.HTTPMethod('GET', ATileUrl);

        if FHTTP.ResultCode = 200 then begin
          PictureStream.Clear;
          PictureStream.Position := 0;
          PictureStream.CopyFrom(FHTTP.Document, 0);

          AImageBitmap.Assign(nil);
          AImageBitmap.Width := cTileSize;
          AImageBitmap.Height := cTileSize;

          if FUseCache then begin
            //if the cache is used, save tile to db
            SaveTileToCache(Ax, Ay, ATileUrl, PictureStream, Now);
          end;

          StreamPicToBitmap(PictureStream, AImageBitmap);
        end else begin
          //HTTP error
          CreateEmptyTile(AImageBitmap, IntToStr(Ax), IntToStr(Ay), IntToStr(FZoom));
          FLastError := Format('Error downloading file "%s"', [ATileUrl]);
        end;}

      end else begin //work offline
        //tile doesnt found, return empty tile
        CreateEmptyTile(AImageBitmap, IntToStr(Ax), IntToStr(Ay), IntToStr(FZoom));
      end;

    end else begin //end - valid coordinates
      CreateEmptyTile(AImageBitmap, IntToStr(Ax), IntToStr(Ay), IntToStr(FZoom));
    end;

  finally
    FreeAndNil(PictureStream);
  end;
end;

function TSimpleOsmMap.GetMapControlTypeUnderMouse(AX, AY: Integer): TMapControlType;
begin
  Result := mcUnknown;

  if MouseOverBtnZoomIn(AX, AY) then begin
    Result := mcZoomIn;
  end else if MouseOverBtnZoomOut(AX, AY) then begin
    Result := mcZoomOut;
  end else if MouseOverBtnMoveTop(AX, AY) then begin
    Result := mcMoveTop;
  end else if MouseOverBtnMoveBottom(AX, AY) then begin
    Result := mcMoveBottom;
  end else if MouseOverBtnMoveLeft(AX, AY) then begin
    Result := mcMoveLeft;
  end else if MouseOverBtnMoveRight(AX, AY) then begin
    Result := mcMoveRight;
  end;
end;

procedure TSimpleOsmMap.GetMoveBtnRect(var ARect: TRect);
begin
  ARect := Rect(Self.Width - cBtnMoveWidth, 0, Self.Width, cBtnMoveHeight);
end;

procedure TSimpleOsmMap.GetMoveLeftRect(var ARect: TRect);
var
  btnRect: TRect;
begin
  GetMoveBtnRect(btnRect);
  ARect := TRect.Create(btnRect);
  ARect.Offset(-cBtnMoveMarginLeft -(cBtnMoveWidth * 2), cBtnMoveMarginTop + cBtnMoveHeight);
end;

procedure TSimpleOsmMap.GetMoveRightRect(var ARect: TRect);
var
  btnRect: TRect;
begin
  GetMoveBtnRect(btnRect);
  ARect := TRect.Create(btnRect);
  ARect.Offset(-cBtnMoveMarginLeft, cBtnMoveMarginTop + cBtnMoveHeight);
end;

procedure TSimpleOsmMap.GetMoveTopRect(var ARect: TRect);
var
  btnRect: TRect;
begin
  GetMoveBtnRect(btnRect);
  ARect := TRect.Create(btnRect);
  ARect.Offset(-cBtnMoveMarginLeft - cBtnMoveWidth, cBtnMoveMarginTop);
end;

function TSimpleOsmMap.GetMouseCoordinates: TMapPosition;
begin
  Result.LatDec := cErrCoord;
  Result.LonDec := cErrCoord;
  Result.X := cErrPixel;
  Result.Y := cErrPixel;

  if RangeLonOk(FMousePoint.LonDec)
    and RangeLatOk(FMousePoint.LatDec)
  then begin
    Result.LatDec := FMousePoint.LatDec;
    Result.LonDec := FMousePoint.LonDec;
    Result.X := FMousePoint.X;
    Result.Y := FMousePoint.Y;
  end;
end;

procedure TSimpleOsmMap.GetMoveBottomRect(var ARect: TRect);
var
  btnRect: TRect;
begin
  GetMoveBtnRect(btnRect);
  ARect := TRect.Create(btnRect);
  ARect.Offset(-cBtnMoveMarginLeft - cBtnMoveWidth, cBtnMoveMarginTop + (cBtnMoveHeight * 2));
end;

procedure TSimpleOsmMap.DrawMapObject(ABitmap: TBitmap32; AMapObject: TMapObject);
var
  Sampler: TRadialGradientSampler;
  Filler: TSamplerFiller;
  Outline: TArrayOfFloatPoint;
  LabelOffset: Integer;
begin
  if AMapObject.Kind in [mokGeneral, mokPOI] then begin
    //map object is POINT

    if MapObjectInVisibleArea(AMapObject) then begin
      if (AMapObject.ImgBitmap <> nil) then begin //picture

        ABitmap.Draw(
          AMapObject.X - Trunc(AMapObject.ImgBitmap.Width / 2),
          AMapObject.Y - Trunc(AMapObject.ImgBitmap.Height / 2),
          AMapObject.ImgBitmap
        );

        LabelOffset := Trunc(AMapObject.ImgBitmap.Height * 0.66);

      end else begin  //circle

        Sampler := TRadialGradientSampler.Create;
        try
          Sampler.Gradient.StartColor := BrighterColor(Color32(AMapObject.Color), 40);
          Sampler.Gradient.EndColor := Color32(AMapObject.Color);
          Sampler.Radius := cDrawSamplerRadius;
          Sampler.Center := FloatPoint(AMapObject.X, AMapObject.Y);

          Filler := TSamplerFiller.Create(Sampler);
          try
            Filler.Sampler := Sampler;
            Outline := Circle(AMapObject.X, AMapObject.Y, AMapObject.Radius, 20);

            PolygonFS(ABitmap, Outline, Filler, pfNonZero {pfAlternate, pfWinding});
            PolylineFS(ABitmap, Outline, clBlack32, True, 1);
          finally
            FreeAndNil(Filler);
          end;

        finally
          FreeAndNil(Sampler);
        end;

        LabelOffset := AMapObject.Radius;
      end; //end - draw object

      if FShowObjectsLabel then begin //draw object label
        ABitmap.Font.Size := 9;
        ABitmap.Font.Style := [fsBold];
        ABitmap.Textout(
          AMapObject.X - Trunc(ABitmap.TextWidth(AMapObject.Caption) / 2),
          AMapObject.Y + LabelOffset,
          AMapObject.Caption
        );
      end; //end - draw label
    end; //end - object in view

  end; //end - proper kind
end;

procedure TSimpleOsmMap.DrawMapObjectDrawing(ABitmap: TBitmap32; AMapObject: TMapObject);
var
//  Sampler: TRadialGradientSampler;
//  Filler: TSamplerFiller;
  Outline: TArrayOfFloatPoint;
  LabelOffset: Integer;
  Rect: TFloatRect;
  lineBegin, lineEnd: TPoint;
begin
  if (AMapObject.Kind = mokDraw) then begin
    //map object is DRAWING

    if MapObjectInVisibleArea(AMapObject)then begin
      if AMapObject.DrawKind = modkCircle then begin
        //circle

        Outline := Circle(AMapObject.X, AMapObject.Y, Round(AMapObject.Radius * CalculatePixelRation(AMapObject.Zoom, Zoom)) , 20);
        PolylineFS(ABitmap, Outline, Color32(AMapObject.Color), True, AMapObject.BorderWidth);

        LabelOffset := Round(AMapObject.Radius * CalculatePixelRation(AMapObject.Zoom, Zoom));
      end else if AMapObject.DrawKind = modkRectangle then begin
        //rectangle

        Rect := FloatRect(
          AMapObject.X - (AMapObject.Width / 2)  * CalculatePixelRation(AMapObject.Zoom, Zoom),   //left
          AMapObject.Y - (AMapObject.Height / 2) * CalculatePixelRation(AMapObject.Zoom, Zoom),   //top
          AMapObject.X + (AMapObject.Width / 2)  * CalculatePixelRation(AMapObject.Zoom, Zoom),   //right
          AMapObject.Y + (AMapObject.Height / 2) * CalculatePixelRation(AMapObject.Zoom, Zoom)    //bottom
        );
        Outline := Rectangle(Rect);
        PolylineFS(ABitmap, Outline, Color32(AMapObject.Color), True, AMapObject.BorderWidth);

        LabelOffset := Round((AMapObject.Height / 2) * CalculatePixelRation(AMapObject.Zoom, Zoom));
      end else if AMapObject.DrawKind = modkLine then begin
        //line

        lineBegin.SetLocation(AMapObject.X, AMapObject.Y);
        lineEnd := GetLineEnd(lineBegin, AMapObject.Angle, FMapResolution, AMapObject.Width); //width of the LINE contans length information
        Outline := Line(lineBegin.X, lineBegin.Y, lineEnd.X, lineEnd.Y);
        PolylineFS(ABitmap, Outline, Color32(AMapObject.Color), True, AMapObject.BorderWidth);

        if ((AMapObject.Angle >= 0) and (AMapObject.Angle <= 90))
          or ((AMapObject.Angle >= 270) and (AMapObject.Angle <= 360))
        then begin
          //label under begin pont of the line
          LabelOffset := AMapObject.Y + (AMapObject.BorderWidth * 4);
        end else begin
          //label above begin pont of the line
          LabelOffset := AMapObject.Y - (AMapObject.BorderWidth * 4);
        end;
      end else begin
        EXIT;
      end;

      if FShowObjectsLabel then begin //draw object label
        if Trim(AMapObject.Caption) <> '' then begin

          ABitmap.Font.Size := 9;
          ABitmap.Font.Style := [fsBold];
          ABitmap.Textout(
            AMapObject.X - Trunc(ABitmap.TextWidth(AMapObject.Caption) / 2),
            AMapObject.Y + LabelOffset,
            AMapObject.Caption
          );
        end;
      end; //end - draw label

    end; //end - object is visible
  end; //end - propper kind
end;

procedure TSimpleOsmMap.DrawMultiMapObject(ABitmap: TBitmap32; AMapObject: TMapObject);
var
  Sampler: TRadialGradientSampler;
  Filler: TSamplerFiller;
  Outline: TArrayOfFloatPoint;
  LabelOffset, I: Integer;
  PointsCount: string;
  SL: TStringList;
begin
  if AMapObject.Kind in [mokGeneral, mokPOI] then begin
    if MapObjectInVisibleArea(AMapObject) then begin
      Sampler := TRadialGradientSampler.Create;
      try
        AMapObject.Radius := cMultiPointSize; //pixels
        //AMapObject.Color := cMultiPointColor;

        Sampler.Gradient.StartColor := BrighterColor(Color32(AMapObject.Color), 40);
        Sampler.Gradient.EndColor := Color32(AMapObject.Color);
        Sampler.Radius := cDrawSamplerRadius;
        Sampler.Center := FloatPoint(AMapObject.X, AMapObject.Y);

        Filler := TSamplerFiller.Create(Sampler);
        try
          Filler.Sampler := Sampler;
          Outline := Circle(AMapObject.X, AMapObject.Y, AMapObject.Radius, 20);

          PolygonFS(ABitmap, Outline, Filler, pfNonZero {pfAlternate, pfWinding});
          PolylineFS(ABitmap, Outline, clBlack32, True, 1);
        finally
          FreeAndNil(Filler);
        end;

      finally
        FreeAndNil(Sampler);
      end;

      //setup font
      ABitmap.Font.Size := 9;
      ABitmap.Font.Style := [fsBold];

      //draw points count
      SL := TStringList.Create;
      try
        SL.Text := StringReplace(AMapObject.Caption, cMapObjectCaptionDelimiter, cCRLF, [rfReplaceAll]);
        PointsCount := IntToStr(SL.Count);

        ABitmap.Textout(
          AMapObject.X - Trunc(ABitmap.TextWidth(PointsCount) / 2),
          AMapObject.Y - Trunc(ABitmap.TextHeight(PointsCount) / 2),
          PointsCount
        );

        LabelOffset := AMapObject.Radius;
        if FShowObjectsLabel then begin //draw object label
          for I := 0 to SL.Count - 1 do begin
            ABitmap.Textout(
              AMapObject.X - Trunc(ABitmap.TextWidth(SL.Strings[I]) / 2),
              AMapObject.Y + LabelOffset + (I * ABitmap.TextHeight(SL.Strings[I])),
              SL.Strings[I]
            );
          end; //end - for
        end; //end - draw label
      finally
        FreeAndNil(SL);
      end;

    end; //end - object in view
  end; //end - proper object kind
end;

procedure TSimpleOsmMap.OnTileDownloadComplete;
var
  DownloadedTile: TTile;
  TileBitmap: TBitmap;
  MapLayer: TBitmapLayer;
begin
  try
    if (FTileDownloadThread <> nil) then begin
      DownloadedTile := FTileDownloadThread.DownloadedTile;
      if (not DownloadedTile.DownloadError) then begin

        TileBitmap := TBitmap.Create;
        try
          TileBitmap.Assign(nil);
          TileBitmap.Width := cTileSize;
          TileBitmap.Height := cTileSize;

          SaveTileToCache(DownloadedTile.XTile, DownloadedTile.YTile,
            DownloadedTile.TileUrl, DownloadedTile.TileStream,
            Now, DownloadedTile.Zoom);

          StreamPicToBitmap(DownloadedTile.TileStream, TileBitmap);

          MapLayer := TBitmapLayer(Self.Layers[Ord(mlMap)]);
          MapLayer.Bitmap.Canvas.Draw(
            DownloadedTile.PosXpx,
            DownloadedTile.PosYPx,
            TileBitmap);

          //when all tiles are downloaded, repaint map
          if (FTileDownloadThread.GetArrayLength = 0) then begin
            Self.ForceFullInvalidate;
          end;

        finally
          FreeAndNil(TileBitmap);
        end;

      end; //end - download ok
    end; //end - thread exists
  finally
    if (DownloadedTile.TileStream <> nil) then begin
      FreeAndNil(DownloadedTile.TileStream);
    end;
    Finalize(DownloadedTile);
  end;
end;

function TSimpleOsmMap.CalculatePxX(APointLonDec: Double): Integer;
begin
  if (FCenterPoint.XTile <> cErrTile) and (FZoom <> cErrZoom) then begin
    Result := Trunc((Self.Width/2)
      - ((FCenterPoint.XTile - LonToXtile(APointLonDec, FZoom))*cTileSize)
    );
  end else begin
    Result := cErrPixel;
  end;
end;

function TSimpleOsmMap.CalculatePxY(APointLatDec: Double): Integer;
begin
  if (FCenterPoint.YTile <> cErrTile) and (FZoom <> cErrZoom) then begin
    Result := Trunc((Self.Height/2)
      - ((FCenterPoint.YTile - LatToYtile(APointLatDec, FZoom))*cTileSize));
  end else begin
    Result := cErrPixel;
  end;
end;

procedure TSimpleOsmMap.UpdateMapLayer(Shift: TShiftState; X, Y: Integer);
var
  BmpLayer: TBitmapLayer;
  Dx, Dy: Integer;
begin
  if FPanMap then begin
    if PanMinDiffOk(X, Y) then begin
      BmpLayer := TBitmapLayer(Self.Layers[Ord(mlMap)]);

      Dx := X - FPanFrom.X;
      Dy := Y - FPanFrom.Y;

      BmpLayer.Location := FloatRect(0 + Dx, 0 + Dy, Self.Width + Dx, Self.Height + Dy);
    end;

    //Screen.Cursor := crHandPoint;
  end;
end;

procedure TSimpleOsmMap.UpdateMapObjectLayer(Shift: TShiftState; X, Y: Integer);
var
  BmpLayer: TBitmapLayer;
  I, Delta, Dx, Dy: Integer;
  MO: TMapObject;
  MergedMapObjectList: TObjectList<TMapObject>;
begin
  if (FMapObjectList = nil) or (not FMapLayerCanPaint) then begin
    EXIT;
  end;

  //update points
  BmpLayer := TBitmapLayer(Self.Layers[Ord(mlPoints)]);
  BmpLayer.Bitmap.BeginUpdate;
  try
    if FShowMapObjects then begin
      if FPanMap then begin  //pan map
        if (X <> cErrPixel) and (Y <> cErrPixel) then begin
          if PanMinDiffOk(X, Y) then begin

            Dx := X - FPanFrom.X;
            Dy := Y - FPanFrom.Y;

            BmpLayer.Location := FloatRect(0 + Dx, 0 + Dy, Self.Width + Dx, Self.Height + Dy);
          end;
        end;
      end else begin //redraw map objects position

        BmpLayer.Location := FloatRect(0, 0, Self.Width, Self.Height);
        BmpLayer.Bitmap.SetSize(Self.Width, Self.Height);
        BmpLayer.Bitmap.Clear(clNone32);

        //recalculate pixel position
        if (FCenterPoint.LonDec <> cErrCoord) and (FCenterPoint.LatDec <> cErrCoord)
          and (FZoom <> cErrZoom)
        then begin

          //calculate new position
          for I := 0 to (FMapObjectList.Count - 1) do begin
            MO := FMapObjectList.Items[I];
            if MO.Kind in [mokGeneral, mokPOI] then begin
              //first way -> large position error in low zoom
              //MO.SetLeftTopPx(LonDecToPixelX(MO.LonDec), LatDecToPixelY(MO.LatDec));

              //second way -> smaller position error in low zoom, better way
              MO.SetPxPosition(CalculatePxX(MO.LonDec), CalculatePxY(MO.LatDec));
            end;
          end; //end - for

          //draw object
          if FObjectMerging then begin

            MergedMapObjectList := TObjectList<TMapObject>.Create;
            try

              //copy all map object into temporary list
              for I := 0 to (FMapObjectList.Count - 1) do begin
                if (FMapObjectList.Items[I].Visible) then begin
                  //object is visible
                  if FMapObjectList.Items[I].Kind in [mokGeneral, mokPOI] then begin
                    //it is a point object
                    MO := CloneMapObject(FMapObjectList.Items[I]);
                    MergedMapObjectList.Add(MO);
                  end;
                end;
              end; //end - for

              //search near points and draw results
              while (MergedMapObjectList.Count > 0) do begin
                MO := MergedMapObjectList.First;
                if (MergedMapObjectList.Count > 1) then begin
                  I := 1;
                  while (I < MergedMapObjectList.Count) do begin
                    Delta := MapObjectDistance(MO, MergedMapObjectList.Items[I]);
                    if (Delta <= 0) then begin
                      if (MO.Color <> MergedMapObjectList.Items[I].Color) then begin
                        MO.Color := Blend(MO.Color, MergedMapObjectList.Items[I].Color, 50); //50%
                      end;
                      MO.Caption := MO.Caption + cMapObjectCaptionDelimiter + MergedMapObjectList.Items[I].Caption;
                      MergedMapObjectList.Delete(I);
                    end else begin
                      I := I + 1;
                    end;
                  end; //end - while - naibrous searching
                end;

                if (PointsCountFromCaption(MO.Caption) > 1) then begin
                  DrawMultiMapObject(BmpLayer.Bitmap, MO);
                end else begin
                  DrawMapObject(BmpLayer.Bitmap, MO);
                end;

                MergedMapObjectList.Delete(0); //delete first object
              end; //end - for

            finally
              FreeAndNil(MergedMapObjectList);
            end;

          end else begin //end - meging draw

            for I := 0 to (FMapObjectList.Count - 1) do begin
              MO := FMapObjectList.Items[I];
              if MO.Kind in [mokGeneral, mokPOI] then begin
                if MO.Visible then begin
                  DrawMapObject(BmpLayer.Bitmap, MO);
                end;
              end;
            end; //end - for

          end; //end - normal draw

        end; //end - center coord and zoom ok
      end; //end - redraw map objects
    end else begin //all map objects hidden
      BmpLayer.Location := FloatRect(0, 0, Self.Width, Self.Height);
      BmpLayer.Bitmap.SetSize(Self.Width, Self.Height);
      BmpLayer.Bitmap.Clear(clNone32);
    end;
  finally
    BmpLayer.Bitmap.EndUpdate;
  end;

  //update drawings
  UpdateDrawLayer(Shift, X, Y);
end;

procedure TSimpleOsmMap.UpdateDrawLayer(Shift: TShiftState; X, Y: Integer);
var
  BmpLayer: TBitmapLayer;
  I, Dx, Dy: Integer;
  MO: TMapObject;
begin
  if (FMapObjectList = nil) or (not FMapLayerCanPaint) then begin
    EXIT;
  end;

  BmpLayer := TBitmapLayer(Self.Layers[Ord(mlDraw)]);
  BmpLayer.Bitmap.BeginUpdate;
  try
    if FShowMapObjects then begin
      if FPanMap then begin  //pan map
        if (X <> cErrPixel) and (Y <> cErrPixel) then begin
          if PanMinDiffOk(X, Y) then begin

            Dx := X - FPanFrom.X;
            Dy := Y - FPanFrom.Y;

            BmpLayer.Location := FloatRect(0 + Dx, 0 + Dy, Self.Width + Dx, Self.Height + Dy);
          end;
        end;
      end else begin //redraw map objects position

        BmpLayer.Location := FloatRect(0, 0, Self.Width, Self.Height);
        BmpLayer.Bitmap.SetSize(Self.Width, Self.Height);
        BmpLayer.Bitmap.Clear(clNone32);

        //recalculate pixel position
        if (FCenterPoint.LonDec <> cErrCoord) and (FCenterPoint.LatDec <> cErrCoord)
          and (FZoom <> cErrZoom)
        then begin

          //calculate new position
          for I := 0 to (FMapObjectList.Count - 1) do begin
            MO := FMapObjectList.Items[I];
            if (MO.Kind in [mokDraw]) then begin
              //first way -> large position error in low zoom
              //MO.SetLeftTopPx(LonDecToPixelX(MO.LonDec), LatDecToPixelY(MO.LatDec));

              //second way -> smaller position error in low zoom, better way
              MO.SetPxPosition(CalculatePxX(MO.LonDec), CalculatePxY(MO.LatDec));

              if MO.Visible then begin
                DrawMapObjectDrawing(BmpLayer.Bitmap, MO);
              end;
            end;
          end; //end - for

        end; //end - center coord and zoom ok
      end; //end - redraw map objects
    end else begin //all map objects hidden
      BmpLayer.Location := FloatRect(0, 0, Self.Width, Self.Height);
      BmpLayer.Bitmap.SetSize(Self.Width, Self.Height);
      BmpLayer.Bitmap.Clear(clNone32);
    end;
  finally
    BmpLayer.Bitmap.EndUpdate;
  end;
end;

procedure TSimpleOsmMap.UpdateGpsLayer(Shift: TShiftState; X, Y: Integer);
var
  BmpLayer: TBitmapLayer;
  Dx, Dy: Integer;
begin
  if (FGPSPosition = nil) then begin
    EXIT;
  end;

  BmpLayer := TBitmapLayer(Self.Layers[Ord(mlGps)]);
  if FGPSPosition.Visible then begin
    if FPanMap then begin  //pan map
      if (X <> cErrPixel) and (Y <> cErrPixel) then begin
        if PanMinDiffOk(X, Y) then begin

          Dx := X - FPanFrom.X;
          Dy := Y - FPanFrom.Y;

          BmpLayer.Location := FloatRect(0 + Dx, 0 + Dy, Self.Width + Dx, Self.Height + Dy);
        end;
      end;
    end else begin //redraw gps position

      BmpLayer.Location := FloatRect(0, 0, Self.Width, Self.Height);
      BmpLayer.Bitmap.SetSize(Self.Width, Self.Height);
      BmpLayer.Bitmap.Clear(clNone32);

      //recalculate pixel position
      if (FGPSPosition.LonDec <> cErrCoord) and (FGPSPosition.LatDec <> cErrCoord)
      and (FCenterPoint.LonDec <> cErrCoord) and (FCenterPoint.LatDec <> cErrCoord)
       and (FZoom <> cErrZoom)
      then begin
        //first way -> large marker position error in low zoom
        //FGPSPosition.SetLeftTopPx(LonDecToPixelX(FGpsPosition.LonDec), LatDecToPixelY(FGpsPosition.LatDec));

        //second way -> smaller marker position error in low zoom, better way
        FGPSPosition.SetPxPosition(
          CalculatePxX(FGPSPosition.LonDec),
          CalculatePxY(FGPSPosition.LatDec)
        );

        DrawMapObject(BmpLayer.Bitmap, FGPSPosition);
      end;

    end;
  end else begin //gps hidden
    BmpLayer.Location := FloatRect(0, 0, Self.Width, Self.Height);
    BmpLayer.Bitmap.SetSize(Self.Width, Self.Height);
    BmpLayer.Bitmap.Clear(clNone32);
  end;
end;

procedure TSimpleOsmMap.UpdateCoordLayer(Shift: TShiftState; X, Y: Integer);
var
  Xpos, Ypos: Integer;
  StrText: string;
  BmpLayer: TBitmapLayer;
begin
  BmpLayer := TBitmapLayer(Self.Layers[Ord(mlCoord)]);

  if FShowCoordinates then begin
    if (not FPanMap) then begin

      if (FMousePoint.LonDec <> cErrCoord) and (FMousePoint.LatDec <> cErrCoord) then begin
        BmpLayer.Location := FloatRect(0, 0, Self.Width, Self.Height);
        BmpLayer.Bitmap.SetSize(Self.Width, Self.Height);
        BmpLayer.Bitmap.Clear(clNone32);

        BmpLayer.Bitmap.Canvas.Brush.Style := bsClear;
        BmpLayer.Bitmap.Canvas.Font.Color := clBlack;
        BmpLayer.Bitmap.Canvas.Font.Style := [fsBold];
        BmpLayer.Bitmap.Canvas.Font.Size := 8;

        //mouse coordination
        //X in <0, 180>
        //Y in <0, 85.0511>
        //If coordiantes are ok, draw to text layer
        if RangeLonOk(FMousePoint.LonDec)
          and RangeLatOk(FMousePoint.LatDec)
        then begin
          //coordinates
          StrText := Format('%s  %s', [
            FormatFloat('#0.000000', FMousePoint.LatDec),
            FormatFloat('##0.000000', FMousePoint.LonDec)
          ]);

          Ypos := Self.Height - (BmpLayer.Bitmap.Canvas.TextHeight(StrText) * 2) - 5;
          Xpos := Self.Width - BmpLayer.Bitmap.Canvas.TextWidth(StrText) - 5;

          BmpLayer.Bitmap.Canvas.TextOut(Xpos, Ypos, StrText);
        end;
      end; //end - coordination ok

    end; //end no pan
  end else begin
    BmpLayer.Bitmap.Clear(clNone32);
  end;
end;

procedure TSimpleOsmMap.UpdateInfoLayer(Shift: TShiftState; X, Y: Integer);
var
  Xpos, Ypos: Integer;
  StrText: string;
  BmpLayer: TBitmapLayer;
begin
  BmpLayer := TBitmapLayer(Self.Layers[Ord(mlInfo)]);

  BmpLayer.Location := FloatRect(0, 0, Self.Width, Self.Height);
  BmpLayer.Bitmap.SetSize(Self.Width, Self.Height);
  BmpLayer.Bitmap.Clear(clNone32);

  BmpLayer.Bitmap.Canvas.Brush.Style := bsClear;
  BmpLayer.Bitmap.Canvas.Font.Color := clBlack;
  BmpLayer.Bitmap.Canvas.Font.Style := [fsBold];
  BmpLayer.Bitmap.Canvas.Font.Size := 8;

  //credits
  StrText := '� Contributors OpenStreetMap';
  Ypos := Self.Height - BmpLayer.Bitmap.Canvas.TextHeight(StrText) - 5;
  Xpos := Self.Width - BmpLayer.Bitmap.Canvas.TextWidth(StrText) - 5;
  BmpLayer.Bitmap.Canvas.TextOut(Xpos, Ypos, StrText);

  //map scale
  if (FCenterPoint.LatDec <> cErrCoord) and (FZoom <> cErrZoom) then begin
    DrawMapScaleDist(BmpLayer,
      5, //left point of the distance indicator position
      Self.Height - 5); //bottom point of the dist indicator position
  end;

  //selection rectangle
  if FSelectArea then begin
    DrawSelectionRectangle(BmpLayer, FSelectAreaFrom.X, FSelectAreaFrom.Y, X, Y);
  end;
end;

procedure TSimpleOsmMap.UpdateMapControlLayer(Shift: TShiftState; X, Y: Integer);
var
  BmpLayer: TBitmapLayer;
  p: TPoint;
begin
  BmpLayer := TBitmapLayer(Self.Layers[Ord(mlMapControl)]);
  BmpLayer.Visible := FShowMapZoomControl or FShowMapMoveControl;
  if BmpLayer.Visible then begin

    //get mouse pos if needed
    if (X = cErrPixel) or (Y = cErrPixel) then begin
      Windows.GetCursorPos(p);
      p := Self.ScreenToClient(p);
    end;

    BmpLayer.BeginUpdate;
    try
      BmpLayer.Location := FloatRect(0, 0, Self.Width, Self.Height);
      BmpLayer.Bitmap.SetSize(Self.Width, Self.Height);
      BmpLayer.Bitmap.Clear(clNone32);

      if FShowMapZoomControl then begin
        DrawZoomControl(BmpLayer, X, Y);
      end;

      if FShowMapMoveControl then begin
        DrawMoveControl(BmpLayer, X, Y);
      end;
    finally
      BmpLayer.EndUpdate;
    end;

  end;
end;

procedure TSimpleOsmMap.ZoomAtMapObject(AName: string; ACaption: string = '';
  AZoom: Integer = 16; ALonDec: Double = cErrCoord; ALatDec: Double = cErrCoord;
  AKind: TMapObjectKind = moAll);
var
  mo: TMapObject;
begin
  mo := FindMapObject(AName, ACaption, ALonDec, ALatDec, AKind);
  if (mo <> nil) then begin
    SetCenterLonLatDec(mo.LonDec, mo.LatDec);
    SetZoomInt(AZoom);
    ShowMap;
  end;
end;

procedure TSimpleOsmMap.ZoomIn(ACenter: Boolean = False);
begin
  SetZoom(FZoom + 1, ACenter);
end;

procedure TSimpleOsmMap.ZoomOut(ACenter: Boolean = False);
begin
  SetZoom(FZoom - 1, ACenter);
end;

{procedure TSimpleOsmMap.UpdateLayers(Shift: TShiftState; X, Y: Integer);
begin
  // #1 - MAP
  UpdateMapLayer(Shift, X, Y);

  // #2 - COORDINATIONS
  UpdateCoordLayer(Shift, X, Y);

  // $3 - INFO
  UpdateInfoLayer(Shift, X, Y);

  // #4 - POINTS
  UpdateMapObjectLayer(Shift, X, Y);

  // #5 - GPS
  UpdateGpsLayer(Shift, X, Y);
end;}

procedure TSimpleOsmMap.SaveTileToCache(Ax, Ay: Integer; ABaseMapUrl: string;
  APictureStream: TMemoryStream; ATimeStamp: TDateTime; AZoom: Integer = cErrZoom);
var
  TmpZoom: Integer;
  TmpTileExt: string;
begin
  if (AZoom = cErrZoom) then begin
    TmpZoom := FZoom;
  end else begin
    TmpZoom := AZoom;
  end;

  if (ABaseMapUrl = '') then begin
    if (FOsmMapUrl <> '') then begin
      ABaseMapUrl := FOsmMapUrl;
    end;
  end;

  TmpTileExt := TrimTileExt(ABaseMapUrl); // tile extension
  ABaseMapUrl := TrimUrlBase(ABaseMapUrl); // only server addres

  APictureStream.Position := 0;
  
  if (TmpZoom <> cErrZoom) and (ABaseMapUrl <> '') then begin
    try
      FIBQueryInsert.Close;
      FIBQueryInsert.ParamByName('url_base').AsString := ABaseMapUrl;
      FIBQueryInsert.ParamByName('zoom').AsInteger := TmpZoom;
      FIBQueryInsert.ParamByName('x').AsInteger := Ax;
      FIBQueryInsert.ParamByName('y').AsInteger := Ay;
      FIBQueryInsert.ParamByName('img_extension').AsString := TmpTileExt;
      FIBQueryInsert.ParamByName('img_data').LoadFromStream(APictureStream, ftBlob);
      FIBQueryInsert.ParamByName('upd_ts').AsDateTime := ATimeStamp;
      FIBQueryInsert.ExecSQL;

      CommitTransaction;
    except
      RollbackTransaction;

      FLastError := Format('Error while caching tile "address: %s, x: %d, y: %d, zoom: %d"', [
        ABaseMapUrl, Ax, Ay, TmpZoom
      ]);
    end;
  end;
end;

function TSimpleOsmMap.TileInCache(Ax, Ay: Integer; AZoom: Integer = cErrZoom;
  ACacheTimeout: Integer = cCacheTimeOut; ABaseMapUrl: string = ''): Boolean;
var
  TmpTileExt: string;
begin
  Result := False;

  if (AZoom = cErrZoom) then begin
    AZoom := FZoom;
  end;

  if (ACacheTimeout = cCacheTimeOut) then begin
    ACacheTimeout := FCacheTimeOut;
  end;

  if (ABaseMapUrl = '') then begin
    if (FOsmMapUrl <> '') then begin
      ABaseMapUrl := FOsmMapUrl;
    end;
  end;

  TmpTileExt := TrimTileExt(ABaseMapUrl); // tile extension
  ABaseMapUrl := TrimUrlBase(ABaseMapUrl); // only server addres

  if (AZoom <> cErrZoom) and (ABaseMapUrl <> '') then begin
    FIBQuerySearch.Close;
    FIBQuerySearch.ParamByName('x').AsInteger := Ax;
    FIBQuerySearch.ParamByName('y').AsInteger := Ay;
    FIBQuerySearch.ParamByName('zoom').AsInteger := AZoom;
    FIBQuerySearch.ParamByName('url_base').AsString := ABaseMapUrl;
    FIBQuerySearch.ParamByName('img_ext').AsString := TmpTileExt;
    FIBQuerySearch.Open;

    if FIBQuerySearch.RecordCount = 1 then begin

      if (ACacheTimeout <> cCacheTimeOut) then begin
        if FIBQuerySearch.FieldByName('day_age').AsInteger >= ACacheTimeout then begin

          //delete old tile
          FIBQueryDelete.Close;
          FIBQueryDelete.ParamByName('id').AsLargeInt := FIBQuerySearch.FieldByName('id').AsLargeInt;
          FIBQueryDelete.ExecSQL;

          Result := False; //download again

        end else begin
          Result := FIBQuerySearch.FieldByName('id').AsLargeInt > 0;
        end;
      end else begin
        Result := FIBQuerySearch.FieldByName('id').AsLargeInt > 0;
      end;

    end;
  end;
end;

procedure TSimpleOsmMap.ClearCache(ACacheDayTimeout: Integer = cCacheTimeOut);
begin
  if FIBDatabase.Connected then begin
    try
      FIBQueryClear.Close;
      FIBQueryClear.ParamByName('cache_timeout').AsInteger := ACacheDayTimeout;
      FIBQueryClear.ExecSQL;
    except
      RollbackTransaction;
      EXIT;
    end;
    CommitTransaction;
  end;
end;

function TSimpleOsmMap.AddMapObjectDrawCircle(AName: string; ALatDec: Double; ALonDec: Double; ARadius: Integer;
  ACaption: string = ''; AColor: TColor = clBlack; ABorderWidth: Integer = cBorderDefWidth): TMapObject;
begin
  Result := AddMapObject(
    Zoom{actual map zoom},
    AName, ALatDec, ALonDec, modkCircle,
    0{Width not used}, 0{Height not used}, ARadius,
    0{Angle not used}, AColor, ACaption, ABorderWidth, 0{Meter length not used},
    True {Visible}
  );
end;

function TSimpleOsmMap.AddMapObjectDrawLine(AName: string; ALatDec: Double; ALonDec: Double; AAngle: Double;
  ACaption: string = ''; AWidth: Integer = cDefLineWidth; AColor: TColor = clBlack;
  ALengthMeters: Integer = cDefLineLengthMeters): TMapObject;
begin
  Result := AddMapObject(
    -1 {zoom not used},
    AName, ALatDec, ALonDec, modkLine,
    0{Width not used}, 0{Height not used}, 0{Radius not used},
    AAngle, AColor, ACaption, AWidth{Line widht to BorderWidth}, ALengthMeters,
    True {Visible}
  );
end;

function TSimpleOsmMap.AddMapObjectDrawRectangle(AName: string; ALatDec: Double; ALonDec: Double; AWidth: Integer;
  AHeight: Integer; ACaption: string = ''; AColor: TColor = clBlack; ABorderWidth: Integer = cBorderDefWidth): TMapObject;
begin
  Result := AddMapObject(
    Zoom{actual map zoom},
    AName, ALatDec, ALonDec, modkRectangle,
    AWidth, AHeight, 0{Radius not used},
    0{Angle not used}, AColor, ACaption, ABorderWidth, 0{Meter length not used},
    True {Visible}
  );
end;
{ TSimpleOsmMap -------------------------------------------------------------- }

end.
