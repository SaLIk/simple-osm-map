Copyright (c) 2013 Pavel Sala <alahomora25@seznam.cz>

This work is free. You can redistribute it and/or modify it.


This software is provided 'as-is', without any express or implied
warranty.  In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
   claim that you wrote the original software. If you use this software
   in a product, an acknowledgment in the product documentation would be
   appreciated but is not required.
2. Altered source versions must be plainly marked as such, and must not be
   misrepresented as being the original software.
3. This notice may not be removed or altered from any source distribution.


REQUIRED LIBRARIES
--------------------

SYNAPSE (Release 40, 2012-04-23)
  - Ararat Synapse - Pascal TCP/IP Library for Dephi, C++Builder, Kylix
    and FreePascal.
  - BSD License
  - http://sourceforge.net/projects/synalist/

GRAPHICS32 (2.0.0 alpha)
  - Graphics library for Delphi and Kylix/CLX. Optimized for 32-bit pixel formats.
  - Mozilla Public License 1.1 (MPL 1.1)
  - http://sourceforge.net/projects/graphics32/?source=navbar

GRAPHICEX (version - 9.9, 03-SEP-2000 ml)
  - GraphicEx is an addendum to Delphi's Graphics.pas to enable your application
    to load many common image formats.
  - Mozilla Public License 1.1 (MPL 1.1)
  - http://www.delphi-gems.com/index.php/libs/graphicex-library 



SUPPORT
-------
For latest news and support visit the SimpleOsmMap home page at https://gitlab.com/u/SaLIk.